/* Copyright (C) 2019, 2020, 2021 Brandon Invergo <b.invergo@exeter.ac.uk> */

/* This program is free software: you can redistribute it and/or modify */
/* it under the terms of the GNU General Public License as published by */
/* the Free Software Foundation, either version 3 of the License, or */
/* (at your option) any later version. */

/* This program is distributed in the hope that it will be useful, */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the */
/* GNU General Public License for more details. */

/* You should have received a copy of the GNU General Public License */
/* along with this program.  If not, see <http://www.gnu.org/licenses/>. */

#include <config.h>

#include <check.h>

#include <math.h>
#include <errno.h>
#include "error.h"
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_math.h>
#include "gl_linkedhash_list.h"
#include "gl_xlist.h"

#include "../src/seq-wins.h"
#include "../src/sequences.h"
#include "../src/motifstats.h"
#include "../src/pssm.h"
#include "../src/sequence.h"

#include "test-util.h"

#define TOL 1e-3


seq_set seq_wins;
size_t win_len;
gsl_vector *total_emp_counts;
gsl_vector *total_pos_counts;
gsl_matrix *emp_counts;
gsl_vector *num_pos_pc;
motifstats bg_pos_freqs;
gsl_matrix *bg_global_freqs;
gsl_matrix *pfm;
gsl_matrix *pssm;

void
aa_setup (void)
{
  win_len = read_seq_wins (&seq_wins, "data/test-seq-wins.txt", SEQ_AA);

  seq_set bg_seq_wins;
  read_seq_wins (&bg_seq_wins, "data/test-bg-seq-wins.txt", SEQ_AA);
  bg_pos_freqs.name = NULL;
  bg_pos_freqs.pfm = NULL;
  bg_pos_freqs.pssm = NULL;
  bg_pos_freqs.bg_freqs = NULL;
  bg_pos_freqs.col_ic = NULL;
  bg_pos_freqs.col_relh = NULL;
  bg_pos_freqs.type = SEQ_AA;
  bg_pos_freqs.method = SCORE_FREQ;
  bg_pos_freqs.bg = BG_NONE;
  bg_pos_freqs.pc_n = PC_N_NONE;
  bg_pos_freqs.pc_dist = PC_DIST_EQUAL;
  bg_pos_freqs.n = gl_list_size (bg_seq_wins.seqs);
  bg_pos_freqs.length = win_len;
  bg_pos_freqs.weighted = false;
  bg_pos_freqs.ic_base = 2.0;
  bg_pos_freqs.n_feats = 0;
  calc_motif_stats (&bg_pos_freqs, &bg_seq_wins, NULL, 0.0, 0.0);
  clear_seq_win_set (&bg_seq_wins);

  seq_set bg_seqs;
  read_sequences (&bg_seqs, "data/test.fasta", SEQ_AA);
  gsl_matrix *bg_global_freqs_tmp = calc_global_bg (bg_seqs);
  bg_global_freqs = gsl_matrix_alloc (NUM_AA, win_len);
  gsl_vector_const_view bg_freqs_v = gsl_matrix_const_column (bg_global_freqs_tmp, 0);
  for (size_t i=0; i<win_len; i++)
    {
      gsl_matrix_set_col (bg_global_freqs, i, &bg_freqs_v.vector);
    }
  clear_global_seq_set (&bg_seqs);
  gsl_matrix_free (bg_global_freqs_tmp);

  total_emp_counts = gsl_vector_calloc (win_len);
  if (!total_emp_counts)
    error (EXIT_FAILURE, errno, "Memory exhausted");
  total_pos_counts = gsl_vector_calloc (win_len);
  if (!total_pos_counts)
    error (EXIT_FAILURE, errno, "Memory exhausted");
  num_pos_pc = gsl_vector_calloc (win_len);
  if (!num_pos_pc)
    error (EXIT_FAILURE, errno, "Memory exhausted");
  pfm = gsl_matrix_calloc (NUM_AA, win_len);
  if (!pfm)
    error (EXIT_FAILURE, errno, "Memory exhausted");
  pssm = gsl_matrix_calloc (NUM_AA, win_len);
  if (!pssm)
    error (EXIT_FAILURE, errno, "Memory exhausted");
}

void
aa_teardown (void)
{
  clear_seq_win_set (&seq_wins);
  free_motif_stats (bg_pos_freqs, false);
  gsl_matrix_free (bg_global_freqs);
  gsl_vector_free (total_emp_counts);
  gsl_vector_free (total_pos_counts);
  gsl_vector_free (num_pos_pc);
  gsl_matrix_free (pfm);
  gsl_matrix_free (pssm);
}


void
nuc_setup (void)
{
  win_len = read_seq_wins (&seq_wins, "data/test-nuc-seq-wins.txt", SEQ_NUC);

  seq_set bg_seq_wins;
  read_seq_wins (&bg_seq_wins, "data/test-nuc-bg-seq-wins.txt", SEQ_NUC);
  bg_pos_freqs.name = NULL;
  bg_pos_freqs.pfm = NULL;
  bg_pos_freqs.pssm = NULL;
  bg_pos_freqs.bg_freqs = NULL;
  bg_pos_freqs.col_ic = NULL;
  bg_pos_freqs.col_relh = NULL;
  bg_pos_freqs.type = SEQ_NUC;
  bg_pos_freqs.method = SCORE_FREQ;
  bg_pos_freqs.bg = BG_NONE;
  bg_pos_freqs.pc_n = PC_N_NONE;
  bg_pos_freqs.pc_dist = PC_DIST_EQUAL;
  bg_pos_freqs.n = gl_list_size (bg_seq_wins.seqs);
  bg_pos_freqs.length = win_len;
  bg_pos_freqs.weighted = false;
  bg_pos_freqs.ic_base = 2.0;
  bg_pos_freqs.n_feats = 0;
  calc_motif_stats (&bg_pos_freqs, &bg_seq_wins, NULL, 0.0, 0.0);
  clear_seq_win_set (&bg_seq_wins);

  seq_set bg_seqs;
  read_sequences (&bg_seqs, "data/test-nuc.fasta", SEQ_NUC);
  gsl_matrix *bg_global_freqs_tmp = calc_global_bg (bg_seqs);
  bg_global_freqs = gsl_matrix_alloc (NUM_NUC, win_len);
  gsl_vector_const_view bg_freqs_v = gsl_matrix_const_column (bg_global_freqs_tmp, 0);
  for (size_t i=0; i<win_len; i++)
    {
      gsl_matrix_set_col (bg_global_freqs, i, &bg_freqs_v.vector);
    }
  clear_global_seq_set (&bg_seqs);
  gsl_matrix_free (bg_global_freqs_tmp);

  total_emp_counts = gsl_vector_calloc (win_len);
  if (!total_emp_counts)
    error (EXIT_FAILURE, errno, "Memory exhausted");
  total_pos_counts = gsl_vector_calloc (win_len);
  if (!total_pos_counts)
    error (EXIT_FAILURE, errno, "Memory exhausted");
  num_pos_pc = gsl_vector_calloc (win_len);
  if (!num_pos_pc)
    error (EXIT_FAILURE, errno, "Memory exhausted");
  pfm = gsl_matrix_calloc (NUM_NUC, win_len);
  if (!pfm)
    error (EXIT_FAILURE, errno, "Memory exhausted");
  pssm = gsl_matrix_calloc (NUM_NUC, win_len);
  if (!pssm)
    error (EXIT_FAILURE, errno, "Memory exhausted");
}

void
nuc_teardown (void)
{
  clear_seq_win_set (&seq_wins);
  free_motif_stats (bg_pos_freqs, false);
  gsl_matrix_free (bg_global_freqs);
  gsl_vector_free (total_emp_counts);
  gsl_vector_free (total_pos_counts);
  gsl_vector_free (num_pos_pc);
  gsl_matrix_free (pfm);
  gsl_matrix_free (pssm);
}


START_TEST(test_pos_bg)
{
  seq_set bg_seqs;
  size_t win_len = read_seq_wins (&bg_seqs, "data/test-seq-wins.txt", SEQ_GUESS);
  double exp_m[] =
    {0.0, 0.0, 0.0, 0.4, 0.2,  /* ARG */
     0.0, 0.0, 0.0, 0.0, 0.0,  /* HIS */
     0.0, 0.0, 0.0, 0.0, 0.0,  /* LYS */
     0.0, 0.0, 0.0, 0.0, 0.0,  /* ASP */
     0.2, 0.0, 0.0, 0.4, 0.0,  /* GLU */
     0.2, 0.0, 1.0, 0.0, 0.0,  /* SER */
     0.2, 0.0, 0.0, 0.2, 0.0,  /* THR */
     0.0, 0.0, 0.0, 0.0, 0.2,  /* ASN */
     0.0, 0.0, 0.0, 0.0, 0.0,  /* GLN */
     0.0, 0.0, 0.0, 0.0, 0.0,  /* CYS */
     0.2, 0.2, 0.0, 0.0, 0.0,  /* GLY */
     0.0, 0.2, 0.0, 0.0, 0.2,  /* PRO */
     0.0, 0.2, 0.0, 0.0, 0.2,  /* ALA */
     0.0, 0.0, 0.0, 0.0, 0.0,  /* VAL */
     0.0, 0.0, 0.0, 0.0, 0.0,  /* ILE */
     0.2, 0.4, 0.0, 0.0, 0.2,  /* LEU */
     0.0, 0.0, 0.0, 0.0, 0.0,  /* MET */
     0.0, 0.0, 0.0, 0.0, 0.0,  /* PHE */
     0.0, 0.0, 0.0, 0.0, 0.0,  /* TYR */
     0.0, 0.0, 0.0, 0.0, 0.0}; /* TRP */
  gsl_matrix_view expected = gsl_matrix_view_array (exp_m, NUM_AA, 5);
  motifstats bg_freqs;
  bg_freqs.name = NULL;
  bg_freqs.pfm = NULL;
  bg_freqs.pssm = NULL;
  bg_freqs.bg_freqs = NULL;
  bg_freqs.col_ic = NULL;
  bg_freqs.col_relh = NULL;
  bg_freqs.type = SEQ_AA;
  bg_freqs.method = SCORE_FREQ;
  bg_freqs.bg = BG_NONE;
  bg_freqs.pc_n = PC_N_NONE;
  bg_freqs.pc_dist = PC_DIST_EQUAL;
  bg_freqs.n = gl_list_size (bg_seqs.seqs);
  bg_freqs.length = win_len;
  bg_freqs.weighted = false;
  bg_freqs.ic_base = 2.0;
  bg_freqs.n_feats = 0;
  calc_motif_stats (&bg_freqs, &bg_seqs, NULL, 0.0, 0.0);
  assert_gsl_matrix_eq_tol (bg_freqs.pfm, &expected.matrix, TOL);
  free_motif_stats (bg_freqs, false);
  clear_seq_win_set (&bg_seqs);
}
END_TEST


START_TEST(test_pos_nuc_bg)
{
  seq_set bg_seqs;
  size_t win_len = read_seq_wins (&bg_seqs, "data/test-nuc-seq-wins.txt", SEQ_GUESS);
  double exp_m[] =
    {0.0, 0.0,  0.25, 1.0, 0.0,  /* A */
     0.5, 0.75, 0.5,  0.0, 0.0,  /* C */
     0.0, 0.0,  0.0,  0.0, 0.0,  /* G */
     0.5, 0.25, 0.25, 0.0, 1.0}; /* T */
  gsl_matrix_view expected = gsl_matrix_view_array (exp_m, NUM_NUC, 5);
  motifstats bg_freqs;
  bg_freqs.name = NULL;
  bg_freqs.pfm = NULL;
  bg_freqs.pssm = NULL;
  bg_freqs.bg_freqs = NULL;
  bg_freqs.col_ic = NULL;
  bg_freqs.col_relh = NULL;
  bg_freqs.type = SEQ_NUC;
  bg_freqs.method = SCORE_FREQ;
  bg_freqs.bg = BG_NONE;
  bg_freqs.pc_n = PC_N_NONE;
  bg_freqs.pc_dist = PC_DIST_EQUAL;
  bg_freqs.n = gl_list_size (bg_seqs.seqs);
  bg_freqs.length = win_len;
  bg_freqs.weighted = false;
  bg_freqs.ic_base = 2.0;
  bg_freqs.n_feats = 0;
  calc_motif_stats (&bg_freqs, &bg_seqs, NULL, 0.0, 0.0);
  assert_gsl_matrix_eq_tol (bg_freqs.pfm, &expected.matrix, TOL);
  free_motif_stats (bg_freqs, false);
  clear_seq_win_set (&bg_seqs);
}
END_TEST


START_TEST(test_total_emp_counts)
{
  count_pos_res (&seq_wins, total_emp_counts, total_pos_counts,
                 pfm, bg_pos_freqs.pfm, false);
  double exp_dat[] = {5, 4, 1, 3, 5};
  gsl_vector_view exp_v = gsl_vector_view_array (exp_dat, win_len);
  assert_gsl_vector_eq_tol (total_emp_counts, &exp_v.vector, TOL);
}
END_TEST


START_TEST(test_nuc_total_emp_counts)
{
  count_pos_res (&seq_wins, total_emp_counts, total_pos_counts,
                 pfm, bg_pos_freqs.pfm, false);
  double exp_dat[] = {2, 2, 3, 1, 1};
  gsl_vector_view exp_v = gsl_vector_view_array (exp_dat, win_len);
  assert_gsl_vector_eq_tol (total_emp_counts, &exp_v.vector, TOL);
}
END_TEST


START_TEST(test_total_emp_counts_weighted)
{
  weight_seq_windows (&seq_wins);
  count_pos_res (&seq_wins, total_emp_counts, total_pos_counts,
                 pfm, bg_pos_freqs.pfm, true);
  double exp_dat[] = {5, 4, 1, 3, 5};
  gsl_vector_view exp_v = gsl_vector_view_array (exp_dat, win_len);
  assert_gsl_vector_eq_tol (total_emp_counts, &exp_v.vector, TOL);
}
END_TEST


START_TEST(test_total_pos_counts)
{
  count_pos_res (&seq_wins, total_pos_counts, total_pos_counts,
                 pfm, bg_pos_freqs.pfm, false);
  double exp_dat[] = {5, 5, 5, 5, 5};
  gsl_vector_view exp_v = gsl_vector_view_array (exp_dat, win_len);
  assert_gsl_vector_eq_tol (total_pos_counts, &exp_v.vector, TOL);
}
END_TEST


START_TEST(test_nuc_total_pos_counts)
{
  count_pos_res (&seq_wins, total_pos_counts, total_pos_counts,
                 pfm, bg_pos_freqs.pfm, false);
  double exp_dat[] = {4, 4, 4, 4, 4};
  gsl_vector_view exp_v = gsl_vector_view_array (exp_dat, win_len);
  assert_gsl_vector_eq_tol (total_pos_counts, &exp_v.vector, TOL);
}
END_TEST


START_TEST(test_total_pos_counts_weighted)
{
  weight_seq_windows (&seq_wins);
  count_pos_res (&seq_wins, total_pos_counts, total_pos_counts,
                 pfm, bg_pos_freqs.pfm, true);
  double exp_dat[] = {5, 5, 5, 5, 5};
  gsl_vector_view exp_v = gsl_vector_view_array (exp_dat, win_len);
  assert_gsl_vector_eq_tol (total_pos_counts, &exp_v.vector, TOL);
}
END_TEST


START_TEST(test_emp_counts)
{
  count_pos_res (&seq_wins, total_emp_counts, total_pos_counts,
                 pfm, bg_pos_freqs.pfm, false);
  double exp_dat[] = {0, 0, 0, 2, 1, /* ARG */
                      0, 0, 0, 0, 0, /* HIS */
                      0, 0, 0, 0, 0, /* LYS */
                      0, 0, 0, 0, 0, /* ASP */
                      1, 0, 0, 2, 0, /* GLU */
                      1, 0, 5, 0, 0, /* SER */
                      1, 0, 0, 1, 0, /* THR */
                      0, 0, 0, 0, 1, /* ASN */
                      0, 0, 0, 0, 0, /* GLN */
                      0, 0, 0, 0, 0, /* CYS */
                      1, 1, 0, 0, 0, /* GLY */
                      0, 1, 0, 0, 1, /* PRO */
                      0, 1, 0, 0, 1, /* ALA */
                      0, 0, 0, 0, 0, /* VAL */
                      0, 0, 0, 0, 0, /* ILE */
                      1, 2, 0, 0, 1, /* LEU */
                      0, 0, 0, 0, 0, /* MET */
                      0, 0, 0, 0, 0, /* PHE */
                      0, 0, 0, 0, 0, /* TYR */
                      0, 0, 0, 0, 0}; /* TRP */
  gsl_matrix_view exp_m = gsl_matrix_view_array (exp_dat, NUM_AA, win_len);
  assert_gsl_matrix_eq_tol (pfm, &exp_m.matrix, TOL);
}
END_TEST


START_TEST(test_nuc_emp_counts)
{
  count_pos_res (&seq_wins, total_emp_counts, total_pos_counts,
                 pfm, bg_pos_freqs.pfm, false);
  double exp_dat[] = {0, 0, 1, 4, 0, /* A */
                      2, 3, 2, 0, 0, /* C */
                      0, 0, 0, 0, 0, /* G */
                      2, 1, 1, 0, 4}; /* T */
  gsl_matrix_view exp_m = gsl_matrix_view_array (exp_dat, NUM_NUC, win_len);
  assert_gsl_matrix_eq_tol (pfm, &exp_m.matrix, TOL);
}
END_TEST


START_TEST(test_emp_counts_weighted)
{
  weight_seq_windows (&seq_wins);
  count_pos_res (&seq_wins, total_emp_counts, total_pos_counts,
                 pfm, bg_pos_freqs.pfm, true);
  double exp_dat[] = {0,        0,        0,   1.90833,  0.891665, /* ARG */
                      0,        0,        0,   0,        0,        /* HIS */
                      0,        0,        0,   0,        0,        /* LYS */
                      0,        0,        0,   0,        0,        /* ASP */
                      0.891665, 0,        0,   1.908333, 0,        /* GLU */
                      1.183335, 0,        5.0, 0,        0,        /* SER */
                      1.016665, 0,        0,   1.183335, 0,        /* THR */
                      0,        0,        0,   0,        1.183335, /* ASN */
                      0,        0,        0,   0,        0,        /* GLN */
                      0,        0,        0,   0,        0,        /* CYS */
                      1.016665, 1.016665, 0,   0,        0,        /* GLY */
                      0,        1.183335, 0,   0,        1.016665, /* PRO */
                      0,        1.016665, 0,   0,        1.016665, /* ALA */
                      0,        0,        0,   0,        0,        /* VAL */
                      0,        0,        0,   0,        0,        /* ILE */
                      0.891665, 1.78333,  0,   0,        0.891665, /* LEU */
                      0,        0,        0,   0,        0,        /* MET */
                      0,        0,        0,   0,        0,        /* PHE */
                      0,        0,        0,   0,        0,        /* TYR */
                      0,        0,        0,   0,        0};       /* TRP */
  gsl_matrix_view exp_m = gsl_matrix_view_array (exp_dat, NUM_AA, win_len);
  assert_gsl_matrix_eq_tol (pfm, &exp_m.matrix, TOL);
}
END_TEST


START_TEST(test_calc_num_pos_pc)
{
  count_pos_res (&seq_wins, total_emp_counts, total_pos_counts,
                 pfm, bg_pos_freqs.pfm, true);
  calc_num_pos_pc (total_emp_counts, num_pos_pc, 1.0);
  double exp_dat[] = {5, 4, 1, 3, 5};
  gsl_vector_view exp_v = gsl_vector_view_array (exp_dat, win_len);
  assert_gsl_vector_eq_tol (num_pos_pc, &exp_v.vector, TOL);
}
END_TEST


START_TEST(test_nuc_calc_num_pos_pc)
{
  count_pos_res (&seq_wins, total_emp_counts, total_pos_counts,
                 pfm, bg_pos_freqs.pfm, true);
  calc_num_pos_pc (total_emp_counts, num_pos_pc, 1.0);
  double exp_dat[] = {2, 2, 3, 1, 1};
  gsl_vector_view exp_v = gsl_vector_view_array (exp_dat, win_len);
  assert_gsl_vector_eq_tol (num_pos_pc, &exp_v.vector, TOL);
}
END_TEST


START_TEST(test_calc_num_pos_pc2)
{
  count_pos_res (&seq_wins, total_emp_counts, total_pos_counts,
                 pfm, bg_pos_freqs.pfm, true);
  calc_num_pos_pc (total_emp_counts, num_pos_pc, 2.0);
  double exp_dat[] = {10, 8, 2, 6, 10};
  gsl_vector_view exp_v = gsl_vector_view_array (exp_dat, win_len);
  assert_gsl_vector_eq_tol (num_pos_pc, &exp_v.vector, TOL);
}
END_TEST


START_TEST(test_nuc_calc_num_pos_pc2)
{
  count_pos_res (&seq_wins, total_emp_counts, total_pos_counts,
                 pfm, bg_pos_freqs.pfm, true);
  calc_num_pos_pc (total_emp_counts, num_pos_pc, 2.0);
  double exp_dat[] = {4, 4, 6, 2, 2};
  gsl_vector_view exp_v = gsl_vector_view_array (exp_dat, win_len);
  assert_gsl_vector_eq_tol (num_pos_pc, &exp_v.vector, TOL);
}
END_TEST


START_TEST(test_calc_pos_freqs_bg_global)
{
  count_pos_res (&seq_wins, total_emp_counts, total_pos_counts,
                 pfm, bg_pos_freqs.pfm, false);
  calc_num_pos_pc (total_emp_counts, num_pos_pc, 1.0);
  calc_pos_freqs (pfm, total_pos_counts, bg_global_freqs, num_pos_pc,
                  0.0, BG_GLOBAL, PC_N_POS, PC_DIST_FREQ);
  double exp_m[] = {(5*0.0582192 + 0)/(5 + 5), (4*0.0582192 + 0)/(4 + 5),  (1*0.0582192 + 0)/(1 + 5),  (3*0.0582192 + 2)/(3 + 5),  (5*0.0582192 + 1)/(5 + 5),
                    (5*0.00684932 + 0)/(5 + 5), (4*0.00684932 + 0)/(4 + 5),  (1*0.00684932 + 0)/(1 + 5),  (3*0.00684932 + 0)/(3 + 5),  (5*0.00684932 + 0)/(5 + 5),
                    (5*0.0650685 + 0)/(5 + 5), (4*0.0650685 + 0)/(4 + 5),  (1*0.0650685 + 0)/(1 + 5),  (3*0.0650685 + 0)/(3 + 5),  (5*0.0650685 + 0)/(5 + 5),
                    (5*0.0547945 + 0)/(5 + 5), (4*0.0547945 + 0)/(4 + 5),  (1*0.0547945 + 0)/(1 + 5),  (3*0.0547945 + 0)/(3 + 5),  (5*0.0547945 + 0)/(5 + 5),
                    (5*0.0684932 + 1)/(5 + 5), (4*0.0684932 + 0)/(4 + 5),  (1*0.0684932 + 0)/(1 + 5),  (3*0.0684932 + 2)/(3 + 5),  (5*0.0684932 + 0)/(5 + 5),
                    (5*0.0856164 + 1)/(5 + 5), (4*0.0856164 + 0)/(4 + 5),  (1*0.0856164 + 5)/(1 + 5),  (3*0.0856164 + 0)/(3 + 5),  (5*0.0856164 + 0)/(5 + 5),
                    (5*0.0342466 + 1)/(5 + 5), (4*0.0342466 + 0)/(4 + 5),  (1*0.0342466 + 0)/(1 + 5),  (3*0.0342466 + 1)/(3 + 5),  (5*0.0342466 + 0)/(5 + 5),
                    (5*0.0376712 + 0)/(5 + 5), (4*0.0376712 + 0)/(4 + 5),  (1*0.0376712 + 0)/(1 + 5),  (3*0.0376712 + 0)/(3 + 5),  (5*0.0376712 + 1)/(5 + 5),
                    (5*0.0547945 + 0)/(5 + 5), (4*0.0547945 + 0)/(4 + 5),  (1*0.0547945 + 0)/(1 + 5),  (3*0.0547945 + 0)/(3 + 5),  (5*0.0547945 + 0)/(5 + 5),
                    (5*0.0342466 + 0)/(5 + 5), (4*0.0342466 + 0)/(4 + 5),  (1*0.0342466 + 0)/(1 + 5),  (3*0.0342466 + 0)/(3 + 5),  (5*0.0342466 + 0)/(5 + 5),
                    (5*0.0582192 + 1)/(5 + 5), (4*0.0582192 + 1)/(4 + 5),  (1*0.0582192 + 0)/(1 + 5),  (3*0.0582192 + 0)/(3 + 5),  (5*0.0582192 + 0)/(5 + 5),
                    (5*0.0650685 + 0)/(5 + 5), (4*0.0650685 + 1)/(4 + 5),  (1*0.0650685 + 0)/(1 + 5),  (3*0.0650685 + 0)/(3 + 5),  (5*0.0650685 + 1)/(5 + 5),
                    (5*0.0650685 + 0)/(5 + 5), (4*0.0650685 + 1)/(4 + 5),  (1*0.0650685 + 0)/(1 + 5),  (3*0.0650685 + 0)/(3 + 5),  (5*0.0650685 + 1)/(5 + 5),
                    (5*0.0582192 + 0)/(5 + 5), (4*0.0582192 + 0)/(4 + 5),  (1*0.0582192 + 0)/(1 + 5),  (3*0.0582192 + 0)/(3 + 5),  (5*0.0582192 + 0)/(5 + 5),
                    (5*0.0445205 + 0)/(5 + 5), (4*0.0445205 + 0)/(4 + 5),  (1*0.0445205 + 0)/(1 + 5),  (3*0.0445205 + 0)/(3 + 5),  (5*0.0445205 + 0)/(5 + 5),
                    (5*0.0753425 + 1)/(5 + 5), (4*0.0753425 + 2)/(4 + 5),  (1*0.0753425 + 0)/(1 + 5),  (3*0.0753425 + 0)/(3 + 5),  (5*0.0753425 + 1)/(5 + 5),
                    (5*0.0273973 + 0)/(5 + 5), (4*0.0273973 + 0)/(4 + 5),  (1*0.0273973 + 0)/(1 + 5),  (3*0.0273973 + 0)/(3 + 5),  (5*0.0273973 + 0)/(5 + 5),
                    (5*0.0273973 + 0)/(5 + 5), (4*0.0273973 + 0)/(4 + 5),  (1*0.0273973 + 0)/(1 + 5),  (3*0.0273973 + 0)/(3 + 5),  (5*0.0273973 + 0)/(5 + 5),
                    (5*0.0410959 + 0)/(5 + 5), (4*0.0410959 + 0)/(4 + 5),  (1*0.0410959 + 0)/(1 + 5),  (3*0.0410959 + 0)/(3 + 5),  (5*0.0410959 + 0)/(5 + 5),
                    (5*0.0376712 + 0)/(5 + 5), (4*0.0376712 + 0)/(4 + 5),  (1*0.0376712 + 0)/(1 + 5),  (3*0.0376712 + 0)/(3 + 5),  (5*0.0376712 + 0)/(5 + 5)};
  gsl_matrix_view expected = gsl_matrix_view_array (exp_m, NUM_AA, win_len);
  assert_gsl_matrix_eq_tol (pfm, &expected.matrix, TOL);
}
END_TEST


START_TEST(test_nuc_calc_pos_freqs_bg_global)
{
  count_pos_res (&seq_wins, total_emp_counts, total_pos_counts,
                 pfm, bg_pos_freqs.pfm, false);
  calc_num_pos_pc (total_emp_counts, num_pos_pc, 1.0);
  calc_pos_freqs (pfm, total_pos_counts, bg_global_freqs, num_pos_pc,
                  0.0, BG_GLOBAL, PC_N_POS, PC_DIST_FREQ);
  double exp_m[] = {(2*0.4222222 + 0)/(2 + 4), (2*0.4222222 + 0)/(2 + 4),  (3*0.4222222 + 1)/(3 + 4),  (1*0.4222222 + 4)/(1 + 4),  (1*0.4222222 + 0)/(1 + 4),
                    (2*0.1138889 + 2)/(2 + 4), (2*0.1138889 + 3)/(2 + 4),  (3*0.1138889 + 2)/(3 + 4),  (1*0.1138889 + 0)/(1 + 4),  (1*0.1138889 + 0)/(1 + 4),
                    (2*0.2111111 + 0)/(2 + 4), (2*0.2111111 + 0)/(2 + 4),  (3*0.2111111 + 0)/(3 + 4),  (1*0.2111111 + 0)/(1 + 4),  (1*0.2111111 + 0)/(1 + 4),
                    (2*0.2527778 + 2)/(2 + 4), (2*0.2527778 + 1)/(2 + 4),  (3*0.2527778 + 1)/(3 + 4),  (1*0.2527778 + 0)/(1 + 4),  (1*0.2527778 + 4)/(1 + 4)};
  gsl_matrix_view expected = gsl_matrix_view_array (exp_m, NUM_NUC, win_len);
  assert_gsl_matrix_eq_tol (pfm, &expected.matrix, TOL);
}
END_TEST


START_TEST(test_calc_pos_freqs_bg_pos)
{
  count_pos_res (&seq_wins, total_emp_counts, total_pos_counts,
                 pfm, bg_pos_freqs.pfm, false);
  calc_num_pos_pc (total_emp_counts, num_pos_pc, 1.0);
  calc_pos_freqs (pfm, total_pos_counts, bg_pos_freqs.pfm, num_pos_pc, 0.0,
                  BG_POS, PC_N_POS, PC_DIST_FREQ);
  double exp_m[] = {(5*0.108194 + 0)/(5 + 5), (4*0.058295 + 0)/(4 + 5),  (1*0.0 + 0)/(1 + 5),  (3*0.036543 + 2)/(3 + 5),  (5*0.064242 + 1)/(5 + 5),
                    (5*0.022031 + 0)/(5 + 5), (4*0.029912 + 0)/(4 + 5),  (1*0.0 + 0)/(1 + 5),  (3*0.013229 + 0)/(3 + 5),  (5*0.017613 + 0)/(5 + 5),
                    (5*0.058861 + 0)/(5 + 5), (4*0.057788 + 0)/(4 + 5),  (1*0.0 + 0)/(1 + 5),  (3*0.033333 + 0)/(3 + 5),  (5*0.059512 + 0)/(5 + 5),
                    (5*0.052779 + 0)/(5 + 5), (4*0.067756 + 0)/(4 + 5),  (1*0.0 + 0)/(1 + 5),  (3*0.056648 + 0)/(3 + 5),  (5*0.063229 + 0)/(5 + 5),
                    (5*0.066295 + 1)/(5 + 5), (4*0.064884 + 0)/(4 + 5),  (1*0.0 + 0)/(1 + 5),  (3*0.057831 + 2)/(3 + 5),  (5*0.078931 + 0)/(5 + 5),
                    (5*0.106842 + 1)/(5 + 5), (4*0.093436 + 0)/(4 + 5),  (1*0.604832 + 5)/(1 + 5),  (3*0.070671 + 0)/(3 + 5),  (5*0.116785 + 0)/(5 + 5),
                    (5*0.055820 + 1)/(5 + 5), (4*0.049679 + 0)/(4 + 5),  (1*0.226390 + 0)/(1 + 5),  (3*0.040767 + 1)/(3 + 5),  (5*0.057146 + 0)/(5 + 5),
                    (5*0.040953 + 0)/(5 + 5), (4*0.043259 + 0)/(4 + 5),  (1*0.0 + 0)/(1 + 5),  (3*0.023365 + 0)/(3 + 5),  (5*0.040421 + 1)/(5 + 5),
                    (5*0.042135 + 0)/(5 + 5), (4*0.042414 + 0)/(4 + 5),  (1*0.0 + 0)/(1 + 5),  (3*0.078780 + 0)/(3 + 5),  (5*0.042448 + 0)/(5 + 5),
                    (5*0.012739 + 0)/(5 + 5), (4*0.010652 + 0)/(4 + 5),  (1*0.0 + 0)/(1 + 5),  (3*0.008498 + 0)/(3 + 5),  (5*0.016092 + 0)/(5 + 5),
                    (5*0.060551 + 1)/(5 + 5), (4*0.076542 + 1)/(4 + 5),  (1*0.0 + 0)/(1 + 5),  (3*0.048370 + 0)/(3 + 5),  (5*0.062046 + 0)/(5 + 5),
                    (5*0.083797 + 0)/(5 + 5), (4*0.067081 + 1)/(4 + 5),  (1*0.0 + 0)/(1 + 5),  (3*0.210221 + 0)/(3 + 5),  (5*0.076406 + 1)/(5 + 5),
                    (5*0.067984 + 0)/(5 + 5), (4*0.064715 + 1)/(4 + 5),  (1*0.0 + 0)/(1 + 5),  (3*0.044315 + 0)/(3 + 5),  (5*0.067452 + 1)/(5 + 5),
                    (5*0.048893 + 0)/(5 + 5), (4*0.051875 + 0)/(4 + 5),  (1*0.0 + 0)/(1 + 5),  (3*0.059689 + 0)/(3 + 5),  (5*0.060525 + 0)/(5 + 5),
                    (5*0.030140 + 0)/(5 + 5), (4*0.041738 + 0)/(4 + 5),  (1*0.0 + 0)/(1 + 5),  (3*0.035192 + 0)/(3 + 5),  (5*0.032818 + 0)/(5 + 5),
                    (5*0.070857 + 1)/(5 + 5), (4*0.096646 + 2)/(4 + 5),  (1*0.0 + 0)/(1 + 5),  (3*0.087565 + 0)/(3 + 5),  (5*0.066776 + 1)/(5 + 5),
                    (5*0.019497 + 0)/(5 + 5), (4*0.021296 + 0)/(4 + 5),  (1*0.0 + 0)/(1 + 5),  (3*0.022521 + 0)/(3 + 5),  (5*0.018120 + 0)/(5 + 5),
                    (5*0.024396 + 0)/(5 + 5), (4*0.028054 + 0)/(4 + 5),  (1*0.0 + 0)/(1 + 5),  (3*0.042288 + 0)/(3 + 5),  (5*0.026229 + 0)/(5 + 5),
                    (5*0.022200 + 0)/(5 + 5), (4*0.027885 + 0)/(4 + 5),  (1*0.168779 + 0)/(1 + 5),  (3*0.021169 + 0)/(3 + 5),  (5*0.026398 + 0)/(5 + 5),
                    (5*0.005136 + 0)/(5 + 5), (4*0.006091 + 0)/(4 + 5),  (1*0.0 + 0)/(1 + 5),  (3*0.009005 + 0)/(3 + 5),  (5*0.006800 + 0)/(5 + 5)};
  gsl_matrix_view expected = gsl_matrix_view_array (exp_m, NUM_AA, win_len);
  assert_gsl_matrix_eq_tol (pfm, &expected.matrix, TOL);
}
END_TEST


START_TEST(test_nuc_calc_pos_freqs_bg_pos)
{
  count_pos_res (&seq_wins, total_emp_counts, total_pos_counts,
                 pfm, bg_pos_freqs.pfm, false);
  calc_num_pos_pc (total_emp_counts, num_pos_pc, 1.0);
  calc_pos_freqs (pfm, total_pos_counts, bg_pos_freqs.pfm, num_pos_pc, 0.0,
                  BG_POS, PC_N_POS, PC_DIST_FREQ);
  double exp_m[] = {(2*0.0   + 0)/(2 + 4), (2*0.0  + 0)/(2 + 4),  (3*0.125 + 1)/(3 + 4),  (1*0.75  + 4)/(1 + 4),  (1*0.0 + 0)/(1 + 4),
                    (2*0.5   + 2)/(2 + 4), (2*0.625 + 3)/(2 + 4),  (3*0.5   + 2)/(3 + 4),  (1*0.125 + 0)/(1 + 4),  (1*0.0 + 0)/(1 + 4),
                    (2*0.125 + 0)/(2 + 4), (2*0.0  + 0)/(2 + 4),  (3*0.125 + 0)/(3 + 4),  (1*0.125 + 0)/(1 + 4),  (1*0.0 + 0)/(1 + 4),
                    (2*0.375 + 2)/(2 + 4), (2*0.375 + 1)/(2 + 4),  (3*0.25  + 1)/(3 + 4),  (1*0.0   + 0)/(1 + 4),  (1*1.0 + 4)/(1 + 4)};
  gsl_matrix_view expected = gsl_matrix_view_array (exp_m, NUM_NUC, win_len);
  assert_gsl_matrix_eq_tol (pfm, &expected.matrix, TOL);
}
END_TEST


START_TEST(test_calc_pos_freqs_const_equal)
{
  count_pos_res (&seq_wins, total_emp_counts, total_pos_counts,
                 pfm, bg_pos_freqs.pfm, false);
  calc_pos_freqs (pfm, total_pos_counts, bg_pos_freqs.pfm, num_pos_pc, 5.0,
                  BG_POS, PC_N_CONST, PC_DIST_EQUAL);
  double exp_m[] = {(5.0/20.0 + 0)/(5 + 5), (5.0/20.0 + 0)/(5 + 5),  (5.0/20.0 + 0)/(5 + 5),  (5.0/20.0 + 2)/(5 + 5),  (5.0/20.0 + 1)/(5 + 5),
                    (5.0/20.0 + 0)/(5 + 5), (5.0/20.0 + 0)/(5 + 5),  (5.0/20.0 + 0)/(5 + 5),  (5.0/20.0 + 0)/(5 + 5),  (5.0/20.0 + 0)/(5 + 5),
                    (5.0/20.0 + 0)/(5 + 5), (5.0/20.0 + 0)/(5 + 5),  (5.0/20.0 + 0)/(5 + 5),  (5.0/20.0 + 0)/(5 + 5),  (5.0/20.0 + 0)/(5 + 5),
                    (5.0/20.0 + 0)/(5 + 5), (5.0/20.0 + 0)/(5 + 5),  (5.0/20.0 + 0)/(5 + 5),  (5.0/20.0 + 0)/(5 + 5),  (5.0/20.0 + 0)/(5 + 5),
                    (5.0/20.0 + 1)/(5 + 5), (5.0/20.0 + 0)/(5 + 5),  (5.0/20.0 + 0)/(5 + 5),  (5.0/20.0 + 2)/(5 + 5),  (5.0/20.0 + 0)/(5 + 5),
                    (5.0/20.0 + 1)/(5 + 5), (5.0/20.0 + 0)/(5 + 5),  (5.0/20.0 + 5)/(5 + 5),  (5.0/20.0 + 0)/(5 + 5),  (5.0/20.0 + 0)/(5 + 5),
                    (5.0/20.0 + 1)/(5 + 5), (5.0/20.0 + 0)/(5 + 5),  (5.0/20.0 + 0)/(5 + 5),  (5.0/20.0 + 1)/(5 + 5),  (5.0/20.0 + 0)/(5 + 5),
                    (5.0/20.0 + 0)/(5 + 5), (5.0/20.0 + 0)/(5 + 5),  (5.0/20.0 + 0)/(5 + 5),  (5.0/20.0 + 0)/(5 + 5),  (5.0/20.0 + 1)/(5 + 5),
                    (5.0/20.0 + 0)/(5 + 5), (5.0/20.0 + 0)/(5 + 5),  (5.0/20.0 + 0)/(5 + 5),  (5.0/20.0 + 0)/(5 + 5),  (5.0/20.0 + 0)/(5 + 5),
                    (5.0/20.0 + 0)/(5 + 5), (5.0/20.0 + 0)/(5 + 5),  (5.0/20.0 + 0)/(5 + 5),  (5.0/20.0 + 0)/(5 + 5),  (5.0/20.0 + 0)/(5 + 5),
                    (5.0/20.0 + 1)/(5 + 5), (5.0/20.0 + 1)/(5 + 5),  (5.0/20.0 + 0)/(5 + 5),  (5.0/20.0 + 0)/(5 + 5),  (5.0/20.0 + 0)/(5 + 5),
                    (5.0/20.0 + 0)/(5 + 5), (5.0/20.0 + 1)/(5 + 5),  (5.0/20.0 + 0)/(5 + 5),  (5.0/20.0 + 0)/(5 + 5),  (5.0/20.0 + 1)/(5 + 5),
                    (5.0/20.0 + 0)/(5 + 5), (5.0/20.0 + 1)/(5 + 5),  (5.0/20.0 + 0)/(5 + 5),  (5.0/20.0 + 0)/(5 + 5),  (5.0/20.0 + 1)/(5 + 5),
                    (5.0/20.0 + 0)/(5 + 5), (5.0/20.0 + 0)/(5 + 5),  (5.0/20.0 + 0)/(5 + 5),  (5.0/20.0 + 0)/(5 + 5),  (5.0/20.0 + 0)/(5 + 5),
                    (5.0/20.0 + 0)/(5 + 5), (5.0/20.0 + 0)/(5 + 5),  (5.0/20.0 + 0)/(5 + 5),  (5.0/20.0 + 0)/(5 + 5),  (5.0/20.0 + 0)/(5 + 5),
                    (5.0/20.0 + 1)/(5 + 5), (5.0/20.0 + 2)/(5 + 5),  (5.0/20.0 + 0)/(5 + 5),  (5.0/20.0 + 0)/(5 + 5),  (5.0/20.0 + 1)/(5 + 5),
                    (5.0/20.0 + 0)/(5 + 5), (5.0/20.0 + 0)/(5 + 5),  (5.0/20.0 + 0)/(5 + 5),  (5.0/20.0 + 0)/(5 + 5),  (5.0/20.0 + 0)/(5 + 5),
                    (5.0/20.0 + 0)/(5 + 5), (5.0/20.0 + 0)/(5 + 5),  (5.0/20.0 + 0)/(5 + 5),  (5.0/20.0 + 0)/(5 + 5),  (5.0/20.0 + 0)/(5 + 5),
                    (5.0/20.0 + 0)/(5 + 5), (5.0/20.0 + 0)/(5 + 5),  (5.0/20.0 + 0)/(5 + 5),  (5.0/20.0 + 0)/(5 + 5),  (5.0/20.0 + 0)/(5 + 5),
                    (5.0/20.0 + 0)/(5 + 5), (5.0/20.0 + 0)/(5 + 5),  (5.0/20.0 + 0)/(5 + 5),  (5.0/20.0 + 0)/(5 + 5),  (5.0/20.0 + 0)/(5 + 5)};
  gsl_matrix_view expected = gsl_matrix_view_array (exp_m, NUM_AA, win_len);
  assert_gsl_matrix_eq_tol (pfm, &expected.matrix, TOL);
}
END_TEST


START_TEST(test_nuc_calc_pos_freqs_const_equal)
{
  count_pos_res (&seq_wins, total_emp_counts, total_pos_counts,
                 pfm, bg_pos_freqs.pfm, false);
  calc_pos_freqs (pfm, total_pos_counts, bg_pos_freqs.pfm, num_pos_pc, 5.0,
                  BG_POS, PC_N_CONST, PC_DIST_EQUAL);
  double exp_m[] = {(5.0/4.0 + 0)/(5 + 4), (5.0/4.0 + 0)/(5 + 4),  (5.0/4.0 + 1)/(5 + 4),  (5.0/4.0 + 4)/(5 + 4),  (5.0/4.0 + 0)/(5 + 4),
                    (5.0/4.0 + 2)/(5 + 4), (5.0/4.0 + 3)/(5 + 4),  (5.0/4.0 + 2)/(5 + 4),  (5.0/4.0 + 0)/(5 + 4),  (5.0/4.0 + 0)/(5 + 4),
                    (5.0/4.0 + 0)/(5 + 4), (5.0/4.0 + 0)/(5 + 4),  (5.0/4.0 + 0)/(5 + 4),  (5.0/4.0 + 0)/(5 + 4),  (5.0/4.0 + 0)/(5 + 4),
                    (5.0/4.0 + 2)/(5 + 4), (5.0/4.0 + 1)/(5 + 4),  (5.0/4.0 + 1)/(5 + 4),  (5.0/4.0 + 0)/(5 + 4),  (5.0/4.0 + 4)/(5 + 4)};
  gsl_matrix_view expected = gsl_matrix_view_array (exp_m, NUM_NUC, win_len);
  assert_gsl_matrix_eq_tol (pfm, &expected.matrix, TOL);
}
END_TEST


START_TEST(test_calc_fc_score_global)
{
  count_pos_res (&seq_wins, total_emp_counts, total_pos_counts,
                 pfm, bg_pos_freqs.pfm, false);
  calc_num_pos_pc (total_emp_counts, num_pos_pc, 1.0);
  calc_pos_freqs (pfm, total_pos_counts, bg_global_freqs, num_pos_pc, 0.0,
                  BG_GLOBAL, PC_N_POS, PC_DIST_FREQ);
  calc_fc_score (pssm, pfm, bg_global_freqs, BG_GLOBAL, 2.0);
  double exp_m[] = {log2((5*0.0582192 + 0)/(5 + 5)/0.0582192), log2((4*0.0582192 + 0)/(4 + 5)/0.0582192),  log2((1*0.0582192 + 0)/(1 + 5)/0.0582192),  log2((3*0.0582192 + 2)/(3 + 5)/0.0582192),  log2((5*0.0582192 + 1)/(5 + 5)/0.0582192),
                    log2((5*0.00684932 + 0)/(5 + 5)/0.00684932), log2((4*0.00684932 + 0)/(4 + 5)/0.00684932),  log2((1*0.00684932 + 0)/(1 + 5)/0.00684932),  log2((3*0.00684932 + 0)/(3 + 5)/0.00684932),  log2((5*0.00684932 + 0)/(5 + 5)/0.00684932),
                    log2((5*0.0650685 + 0)/(5 + 5)/0.0650685), log2((4*0.0650685 + 0)/(4 + 5)/0.0650685),  log2((1*0.0650685 + 0)/(1 + 5)/0.0650685),  log2((3*0.0650685 + 0)/(3 + 5)/0.0650685),  log2((5*0.0650685 + 0)/(5 + 5)/0.0650685),
                    log2((5*0.0547945 + 0)/(5 + 5)/0.0547945), log2((4*0.0547945 + 0)/(4 + 5)/0.0547945),  log2((1*0.0547945 + 0)/(1 + 5)/0.0547945),  log2((3*0.0547945 + 0)/(3 + 5)/0.0547945),  log2((5*0.0547945 + 0)/(5 + 5)/0.0547945),
                    log2((5*0.0684932 + 1)/(5 + 5)/0.0684932), log2((4*0.0684932 + 0)/(4 + 5)/0.0684932),  log2((1*0.0684932 + 0)/(1 + 5)/0.0684932),  log2((3*0.0684932 + 2)/(3 + 5)/0.0684932),  log2((5*0.0684932 + 0)/(5 + 5)/0.0684932),
                    log2((5*0.0856164 + 1)/(5 + 5)/0.0856164), log2((4*0.0856164 + 0)/(4 + 5)/0.0856164),  log2((1*0.0856164 + 5)/(1 + 5)/0.0856164),  log2((3*0.0856164 + 0)/(3 + 5)/0.0856164),  log2((5*0.0856164 + 0)/(5 + 5)/0.0856164),
                    log2((5*0.0342466 + 1)/(5 + 5)/0.0342466), log2((4*0.0342466 + 0)/(4 + 5)/0.0342466),  log2((1*0.0342466 + 0)/(1 + 5)/0.0342466),  log2((3*0.0342466 + 1)/(3 + 5)/0.0342466),  log2((5*0.0342466 + 0)/(5 + 5)/0.0342466),
                    log2((5*0.0376712 + 0)/(5 + 5)/0.0376712), log2((4*0.0376712 + 0)/(4 + 5)/0.0376712),  log2((1*0.0376712 + 0)/(1 + 5)/0.0376712),  log2((3*0.0376712 + 0)/(3 + 5)/0.0376712),  log2((5*0.0376712 + 1)/(5 + 5)/0.0376712),
                    log2((5*0.0547945 + 0)/(5 + 5)/0.0547945), log2((4*0.0547945 + 0)/(4 + 5)/0.0547945),  log2((1*0.0547945 + 0)/(1 + 5)/0.0547945),  log2((3*0.0547945 + 0)/(3 + 5)/0.0547945),  log2((5*0.0547945 + 0)/(5 + 5)/0.0547945),
                    log2((5*0.0342466 + 0)/(5 + 5)/0.0342466), log2((4*0.0342466 + 0)/(4 + 5)/0.0342466),  log2((1*0.0342466 + 0)/(1 + 5)/0.0342466),  log2((3*0.0342466 + 0)/(3 + 5)/0.0342466),  log2((5*0.0342466 + 0)/(5 + 5)/0.0342466),
                    log2((5*0.0582192 + 1)/(5 + 5)/0.0582192), log2((4*0.0582192 + 1)/(4 + 5)/0.0582192),  log2((1*0.0582192 + 0)/(1 + 5)/0.0582192),  log2((3*0.0582192 + 0)/(3 + 5)/0.0582192),  log2((5*0.0582192 + 0)/(5 + 5)/0.0582192),
                    log2((5*0.0650685 + 0)/(5 + 5)/0.0650685), log2((4*0.0650685 + 1)/(4 + 5)/0.0650685),  log2((1*0.0650685 + 0)/(1 + 5)/0.0650685),  log2((3*0.0650685 + 0)/(3 + 5)/0.0650685),  log2((5*0.0650685 + 1)/(5 + 5)/0.0650685),
                    log2((5*0.0650685 + 0)/(5 + 5)/0.0650685), log2((4*0.0650685 + 1)/(4 + 5)/0.0650685),  log2((1*0.0650685 + 0)/(1 + 5)/0.0650685),  log2((3*0.0650685 + 0)/(3 + 5)/0.0650685),  log2((5*0.0650685 + 1)/(5 + 5)/0.0650685),
                    log2((5*0.0582192 + 0)/(5 + 5)/0.0582192), log2((4*0.0582192 + 0)/(4 + 5)/0.0582192),  log2((1*0.0582192 + 0)/(1 + 5)/0.0582192),  log2((3*0.0582192 + 0)/(3 + 5)/0.0582192),  log2((5*0.0582192 + 0)/(5 + 5)/0.0582192),
                    log2((5*0.0445205 + 0)/(5 + 5)/0.0445205), log2((4*0.0445205 + 0)/(4 + 5)/0.0445205),  log2((1*0.0445205 + 0)/(1 + 5)/0.0445205),  log2((3*0.0445205 + 0)/(3 + 5)/0.0445205),  log2((5*0.0445205 + 0)/(5 + 5)/0.0445205),
                    log2((5*0.0753425 + 1)/(5 + 5)/0.0753425), log2((4*0.0753425 + 2)/(4 + 5)/0.0753425),  log2((1*0.0753425 + 0)/(1 + 5)/0.0753425),  log2((3*0.0753425 + 0)/(3 + 5)/0.0753425),  log2((5*0.0753425 + 1)/(5 + 5)/0.0753425),
                    log2((5*0.0273973 + 0)/(5 + 5)/0.0273973), log2((4*0.0273973 + 0)/(4 + 5)/0.0273973),  log2((1*0.0273973 + 0)/(1 + 5)/0.0273973),  log2((3*0.0273973 + 0)/(3 + 5)/0.0273973),  log2((5*0.0273973 + 0)/(5 + 5)/0.0273973),
                    log2((5*0.0273973 + 0)/(5 + 5)/0.0273973), log2((4*0.0273973 + 0)/(4 + 5)/0.0273973),  log2((1*0.0273973 + 0)/(1 + 5)/0.0273973),  log2((3*0.0273973 + 0)/(3 + 5)/0.0273973),  log2((5*0.0273973 + 0)/(5 + 5)/0.0273973),
                    log2((5*0.0410959 + 0)/(5 + 5)/0.0410959), log2((4*0.0410959 + 0)/(4 + 5)/0.0410959),  log2((1*0.0410959 + 0)/(1 + 5)/0.0410959),  log2((3*0.0410959 + 0)/(3 + 5)/0.0410959),  log2((5*0.0410959 + 0)/(5 + 5)/0.0410959),
                    log2((5*0.0376712 + 0)/(5 + 5)/0.0376712), log2((4*0.0376712 + 0)/(4 + 5)/0.0376712),  log2((1*0.0376712 + 0)/(1 + 5)/0.0376712),  log2((3*0.0376712 + 0)/(3 + 5)/0.0376712),  log2((5*0.0376712 + 0)/(5 + 5)/0.0376712)};
  gsl_matrix_view expected = gsl_matrix_view_array (exp_m, NUM_AA, win_len);
  assert_gsl_matrix_eq_tol (pssm, &expected.matrix, TOL);
}
END_TEST


START_TEST(test_nuc_calc_fc_score_global)
{
  count_pos_res (&seq_wins, total_emp_counts, total_pos_counts,
                 pfm, bg_pos_freqs.pfm, false);
  calc_num_pos_pc (total_emp_counts, num_pos_pc, 1.0);
  calc_pos_freqs (pfm, total_pos_counts, bg_global_freqs, num_pos_pc, 0.0,
                  BG_GLOBAL, PC_N_POS, PC_DIST_FREQ);
  calc_fc_score (pssm, pfm, bg_global_freqs, BG_GLOBAL, 2.0);
  double exp_m[] = {log2((2*0.4222222 + 0)/(2 + 4)/0.4222222), log2((2*0.4222222 + 0)/(2 + 4)/0.4222222), log2((3*0.4222222 + 1)/(3 + 4)/0.4222222), log2((1*0.4222222 + 4)/(1 + 4)/0.4222222), log2((1*0.4222222 + 0)/(1 + 4)/0.4222222),
                    log2((2*0.1138889 + 2)/(2 + 4)/0.1138889), log2((2*0.1138889 + 3)/(2 + 4)/0.1138889), log2((3*0.1138889 + 2)/(3 + 4)/0.1138889), log2((1*0.1138889 + 0)/(1 + 4)/0.1138889), log2((1*0.1138889 + 0)/(1 + 4)/0.1138889),
                    log2((2*0.2111111 + 0)/(2 + 4)/0.2111111), log2((2*0.2111111 + 0)/(2 + 4)/0.2111111), log2((3*0.2111111 + 0)/(3 + 4)/0.2111111), log2((1*0.2111111 + 0)/(1 + 4)/0.2111111), log2((1*0.2111111 + 0)/(1 + 4)/0.2111111),
                    log2((2*0.2527778 + 2)/(2 + 4)/0.2527778), log2((2*0.2527778 + 1)/(2 + 4)/0.2527778), log2((3*0.2527778 + 1)/(3 + 4)/0.2527778), log2((1*0.2527778 + 0)/(1 + 4)/0.2527778), log2((1*0.2527778 + 4)/(1 + 4)/0.2527778)};
                    gsl_matrix_view expected = gsl_matrix_view_array (exp_m, NUM_NUC, win_len);
  assert_gsl_matrix_eq_tol (pssm, &expected.matrix, TOL);
}
END_TEST


START_TEST(test_nuc_calc_diff_score_global)
{
  count_pos_res (&seq_wins, total_emp_counts, total_pos_counts,
                 pfm, bg_pos_freqs.pfm, false);
  calc_num_pos_pc (total_emp_counts, num_pos_pc, 1.0);
  calc_pos_freqs (pfm, total_pos_counts, bg_global_freqs, num_pos_pc, 0.0,
                  BG_GLOBAL, PC_N_POS, PC_DIST_FREQ);
  calc_diff_score (pssm, pfm, bg_global_freqs, BG_GLOBAL, 1.2);
  double exp_m[] = {-pow(fabs((2*0.4222222 + 0)/(2 + 4)-0.4222222), 1.2), -pow(fabs((2*0.4222222 + 0)/(2 + 4)-0.4222222), 1.2), -pow(fabs((3*0.4222222 + 1)/(3 + 4)-0.4222222), 1.2),  pow(fabs((1*0.4222222 + 4)/(1 + 4)-0.4222222), 1.2), -pow(fabs((1*0.4222222 + 0)/(1 + 4)-0.4222222), 1.2),
                     pow(fabs((2*0.1138889 + 2)/(2 + 4)-0.1138889), 1.2),  pow(fabs((2*0.1138889 + 3)/(2 + 4)-0.1138889), 1.2),  pow(fabs((3*0.1138889 + 2)/(3 + 4)-0.1138889), 1.2), -pow(fabs((1*0.1138889 + 0)/(1 + 4)-0.113888), 1.2),  -pow(fabs((1*0.1138889 + 0)/(1 + 4)-0.1138889), 1.2),
                    -pow(fabs((2*0.2111111 + 0)/(2 + 4)-0.2111111), 1.2), -pow(fabs((2*0.2111111 + 0)/(2 + 4)-0.2111111), 1.2), -pow(fabs((3*0.2111111 + 0)/(3 + 4)-0.2111111), 1.2), -pow(fabs((1*0.2111111 + 0)/(1 + 4)-0.2111111), 1.2), -pow(fabs((1*0.2111111 + 0)/(1 + 4)-0.2111111), 1.2),
                     pow(fabs((2*0.2527778 + 2)/(2 + 4)-0.2527778), 1.2), -pow(fabs((2*0.2527778 + 1)/(2 + 4)-0.2527778), 1.2), -pow(fabs((3*0.2527778 + 1)/(3 + 4)-0.2527778), 1.2), -pow(fabs((1*0.2527778 + 0)/(1 + 4)-0.2527778), 1.2),  pow(fabs((1*0.2527778 + 4)/(1 + 4)-0.2527778), 1.2)};
                    gsl_matrix_view expected = gsl_matrix_view_array (exp_m, NUM_NUC, win_len);
  assert_gsl_matrix_eq_tol (pssm, &expected.matrix, TOL);
}
END_TEST


START_TEST(test_calc_fc_score_pos)
{
  count_pos_res (&seq_wins, total_emp_counts, total_pos_counts,
                 pfm, bg_pos_freqs.pfm, false);
  calc_num_pos_pc (total_emp_counts, num_pos_pc, 1.0);
  calc_pos_freqs (pfm, total_pos_counts, bg_pos_freqs.pfm, num_pos_pc, 0.0,
                  BG_POS, PC_N_POS, PC_DIST_FREQ);
  calc_fc_score (pssm, pfm, bg_pos_freqs.pfm, BG_POS, 2.0);
  double exp_m[] = {log2((5*0.108194 + 0)/(5 + 5)/0.108194), log2((4*0.058295 + 0)/(4 + 5)/0.058295),  log2((1*0.0 + 0)/(1 + 5)/0.0),  log2((3*0.036543 + 2)/(3 + 5)/0.036543),  log2((5*0.064242 + 1)/(5 + 5)/0.064242),
                    log2((5*0.022031 + 0)/(5 + 5)/0.022031), log2((4*0.029912 + 0)/(4 + 5)/0.029912),  log2((1*0.0 + 0)/(1 + 5)/0.0),  log2((3*0.013229 + 0)/(3 + 5)/0.013229),  log2((5*0.017613 + 0)/(5 + 5)/0.017613),
                    log2((5*0.058861 + 0)/(5 + 5)/0.058861), log2((4*0.057788 + 0)/(4 + 5)/0.057788),  log2((1*0.0 + 0)/(1 + 5)/0.0),  log2((3*0.033333 + 0)/(3 + 5)/0.033333),  log2((5*0.059512 + 0)/(5 + 5)/0.059512),
                    log2((5*0.052779 + 0)/(5 + 5)/0.052779), log2((4*0.067756 + 0)/(4 + 5)/0.067756),  log2((1*0.0 + 0)/(1 + 5)/0.0),  log2((3*0.056648 + 0)/(3 + 5)/0.056648),  log2((5*0.063229 + 0)/(5 + 5)/0.063229),
                    log2((5*0.066295 + 1)/(5 + 5)/0.066295), log2((4*0.064884 + 0)/(4 + 5)/0.064884),  log2((1*0.0 + 0)/(1 + 5)/0.0),  log2((3*0.057831 + 2)/(3 + 5)/0.057831),  log2((5*0.078941 + 0)/(5 + 5)/0.078941),
                    log2((5*0.106842 + 1)/(5 + 5)/0.106842), log2((4*0.093436 + 0)/(4 + 5)/0.093426),  log2((1*0.604832 + 5)/(1 + 5)/0.604832),  log2((3*0.070671 + 0)/(3 + 5)/0.070671),  log2((5*0.116785 + 0)/(5 + 5)/0.116785),
                    log2((5*0.055820 + 1)/(5 + 5)/0.055820), log2((4*0.049679 + 0)/(4 + 5)/0.049679),  log2((1*0.226390 + 0)/(1 + 5)/0.226390),  log2((3*0.040767 + 1)/(3 + 5)/0.040767),  log2((5*0.057146 + 0)/(5 + 5)/0.057146),
                    log2((5*0.040953 + 0)/(5 + 5)/0.040953), log2((4*0.043259 + 0)/(4 + 5)/0.043259),  log2((1*0.0 + 0)/(1 + 5)/0.0),  log2((3*0.023365 + 0)/(3 + 5)/0.023365),  log2((5*0.040421 + 1)/(5 + 5)/0.040421),
                    log2((5*0.042135 + 0)/(5 + 5)/0.042135), log2((4*0.042414 + 0)/(4 + 5)/0.042414),  log2((1*0.0 + 0)/(1 + 5)/0.0),  log2((3*0.078780 + 0)/(3 + 5)/0.078780),  log2((5*0.042448 + 0)/(5 + 5)/0.042448),
                    log2((5*0.012739 + 0)/(5 + 5)/0.012739), log2((4*0.010652 + 0)/(4 + 5)/0.010652),  log2((1*0.0 + 0)/(1 + 5)/0.0),  log2((3*0.008498 + 0)/(3 + 5)/0.008498),  log2((5*0.016092 + 0)/(5 + 5)/0.016092),
                    log2((5*0.060551 + 1)/(5 + 5)/0.060551), log2((4*0.076542 + 1)/(4 + 5)/0.076542),  log2((1*0.0 + 0)/(1 + 5)/0.0),  log2((3*0.048370 + 0)/(3 + 5)/0.048370),  log2((5*0.062046 + 0)/(5 + 5)/0.062046),
                    log2((5*0.083697 + 0)/(5 + 5)/0.083697), log2((4*0.067081 + 1)/(4 + 5)/0.067081),  log2((1*0.0 + 0)/(1 + 5)/0.0),  log2((3*0.210221 + 0)/(3 + 5)/0.210221),  log2((5*0.076406 + 1)/(5 + 5)/0.076406),
                    log2((5*0.067984 + 0)/(5 + 5)/0.067984), log2((4*0.064715 + 1)/(4 + 5)/0.064715),  log2((1*0.0 + 0)/(1 + 5)/0.0),  log2((3*0.044315 + 0)/(3 + 5)/0.044315),  log2((5*0.067452 + 1)/(5 + 5)/0.067452),
                    log2((5*0.048893 + 0)/(5 + 5)/0.048893), log2((4*0.051875 + 0)/(4 + 5)/0.051875),  log2((1*0.0 + 0)/(1 + 5)/0.0),  log2((3*0.059689 + 0)/(3 + 5)/0.059689),  log2((5*0.060525 + 0)/(5 + 5)/0.060525),
                    log2((5*0.030140 + 0)/(5 + 5)/0.030140), log2((4*0.041738 + 0)/(4 + 5)/0.041738),  log2((1*0.0 + 0)/(1 + 5)/0.0),  log2((3*0.035192 + 0)/(3 + 5)/0.035192),  log2((5*0.032818 + 0)/(5 + 5)/0.032818),
                    log2((5*0.070857 + 1)/(5 + 5)/0.070857), log2((4*0.096646 + 2)/(4 + 5)/0.096646),  log2((1*0.0 + 0)/(1 + 5)/0.0),  log2((3*0.087565 + 0)/(3 + 5)/0.087565),  log2((5*0.066776 + 1)/(5 + 5)/0.066776),
                    log2((5*0.019497 + 0)/(5 + 5)/0.019497), log2((4*0.021296 + 0)/(4 + 5)/0.021296),  log2((1*0.0 + 0)/(1 + 5)/0.0),  log2((3*0.022521 + 0)/(3 + 5)/0.022521),  log2((5*0.018120 + 0)/(5 + 5)/0.018120),
                    log2((5*0.024396 + 0)/(5 + 5)/0.024396), log2((4*0.028054 + 0)/(4 + 5)/0.028054),  log2((1*0.0 + 0)/(1 + 5)/0.0),  log2((3*0.042288 + 0)/(3 + 5)/0.042288),  log2((5*0.026229 + 0)/(5 + 5)/0.026229),
                    log2((5*0.022200 + 0)/(5 + 5)/0.022200), log2((4*0.027885 + 0)/(4 + 5)/0.027885),  log2((1*0.168779 + 0)/(1 + 5)/0.168779),  log2((3*0.021169 + 0)/(3 + 5)/0.021140),  log2((5*0.026398 + 0)/(5 + 5)/0.026398),
                    log2((5*0.005136 + 0)/(5 + 5)/0.005136), log2((4*0.006091 + 0)/(4 + 5)/0.006091),  log2((1*0.0 + 0)/(1 + 5)/0.0),  log2((3*0.009005 + 0)/(3 + 5)/0.009005),  log2((5*0.006800 + 0)/(5 + 5)/0.006800)};
  gsl_matrix_view expected = gsl_matrix_view_array (exp_m, NUM_AA, win_len);
  assert_gsl_matrix_eq_tol (pssm, &expected.matrix, TOL);
}
END_TEST


START_TEST(test_nuc_calc_fc_score_pos)
{
  count_pos_res (&seq_wins, total_emp_counts, total_pos_counts,
                 pfm, bg_pos_freqs.pfm, false);
  calc_num_pos_pc (total_emp_counts, num_pos_pc, 1.0);
  calc_pos_freqs (pfm, total_pos_counts, bg_pos_freqs.pfm, num_pos_pc, 0.0,
                  BG_POS, PC_N_POS, PC_DIST_FREQ);
  calc_fc_score (pssm, pfm, bg_pos_freqs.pfm, BG_POS, 2.0);
  double exp_m[] = {log2((2*0.0   + 0)/(2 + 4)/0.0),   log2((2*0.0  + 0)/(2 + 4)/0.0),  log2((3*0.125 + 1)/(3 + 4)/0.125), log2((1*0.75  + 4)/(1 + 4)/0.75),  log2((1*0.0 + 0)/(1 + 4)/0.0),
                    log2((2*0.5   + 2)/(2 + 4)/0.5),   log2((2*0.625 + 3)/(2 + 4)/0.625), log2((3*0.5   + 2)/(3 + 4)/0.5),   log2((1*0.125 + 0)/(1 + 4)/0.125), log2((1*0.0 + 0)/(1 + 4)/0.0),
                    log2((2*0.125 + 0)/(2 + 4)/0.125), log2((2*0.0  + 0)/(2 + 4)/0.0),  log2((3*0.125 + 0)/(3 + 4)/0.125), log2((1*0.125 + 0)/(1 + 4)/0.125), log2((1*0.0 + 0)/(1 + 4)/0.0),
                    log2((2*0.375 + 2)/(2 + 4)/0.375), log2((2*0.375 + 1)/(2 + 4)/0.375), log2((3*0.25  + 1)/(3 + 4)/0.25),  log2((1*0.0   + 0)/(1 + 4)/0.0),   log2((1*1.0 + 4)/(1 + 4)/1.0)};
  gsl_matrix_view expected = gsl_matrix_view_array (exp_m, NUM_NUC, win_len);
  assert_gsl_matrix_eq_tol (pssm, &expected.matrix, TOL);
}
END_TEST


START_TEST(test_nuc_calc_diff_score_pos)
{
  count_pos_res (&seq_wins, total_emp_counts, total_pos_counts,
                 pfm, bg_pos_freqs.pfm, false);
  calc_num_pos_pc (total_emp_counts, num_pos_pc, 1.0);
  calc_pos_freqs (pfm, total_pos_counts, bg_pos_freqs.pfm, num_pos_pc, 0.0,
                  BG_POS, PC_N_POS, PC_DIST_FREQ);
  calc_diff_score (pssm, pfm, bg_pos_freqs.pfm, BG_POS, 1.2);
  double exp_m[] = { pow(fabs((2*0.0   + 0)/(2 + 4)-0.0), 1.2),   pow(fabs((2*0.0  + 0)/(2 + 4)-0.0), 1.2),  pow(fabs((3*0.125 + 1)/(3 + 4)-0.125), 1.2),  pow(fabs((1*0.75  + 4)/(1 + 4)-0.75), 1.2),  pow(fabs((1*0.0 + 0)/(1 + 4)-0.0), 1.2),
                     pow(fabs((2*0.5   + 2)/(2 + 4)-0.5), 1.2),   pow(fabs((2*0.625 + 3)/(2 + 4)-0.625), 1.2), pow(fabs((3*0.5   + 2)/(3 + 4)-0.5), 1.2),   -pow(fabs((1*0.125 + 0)/(1 + 4)-0.125), 1.2), pow(fabs((1*0.0 + 0)/(1 + 4)-0.0), 1.2),
                    -pow(fabs((2*0.125 + 0)/(2 + 4)-0.125), 1.2), pow(fabs((2*0.0  + 0)/(2 + 4)-0.0), 1.2), -pow(fabs((3*0.125 + 0)/(3 + 4)-0.125), 1.2), -pow(fabs((1*0.125 + 0)/(1 + 4)-0.125), 1.2), pow(fabs((1*0.0 + 0)/(1 + 4)-0.0), 1.2),
                     pow(fabs((2*0.375 + 2)/(2 + 4)-0.375), 1.2), -pow(fabs((2*0.375 + 1)/(2 + 4)-0.375), 1.2), pow(fabs((3*0.25  + 1)/(3 + 4)-0.25), 1.2),   pow(fabs((1*0.0   + 0)/(1 + 4)-0.0), 1.2),   pow(fabs((1*1.0 + 4)/(1 + 4)-1.0), 1.2)};
  gsl_matrix_view expected = gsl_matrix_view_array (exp_m, NUM_NUC, win_len);
  assert_gsl_matrix_eq_tol (pssm, &expected.matrix, TOL);
}
END_TEST


START_TEST(test_calc_col_ic)
{
  count_pos_res (&seq_wins, total_emp_counts, total_pos_counts,
                 pfm, bg_pos_freqs.pfm, false);
  calc_num_pos_pc (total_emp_counts, num_pos_pc, 1.0);
  calc_pos_freqs (pfm, total_pos_counts, bg_global_freqs, num_pos_pc,
                  0.0, BG_GLOBAL, PC_N_POS, PC_DIST_FREQ);
  gsl_vector *col_ic = gsl_vector_calloc (pfm->size2);
  if (!col_ic)
    error (EXIT_FAILURE, errno, "Memory exhausted");
  calc_col_ic (pfm, col_ic, M_E);
  double exp_v[] = {
    log (4) - (-(2*0.4222222 + 0)/(2 + 4)*log((2*0.4222222 + 0)/(2 + 4))
               + -(2*0.1138889 + 2)/(2 + 4)*log((2*0.1138889 + 2)/(2 + 4))
               + -(2*0.2111111 + 0)/(2 + 4)*log((2*0.2111111 + 0)/(2 + 4))
               + -(2*0.2527778 + 2)/(2 + 4)*log((2*0.2527778 + 2)/(2 + 4))),
    log (4) - (-(2*0.4222222 + 0)/(2 + 4)*log((2*0.4222222 + 0)/(2 + 4))
               + -(2*0.1138889 + 3)/(2 + 4)*log((2*0.1138889 + 3)/(2 + 4))
               + -(2*0.2111111 + 0)/(2 + 4)*log((2*0.2111111 + 0)/(2 + 4))
               + -(2*0.2527778 + 1)/(2 + 4)*log((2*0.2527778 + 1)/(2 + 4))),
    log (4) - (-(3*0.4222222 + 1)/(3 + 4)*log((3*0.4222222 + 1)/(3 + 4))
               + -(3*0.1138889 + 2)/(3 + 4)*log((3*0.1138889 + 2)/(3 + 4))
               + -(3*0.2111111 + 0)/(3 + 4)*log((3*0.2111111 + 0)/(3 + 4))
               + -(3*0.2527778 + 1)/(3 + 4)*log((3*0.2527778 + 1)/(3 + 4))),
    log (4) - (-(1*0.4222222 + 4)/(1 + 4)*log((1*0.4222222 + 4)/(1 + 4))
               + -(1*0.1138889 + 0)/(1 + 4)*log((1*0.1138889 + 0)/(1 + 4))
               + -(1*0.2111111 + 0)/(1 + 4)*log((1*0.2111111 + 0)/(1 + 4))
               +  -(1*0.2527778 + 0)/(1 + 4)*log((1*0.2527778 + 0)/(1 + 4))),
    log (4) - (-(1*0.4222222 + 0)/(1 + 4)*log((1*0.4222222 + 0)/(1 + 4))
               +  -(1*0.1138889 + 0)/(1 + 4)*log((1*0.1138889 + 0)/(1 + 4))
               +  -(1*0.2111111 + 0)/(1 + 4)*log((1*0.2111111 + 0)/(1 + 4))
               +  -(1*0.2527778 + 4)/(1 + 4)*log((1*0.2527778 + 4)/(1 + 4)))};
  gsl_vector_view expected = gsl_vector_view_array (exp_v, win_len);
  assert_gsl_vector_eq_tol (col_ic, &expected.vector, TOL);
  gsl_vector_free (col_ic);
}
END_TEST


START_TEST(test_calc_col_relh)
{
  count_pos_res (&seq_wins, total_emp_counts, total_pos_counts,
                 pfm, bg_pos_freqs.pfm, false);
  calc_num_pos_pc (total_emp_counts, num_pos_pc, 1.0);
  calc_pos_freqs (pfm, total_pos_counts, bg_global_freqs, num_pos_pc,
                  0.0, BG_GLOBAL, PC_N_POS, PC_DIST_FREQ);
  calc_fc_score (pssm, pfm, bg_global_freqs, BG_GLOBAL, 2.0);
  gsl_vector *col_relh = gsl_vector_calloc (pfm->size2);
  if (!col_relh)
    error (EXIT_FAILURE, errno, "Memory exhausted");
  calc_col_relh (pssm, pfm, col_relh);
  double exp_v[] = {
    ((2*0.4222222 + 0)/(2 + 4)*log2((2*0.4222222 + 0)/(2 + 4)/0.4222222)
     + (2*0.1138889 + 2)/(2 + 4)*log2((2*0.1138889 + 2)/(2 + 4)/0.1138889)
     + (2*0.2111111 + 0)/(2 + 4)*log2((2*0.2111111 + 0)/(2 + 4)/0.2111111)
     + (2*0.2527778 + 2)/(2 + 4)*log2((2*0.2527778 + 2)/(2 + 4)/0.2527778)),
    ((2*0.4222222 + 0)/(2 + 4)*log2((2*0.4222222 + 0)/(2 + 4)/0.4222222)
     + (2*0.1138889 + 3)/(2 + 4)*log2((2*0.1138889 + 3)/(2 + 4)/0.1138889)
     + (2*0.2111111 + 0)/(2 + 4)*log2((2*0.2111111 + 0)/(2 + 4)/0.2111111)
     + (2*0.2527778 + 1)/(2 + 4)*log2((2*0.2527778 + 1)/(2 + 4)/0.2527778)),
    ((3*0.4222222 + 1)/(3 + 4)*log2((3*0.4222222 + 1)/(3 + 4)/0.4222222)
     + (3*0.1138889 + 2)/(3 + 4)*log2((3*0.1138889 + 2)/(3 + 4)/0.1138889)
     + (3*0.2111111 + 0)/(3 + 4)*log2((3*0.2111111 + 0)/(3 + 4)/0.2111111)
     + (3*0.2527778 + 1)/(3 + 4)*log2((3*0.2527778 + 1)/(3 + 4)/0.2527778)),
    ((1*0.4222222 + 4)/(1 + 4)*log2((1*0.4222222 + 4)/(1 + 4)/0.4222222)
     + (1*0.1138889 + 0)/(1 + 4)*log2((1*0.1138889 + 0)/(1 + 4)/0.1138889)
     + (1*0.2111111 + 0)/(1 + 4)*log2((1*0.2111111 + 0)/(1 + 4)/0.2111111)
     + (1*0.2527778 + 0)/(1 + 4)*log2((1*0.2527778 + 0)/(1 + 4)/0.2527778)),
    (1*0.4222222 + 0)/(1 + 4)*log2((1*0.4222222 + 0)/(1 + 4)/0.4222222)
    + (1*0.1138889 + 0)/(1 + 4)*log2((1*0.1138889 + 0)/(1 + 4)/0.1138889)
    + (1*0.2111111 + 0)/(1 + 4)*log2((1*0.2111111 + 0)/(1 + 4)/0.2111111)
    + (1*0.2527778 + 4)/(1 + 4)*log2((1*0.2527778 + 4)/(1 + 4)/0.2527778)};
  gsl_vector_view expected = gsl_vector_view_array (exp_v, win_len);
  assert_gsl_vector_eq_tol (col_relh, &expected.vector, TOL);
  gsl_vector_free (col_relh);
}
END_TEST


START_TEST(test_score_sequence)
{
  hid_t file_id = H5Fopen ("data/test-motif.h5", H5F_ACC_RDONLY, H5P_DEFAULT);
  if (file_id < 0)
    error (EXIT_FAILURE, errno, "Failed to open test PSSM file");
  motifstats motif;
  read_motif_stats (file_id, &motif);
  H5Fclose (file_id);
  double score = score_sequence ("ATTAC", motif.pssm, NULL, motif.type,
                                 SCORE_LOGLIK, false, NULL, 0);
  /* Yes, I know that adding a negative value is equivalent to
     subtraction.  I'm just explicitly showing the expected
     calculation here with reference to the above expected PSSM (in
     test_read_pssm) */
  double expected = -1.0 + -1.169925 + -2.584963 + -1.415037 + -1.0;
  ck_assert_double_eq_tol (score, expected, TOL);
  free_motif_stats (motif, true);
}
END_TEST


START_TEST(test_score_sequence_norm)
{
  hid_t file_id = H5Fopen ("data/test-motif.h5", H5F_ACC_RDONLY, H5P_DEFAULT);
  if (file_id < 0)
    error (EXIT_FAILURE, errno, "Failed to open test PSSM file");
  motifstats motif;
  read_motif_stats (file_id, &motif);
  H5Fclose (file_id);
  double score = score_sequence ("ATTAC", motif.pssm, motif.col_ic, motif.type,
                                 SCORE_LOGLIK, true, NULL, 0);
  /* Yes, I know that adding a negative value is equivalent to
     subtraction.  I'm just explicitly showing the expected
     calculation here with reference to the above expected PSSM (in
     test_read_pssm) */
  double min_score = -1.0 + -1.169925 + -2.584963 + -1.415037 + -1.0;
  double max_score = 1.519374 + 2.139930 + 4.073249 + 2.363405 + 1.519374;
  double expected = -1.0 + -1.169925 + -2.584963 + -1.415037 + -1.0;
  expected = (expected - min_score) / (max_score - min_score);
  ck_assert_double_eq_tol (score, expected, TOL);
  free_motif_stats (motif, true);
}
END_TEST


START_TEST(test_score_sequence_omit)
{
  hid_t file_id = H5Fopen ("data/test-motif.h5", H5F_ACC_RDONLY, H5P_DEFAULT);
  if (file_id < 0)
    error (EXIT_FAILURE, errno, "Failed to open test PSSM file");
  motifstats motif;
  read_motif_stats (file_id, &motif);
  H5Fclose (file_id);
  size_t omit[1] = {2};
  double score = score_sequence ("ATTAC", motif.pssm, motif.col_ic, motif.type,
                                 SCORE_LOGLIK, true, NULL, 0);
  /* Yes, I know that adding a negative value is equivalent to
     subtraction.  I'm just explicitly showing the expected
     calculation here with reference to the above expected PSSM (in
     test_read_pssm) */
  double min_score = -1.0 + -1.169925 + -1.415037 + -1.0;
  double max_score = 1.519374 + 2.139930 + 2.363405 + 1.519374;
  double expected = -1.0 + -1.169925 + -1.415037 + -1.0;
  expected = (expected - min_score) / (max_score - min_score);
  ck_assert_double_eq_tol (score, expected, TOL);
  free_motif_stats (motif, true);
}
END_TEST


START_TEST(test_pssm_frob_distance)
{
  gsl_matrix *m1 = gsl_matrix_calloc (3, 2);
  if (!m1)
    error (EXIT_FAILURE, errno, "Memory exhausted");
  gsl_matrix *m2 = gsl_matrix_calloc (3, 2);
  if (!m2)
    error (EXIT_FAILURE, errno, "Memory exhausted");
  gsl_matrix_set_all (m1, 1.0);
  gsl_matrix_set (m2, 0, 0, 1.0);
  gsl_matrix_set (m2, 1, 1, 1.0);
  gsl_matrix_set (m2, 2, 0, 1.0);
  double dist = pssm_frob_distance (m1, m2);
  gsl_matrix_free (m1);
  gsl_matrix_free (m2);
  ck_assert_double_eq_tol (dist, 1.7320508075688772, TOL);
}
END_TEST


Suite *
pssm_suite (void)
{
  Suite *s = suite_create ("PSSMs");

  TCase *tc_pos_bg = tcase_create ("position-specific background frequencies");
  tcase_add_test (tc_pos_bg, test_pos_bg);
  tcase_add_test (tc_pos_bg, test_pos_nuc_bg);
  suite_add_tcase (s, tc_pos_bg);

  TCase *tc_aa_counts = tcase_create ("empirical counts -- amino acids");
  tcase_add_checked_fixture (tc_aa_counts, aa_setup, aa_teardown);
  tcase_add_test (tc_aa_counts, test_total_emp_counts);
  tcase_add_test (tc_aa_counts, test_total_emp_counts_weighted);
  tcase_add_test (tc_aa_counts, test_total_pos_counts);
  tcase_add_test (tc_aa_counts, test_total_pos_counts_weighted);
  tcase_add_test (tc_aa_counts, test_emp_counts);
  tcase_add_test (tc_aa_counts, test_emp_counts_weighted);
  suite_add_tcase (s, tc_aa_counts);
  TCase *tc_aa_pcounts = tcase_create ("pseudocounts -- amino acids");
  tcase_add_checked_fixture (tc_aa_pcounts, aa_setup, aa_teardown);
  tcase_add_test (tc_aa_pcounts, test_calc_num_pos_pc);
  tcase_add_test (tc_aa_pcounts, test_calc_num_pos_pc2);
  tcase_add_test (tc_aa_pcounts, test_calc_pos_freqs_bg_global);
  tcase_add_test (tc_aa_pcounts, test_calc_pos_freqs_bg_pos);
  tcase_add_test (tc_aa_pcounts, test_calc_pos_freqs_const_equal);
  suite_add_tcase (s, tc_aa_pcounts);
  TCase *tc_aa_score = tcase_create ("scoring methods -- amino acids");
  tcase_add_checked_fixture (tc_aa_score, aa_setup, aa_teardown);
  tcase_add_test (tc_aa_score, test_calc_fc_score_pos);
  tcase_add_test (tc_aa_score, test_calc_fc_score_global);
  suite_add_tcase (s, tc_aa_score);

  TCase *tc_nuc_counts = tcase_create ("empirical counts -- nucleotides");
  tcase_add_checked_fixture (tc_nuc_counts, nuc_setup, nuc_teardown);
  tcase_add_test (tc_nuc_counts, test_nuc_total_emp_counts);
  tcase_add_test (tc_nuc_counts, test_nuc_total_pos_counts);
  tcase_add_test (tc_nuc_counts, test_nuc_emp_counts);
  suite_add_tcase (s, tc_nuc_counts);
  TCase *tc_nuc_pcounts = tcase_create ("pseudocounts -- nucleotides");
  tcase_add_checked_fixture (tc_nuc_pcounts, nuc_setup, nuc_teardown);
  tcase_add_test (tc_nuc_pcounts, test_nuc_calc_num_pos_pc);
  tcase_add_test (tc_nuc_pcounts, test_nuc_calc_num_pos_pc2);
  tcase_add_test (tc_nuc_pcounts, test_nuc_calc_pos_freqs_bg_global);
  tcase_add_test (tc_nuc_pcounts, test_nuc_calc_pos_freqs_bg_pos);
  tcase_add_test (tc_nuc_pcounts, test_nuc_calc_pos_freqs_const_equal);
  suite_add_tcase (s, tc_nuc_pcounts);
  TCase *tc_nuc_score = tcase_create ("scoring methods -- nucleotides");
  tcase_add_checked_fixture (tc_nuc_score, nuc_setup, nuc_teardown);
  tcase_add_test (tc_nuc_score, test_nuc_calc_fc_score_pos);
  tcase_add_test (tc_nuc_score, test_nuc_calc_fc_score_global);
  tcase_add_test (tc_nuc_score, test_nuc_calc_diff_score_pos);
  tcase_add_test (tc_nuc_score, test_nuc_calc_diff_score_global);
  suite_add_tcase (s, tc_nuc_score);

  TCase *tc_col_weights = tcase_create ("calculating column weights");
  tcase_add_checked_fixture (tc_col_weights, nuc_setup, nuc_teardown);
  tcase_add_test (tc_col_weights, test_calc_col_ic);
  tcase_add_test (tc_col_weights, test_calc_col_relh);
  suite_add_tcase (s, tc_col_weights);

  TCase *tc_seq_score = tcase_create ("calculating sequence scores");
  tcase_add_test (tc_seq_score, test_score_sequence);
  tcase_add_test (tc_seq_score, test_score_sequence_norm);
  tcase_add_test (tc_seq_score, test_score_sequence_omit);
  suite_add_tcase (s, tc_seq_score);

  TCase *tc_dist = tcase_create ("calculating PSSM distances");
  tcase_add_test (tc_dist, test_pssm_frob_distance);
  suite_add_tcase (s, tc_dist);

  return s;
}


int
main (void)
{
  int n_fail;
  Suite *s;
  SRunner *sr;

  s = pssm_suite ();
  sr = srunner_create (s);

  srunner_run_all (sr, CK_NORMAL);
  n_fail = srunner_ntests_failed (sr);
  srunner_free (sr);
  return (n_fail == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}
