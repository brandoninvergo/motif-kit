/* Copyright (C) 2019 Brandon Invergo <b.invergo@exeter.ac.uk> */

/* This program is free software: you can redistribute it and/or modify */
/* it under the terms of the GNU General Public License as published by */
/* the Free Software Foundation, either version 3 of the License, or */
/* (at your option) any later version. */

/* This program is distributed in the hope that it will be useful, */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the */
/* GNU General Public License for more details. */

/* You should have received a copy of the GNU General Public License */
/* along with this program.  If not, see <http://www.gnu.org/licenses/>. */


#ifndef TEST_UTIL_H
#define TEST_UTIL_H

#include <config.h>
#include <check.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_math.h>

void assert_gsl_matrix_eq_tol (gsl_matrix *m1, gsl_matrix *m2, double tol);
void assert_gsl_vector_eq_tol (gsl_vector *v1, gsl_vector *v2, double tol);

#endif
