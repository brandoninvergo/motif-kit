/* Copyright (C) 2019 Brandon Invergo <b.invergo@exeter.ac.uk> */

/* This program is free software: you can redistribute it and/or modify */
/* it under the terms of the GNU General Public License as published by */
/* the Free Software Foundation, either version 3 of the License, or */
/* (at your option) any later version. */

/* This program is distributed in the hope that it will be useful, */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the */
/* GNU General Public License for more details. */

/* You should have received a copy of the GNU General Public License */
/* along with this program.  If not, see <http://www.gnu.org/licenses/>. */


#include "test-util.h"

void
assert_gsl_matrix_eq_tol (gsl_matrix *m1, gsl_matrix *m2, double tol)
{
  ck_assert_uint_eq (m1->size1, m2->size1);
  ck_assert_uint_eq (m1->size2, m2->size2);
  for (size_t i=0; i<m1->size1; i++)
    {
      for (size_t j=0; j<m1->size2; j++)
        {
          double x = gsl_matrix_get (m1, i, j);
          double y = gsl_matrix_get (m2, i, j);
          ck_assert_msg ((!gsl_isnan (x) && !gsl_isnan (y)) ||
                         (gsl_isnan (x) && gsl_isnan (y)),
                         "x[%zd, %zd] == %f, y[%zd, %zd] == %f",
                           i, j, x, i, j, y);
          if (!gsl_isnan (x) && !gsl_isnan (y))
            ck_assert_msg (!gsl_fcmp (x, y, tol),
                           "x[%zd, %zd] == %f, y[%zd, %zd] == %f",
                           i, j, x, i, j, y);
        }
    }
}

void
assert_gsl_vector_eq_tol (gsl_vector *v1, gsl_vector *v2, double tol)
{
  ck_assert_uint_eq (v1->size, v2->size);
  for (size_t i=0; i<v1->size; i++)
    {
      double x = gsl_vector_get (v1, i);
      double y = gsl_vector_get (v2, i);
          ck_assert_msg ((!gsl_isnan (x) && !gsl_isnan (y)) ||
                         (gsl_isnan (x) && gsl_isnan (y)),
                         "x[%zd] == %f, y[%zd] == %f",
                           i, x, i, y);
          if (!gsl_isnan (x) && !gsl_isnan (y))
            ck_assert_msg (!gsl_fcmp (x, y, tol),
                           "x[%zd] == %f, y[%zd] == %f",
                           i, x, i, y);
    }
}
