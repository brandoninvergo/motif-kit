/* Copyright (C) 2019, 2020, 2021 Brandon Invergo <b.invergo@exeter.ac.uk> */

/* This program is free software: you can redistribute it and/or modify */
/* it under the terms of the GNU General Public License as published by */
/* the Free Software Foundation, either version 3 of the License, or */
/* (at your option) any later version. */

/* This program is distributed in the hope that it will be useful, */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the */
/* GNU General Public License for more details. */

/* You should have received a copy of the GNU General Public License */
/* along with this program.  If not, see <http://www.gnu.org/licenses/>. */


#include <config.h>
#include <check.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_vector.h>
#include "gl_linkedhash_list.h"
#include "gl_xlist.h"
#include "safe-alloc.h"

#include "../src/sequences.h"
#include "test-util.h"

#define TOL 1e-3


START_TEST(test_read_fasta)
{
  seq_set bg_seqs;
  read_sequences (&bg_seqs, "data/test.fasta", SEQ_GUESS);
  ck_assert_uint_eq (gl_list_size (bg_seqs.seqs), 3);
  ck_assert_uint_eq (bg_seqs.type, SEQ_AA);
  clear_global_seq_set (&bg_seqs);
}
END_TEST


START_TEST(test_read_nuc_fasta)
{
  seq_set bg_seqs;
  read_sequences (&bg_seqs, "data/test-nuc.fasta", SEQ_GUESS);
  ck_assert_uint_eq (gl_list_size (bg_seqs.seqs), 3);
  ck_assert_uint_eq (bg_seqs.type, SEQ_NUC);
  clear_global_seq_set (&bg_seqs);
}
END_TEST


START_TEST(test_global_bg)
{
  seq_set bg_seqs;
  read_sequences (&bg_seqs, "data/test.fasta", SEQ_GUESS);
  double exp_m[] = {0.0582192,        /* ARG */
                    0.00684932,       /* HIS */
                    0.0650685,        /* LYS */
                    0.0547945,        /* ASP */
                    0.0684932,        /* GLU */
                    0.0856164,        /* SER */
                    0.0342466,        /* THR */
                    0.0376712,        /* ASN */
                    0.0547945,        /* GLN */
                    0.0342466,        /* CYS */
                    0.0582192,        /* GLY */
                    0.0650685,        /* PRO */
                    0.0650685,        /* ALA */
                    0.0582192,        /* VAL */
                    0.0445205,        /* ILE */
                    0.0753425,        /* LEU */
                    0.0273973,        /* MET */
                    0.0273973,        /* PHE */
                    0.0410959,        /* TYR */
                    0.0376712};       /* TRP */
  gsl_matrix_view expected = gsl_matrix_view_array (exp_m, NUM_AA, 1);
  gsl_matrix *bg_freqs = calc_global_bg (bg_seqs);
  assert_gsl_matrix_eq_tol (bg_freqs, &expected.matrix, TOL);
  gsl_matrix_free (bg_freqs);
  clear_global_seq_set (&bg_seqs);
}
END_TEST

START_TEST(test_global_nuc_bg)
{
  seq_set bg_seqs;
  read_sequences (&bg_seqs, "data/test-nuc.fasta", SEQ_GUESS);
  double exp_m[] = {0.4222222,        /* A */
                    0.1138889,        /* C */
                    0.2111111,        /* G */
                    0.2527778};       /* T */
  gsl_matrix_view expected = gsl_matrix_view_array (exp_m, NUM_NUC, 1);
  gsl_matrix *bg_freqs = calc_global_bg (bg_seqs);
  assert_gsl_matrix_eq_tol (bg_freqs, &expected.matrix, TOL);
  gsl_matrix_free (bg_freqs);
  clear_global_seq_set (&bg_seqs);
}
END_TEST


Suite *
proteome_suite (void)
{
  Suite *s = suite_create ("sequences");

  TCase *tc_read = tcase_create ("read FASTA file");
  tcase_add_test (tc_read, test_read_fasta);
  tcase_add_test (tc_read, test_read_nuc_fasta);
  suite_add_tcase (s, tc_read);
  TCase *tc_dists = tcase_create ("global background distributions");
  tcase_add_test (tc_dists, test_global_bg);
  tcase_add_test (tc_dists, test_global_nuc_bg);
  suite_add_tcase (s, tc_dists);

  return s;
}


int main (void)
{
  Suite *s = proteome_suite ();
  SRunner *sr = srunner_create (s);

  srunner_run_all (sr, CK_NORMAL);
  int n_fail = srunner_ntests_failed (sr);
  srunner_free (sr);
  return (n_fail == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}

