/* Copyright (C) 2021 Brandon M. Invergo <b.invergo@exeter.ac.uk> */

/* This program is free software: you can redistribute it and/or modify */
/* it under the terms of the GNU General Public License as published by */
/* the Free Software Foundation, either version 3 of the License, or */
/* (at your option) any later version. */

/* This program is distributed in the hope that it will be useful, */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the */
/* GNU General Public License for more details. */

/* You should have received a copy of the GNU General Public License */
/* along with this program.  If not, see <http://www.gnu.org/licenses/>. */


#include <config.h>

#include <check.h>

#include <math.h>
#include <errno.h>
#include "error.h"
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_rng.h>
#include "gl_linkedhash_list.h"
#include "gl_xlist.h"
#include "safe-alloc.h"

#include "../src/seq-wins.h"
#include "../src/naive-bayes.h"
#include "test-util.h"

#define TOL 1e-3
#define NRAND 500000


START_TEST(test_read_features)
{
  seq_set seq_wins;
  size_t win_len;
  bool **bernoulli_dat = NULL;
  double **gaussian_dat = NULL;
  double **beta_dat = NULL;
  size_t n_bernoulli = 0;
  size_t n_gaussian = 0;
  size_t n_beta = 0;
  bayes_feat features[5] = {FEATURE_BERNOULLI, FEATURE_GAUSSIAN, FEATURE_BERNOULLI,
    FEATURE_GAUSSIAN, FEATURE_BETA};
  win_len = read_features (&seq_wins, &bernoulli_dat, &gaussian_dat, &beta_dat,
                           "data/test-features.tsv", features, 5,
                           &n_bernoulli, &n_gaussian, &n_beta, SEQ_GUESS, '\t',
                           NULL);
  ck_assert_uint_eq (gl_list_size (seq_wins.seqs), 5);
  ck_assert_uint_eq (win_len, 5);
  ck_assert_uint_eq (seq_wins.type, SEQ_AA);
  for (size_t i=0; i<gl_list_size (seq_wins.seqs); i++)
    {
      seq_win *win = (seq_win *) gl_list_get_at (seq_wins.seqs, i);
      ck_assert_uint_eq (strlen (win->seq), win_len);
      ck_assert_double_eq_tol (win->weight, 1.0, TOL);
    }
  ck_assert_uint_eq (n_bernoulli, 2);
  ck_assert_uint_eq (n_gaussian, 2);
  ck_assert_uint_eq (n_beta, 1);
  ck_assert_uint_eq (bernoulli_dat[0][0], true);
  ck_assert_uint_eq (bernoulli_dat[0][2], false);
  ck_assert_uint_eq (bernoulli_dat[1][0], true);
  ck_assert_uint_eq (bernoulli_dat[1][1], false);
  ck_assert_double_eq_tol (gaussian_dat[0][0], 1.0, TOL);
  ck_assert_double_eq_tol (gaussian_dat[0][4], 5.0, TOL);
  ck_assert_double_eq_tol (gaussian_dat[1][0], 0.1, TOL);
  ck_assert_double_eq_tol (gaussian_dat[1][4], 0.5, TOL);
  ck_assert_double_eq_tol (beta_dat[0][0], 0.2, TOL);
  ck_assert_double_eq_tol (beta_dat[0][4], 0.9, TOL);
  clear_seq_win_set (&seq_wins);
  for (size_t i=0; i<n_bernoulli; i++)
    {
      FREE (bernoulli_dat[i]);
    }
  FREE (bernoulli_dat);
  for (size_t i=0; i<n_gaussian; i++)
    {
      FREE (gaussian_dat[i]);
    }
  FREE (gaussian_dat);
  for (size_t i=0; i<n_beta; i++)
    {
      FREE (beta_dat[i]);
    }
  FREE (beta_dat);
}
END_TEST


START_TEST(test_bernoulli_param)
{
  bool bernoulli_dat[][5] = {{true, true, false, false, true},
                             {true, false, false, false, true}};
  double p;
  p = bernoulli_param (bernoulli_dat[0], 5, 1.0);
  ck_assert_double_eq_tol (p, log10 ((3.0 + 1.0) / (5.0 + 2.0)), TOL);
  /* Without smoothing */
  p = bernoulli_param (bernoulli_dat[1], 5, 0.0);
  ck_assert_double_eq_tol (p, log10 (0.4), TOL);
}
END_TEST


START_TEST(test_gaussian_param)
{
  double gaussian_dat[] = {1, 2, 3, 4, 5};
  gaussian_dist d;
  d = gaussian_param (gaussian_dat, 5);
  ck_assert_double_eq_tol (d.mean, 3.0, TOL);
  ck_assert_double_eq_tol (d.sd, 1.581139, TOL);
}
END_TEST


START_TEST(test_beta_param)
{
  double beta_dat[NRAND];
  gsl_rng *r = gsl_rng_alloc (gsl_rng_default);
  for (size_t i=0; i<NRAND; i++)
    {
      beta_dat[i] = gsl_ran_beta (r, 0.1, 0.5);
    }
  gsl_rng_free (r);
  beta_dist d;
  d = beta_param (beta_dat, NRAND, false, 0.5);
  /* This is an MLE estimate, so chances are we'll be off the target
     by a bit more than the usual tolerance */
  ck_assert_double_eq_tol (d.a, 0.1, 1e-2);
  ck_assert_double_eq_tol (d.b, 0.5, 1e-2);
}
END_TEST


START_TEST(test_score_bernoulli)
{
  ck_assert_double_eq_tol (score_bernoulli (true, 0.3), 0.3, TOL);
  ck_assert_double_eq_tol (score_bernoulli (false, 0.3), 0.7, TOL);
}
END_TEST


START_TEST(test_score_gaussian)
{
  gaussian_dist d = {.mean = 0, .sd = 1};
  ck_assert_double_eq_tol (score_gaussian (1, d), log10 (0.2419707), TOL);
  d.mean = 1;
  ck_assert_double_eq_tol (score_gaussian (2, d), log10 (0.2419707), TOL);
}
END_TEST


START_TEST(test_score_beta)
{
  beta_dist d = {.a = 0.1, .b = 0.1};
  ck_assert_double_eq_tol (score_beta (0.5, d), log10 (0.1766303), TOL);
}
END_TEST


START_TEST(test_score_naive_bayes)
{
  double prior = 0.3;
  double compl_prior = 0.7;
  double seq_likl = log10 (0.0005);
  double compl_seq_likl = log10 (0.0000001);
  double bernoulli_likl[] = {log10 (0.6), log10 (0.4)};
  double compl_bernoulli_likl[] = {log10 (0.4), log10 (0.6)};
  double gaussian_likl[] = {log10 (0.8)};
  double compl_gaussian_likl[] = {log10 (0.5)};
  double beta_likl[] = {log10 (0.1)};
  double compl_beta_likl[] = {log10 (0.25)};
  double expected = (0.3 * 0.0005 * 0.6 * 0.4 * 0.8 * 0.1) / (0.3 * 0.0005 * 0.6 * 0.4 * 0.8 * 0.1 + 0.7 * 0.0000001 * 0.4 * 0.6 * 0.5 * 0.25);
  double score = score_naive_bayes (seq_likl, compl_seq_likl, bernoulli_likl,
                                    compl_bernoulli_likl, 2, gaussian_likl,
                                    compl_gaussian_likl, 1, beta_likl,
                                    compl_beta_likl, 1, prior, compl_prior);
  ck_assert_double_eq_tol (score, expected, TOL);
}
END_TEST


START_TEST(test_score_naive_bayes_no_feats)
{
  double prior = 0.3;
  double compl_prior = 0.7;
  double seq_likl = log10 (0.0005);
  double compl_seq_likl = log10 (0.0000001);
  double *bernoulli_likl = NULL;
  double *compl_bernoulli_likl = NULL;
  double *gaussian_likl = NULL;
  double *compl_gaussian_likl = NULL;
  double *beta_likl = NULL;
  double *compl_beta_likl = NULL;
  double expected = (0.3 * 0.0005) / (0.3 * 0.0005 + 0.7 * 0.0000001);
  double score = score_naive_bayes (seq_likl, compl_seq_likl, bernoulli_likl,
                                    compl_bernoulli_likl, 0, gaussian_likl,
                                    compl_gaussian_likl, 0, beta_likl,
                                    compl_beta_likl, 0, prior, compl_prior);
  ck_assert_double_eq_tol (score, expected, TOL);
}
END_TEST


Suite *
motif_stats_suite (void)
{
  Suite *s = suite_create ("Naive Bayes");

  TCase *tc_feat_file = tcase_create ("Reading features");
  tcase_add_test (tc_feat_file, test_read_features);
  suite_add_tcase (s, tc_feat_file);

  TCase *tc_params = tcase_create ("Calculating parameters");
  tcase_add_test (tc_params, test_bernoulli_param);
  tcase_add_test (tc_params, test_gaussian_param);
  tcase_add_test (tc_params, test_beta_param);
  suite_add_tcase (s, tc_params);

  TCase *tc_likl = tcase_create ("Calculating Naive Bayes feature likelihoods");
  tcase_add_test (tc_likl, test_score_bernoulli);
  tcase_add_test (tc_likl, test_score_gaussian);
  tcase_add_test (tc_likl, test_score_beta);
  suite_add_tcase (s, tc_likl);

  TCase *tc_score = tcase_create ("Calculating Naive Bayes posterior probability");
  tcase_add_test (tc_score, test_score_naive_bayes);
  tcase_add_test (tc_score, test_score_naive_bayes_no_feats);
  suite_add_tcase (s, tc_score);

  return s;
}


int
main (void)
{
  int n_fail;
  Suite *s;
  SRunner *sr;

  s = motif_stats_suite ();
  sr = srunner_create (s);

  srunner_run_all (sr, CK_NORMAL);
  n_fail = srunner_ntests_failed (sr);
  srunner_free (sr);
  return (n_fail == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}

