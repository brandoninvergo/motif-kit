/* Copyright (C) 2019, 2020, 2021 Brandon Invergo <b.invergo@exeter.ac.uk> */

/* This program is free software: you can redistribute it and/or modify */
/* it under the terms of the GNU General Public License as published by */
/* the Free Software Foundation, either version 3 of the License, or */
/* (at your option) any later version. */

/* This program is distributed in the hope that it will be useful, */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the */
/* GNU General Public License for more details. */

/* You should have received a copy of the GNU General Public License */
/* along with this program.  If not, see <http://www.gnu.org/licenses/>. */


#include <config.h>
#include <check.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_vector.h>
#include "gl_linkedhash_list.h"
#include "gl_xlist.h"
#include "safe-alloc.h"

#include "../src/seq-wins.h"
#include "../src/sequence.h"
#include "test-util.h"

#define TOL 1e-3


seq_set res_weight_seq_wins;
gsl_vector *pos_res_counts;
gsl_matrix *res_counts;
gsl_matrix *res_weights;

seq_set nuc_weight_seq_wins;
gsl_vector *pos_nuc_counts;
gsl_matrix *nuc_counts;
gsl_matrix *nuc_weights;

void
aa_weight_setup (void)
{
  res_weight_seq_wins.seqs = gl_list_nx_create_empty (GL_LINKEDHASH_LIST, seq_wins_equal,
                                                      seq_win_hash, seq_win_dispose, false);
  if (!res_weight_seq_wins.seqs)
    error (EXIT_FAILURE, errno, "Memory exhausted");
  /* Use the example sequences from Henikoff & Henikoff 1994 */
  seq_win *seq1;
  if (ALLOC (seq1))
    error (EXIT_FAILURE, errno, "Memory exhausted");
  if (ALLOC_N (seq1->seq, 6))
      error (EXIT_FAILURE, errno, "Memory exhausted");
  strcpy (seq1->seq, "GYVGS");
  seq1->weight = 1.0;
  seq_win *seq2;
  if (ALLOC (seq2))
    error (EXIT_FAILURE, errno, "Memory exhausted");
  if (ALLOC_N (seq2->seq, 6))
      error (EXIT_FAILURE, errno, "Memory exhausted");
  strcpy (seq2->seq, "GFDGF");
  seq2->weight = 1.0;
  seq_win *seq3;
  if (ALLOC (seq3))
    error (EXIT_FAILURE, errno, "Memory exhausted");
  if (ALLOC_N (seq3->seq, 6))
      error (EXIT_FAILURE, errno, "Memory exhausted");
  strcpy (seq3->seq, "GYDGF");
  seq3->weight = 1.0;
  seq_win *seq4;
  if (ALLOC (seq4))
    error (EXIT_FAILURE, errno, "Memory exhausted");
  if (ALLOC_N (seq4->seq, 6))
      error (EXIT_FAILURE, errno, "Memory exhausted");
  strcpy (seq4->seq, "GYQGG");
  seq4->weight = 1.0;
  if (!gl_list_nx_add_last (res_weight_seq_wins.seqs, (void *)seq1))
    error (EXIT_FAILURE, errno, "Memory exhausted");
  if (!gl_list_nx_add_last (res_weight_seq_wins.seqs, (void *)seq2))
    error (EXIT_FAILURE, errno, "Memory exhausted");
  if (!gl_list_nx_add_last (res_weight_seq_wins.seqs, (void *)seq3))
    error (EXIT_FAILURE, errno, "Memory exhausted");
  if (!gl_list_nx_add_last (res_weight_seq_wins.seqs, (void *)seq4))
    error (EXIT_FAILURE, errno, "Memory exhausted");
  res_weight_seq_wins.type = SEQ_AA;
  res_weight_seq_wins.win_len = 5;
  pos_res_counts = gsl_vector_calloc (res_weight_seq_wins.win_len);
  if (!pos_res_counts)
    error(EXIT_FAILURE, errno, "Memory exhausted");
  res_counts = gsl_matrix_calloc (NUM_AA, res_weight_seq_wins.win_len);
  if (!res_counts)
    error(EXIT_FAILURE, errno, "Memory exhausted");
  res_weights = gsl_matrix_calloc (NUM_AA, res_weight_seq_wins.win_len);
  if (!res_weights)
    error(EXIT_FAILURE, errno, "Memory exhausted");
}

void
nuc_weight_setup (void)
{
  nuc_weight_seq_wins.seqs = gl_list_nx_create_empty (GL_LINKEDHASH_LIST, seq_wins_equal,
                                                      seq_win_hash, seq_win_dispose, false);
  if (!nuc_weight_seq_wins.seqs)
    error (EXIT_FAILURE, errno, "Memory exhausted");
  /* Use the example sequences from Henikoff & Henikoff 1994 */
  seq_win *seq1;
  if (ALLOC (seq1))
    error (EXIT_FAILURE, errno, "Memory exhausted");
  if (ALLOC_N (seq1->seq, 6))
    error (EXIT_FAILURE, errno, "Memory exhausted");
  strcpy (seq1->seq, "TCAAT");
  seq1->weight = 1.0;
  seq_win *seq2;
  if (ALLOC (seq2))
    error (EXIT_FAILURE, errno, "Memory exhausted");
  if (ALLOC_N (seq2->seq, 6))
    error (EXIT_FAILURE, errno, "Memory exhausted");
  strcpy (seq2->seq, "CTCAT");
  seq2->weight = 1.0;
  seq_win *seq3;
  if (ALLOC (seq3))
    error (EXIT_FAILURE, errno, "Memory exhausted");
  if (ALLOC_N (seq3->seq, 6))
    error (EXIT_FAILURE, errno, "Memory exhausted");
  strcpy (seq3->seq, "TCCAT");
  seq3->weight = 1.0;
  seq_win *seq4;
  if (ALLOC (seq4))
    error (EXIT_FAILURE, errno, "Memory exhausted");
  if (ALLOC_N (seq4->seq, 6))
    error (EXIT_FAILURE, errno, "Memory exhausted");
  strcpy (seq4->seq, "CCTAT");
  seq4->weight = 1.0;
  if (!gl_list_add_last (nuc_weight_seq_wins.seqs, (void *)seq1))
    error (EXIT_FAILURE, errno, "Memory exhausted");
  if (!gl_list_add_last (nuc_weight_seq_wins.seqs, (void *)seq2))
    error (EXIT_FAILURE, errno, "Memory exhausted");
  if (!gl_list_add_last (nuc_weight_seq_wins.seqs, (void *)seq3))
    error (EXIT_FAILURE, errno, "Memory exhausted");
  if (!gl_list_add_last (nuc_weight_seq_wins.seqs, (void *)seq4))
    error (EXIT_FAILURE, errno, "Memory exhausted");
  nuc_weight_seq_wins.type = SEQ_NUC;
  nuc_weight_seq_wins.win_len = 5;
  pos_nuc_counts = gsl_vector_calloc (nuc_weight_seq_wins.win_len);
  if (!pos_nuc_counts)
    error(EXIT_FAILURE, errno, "Memory exhausted");
  nuc_counts = gsl_matrix_calloc (NUM_NUC, nuc_weight_seq_wins.win_len);
  if (!nuc_counts)
    error(EXIT_FAILURE, errno, "Memory exhausted");
  nuc_weights = gsl_matrix_calloc (NUM_NUC, nuc_weight_seq_wins.win_len);
  if (!nuc_weights)
    error(EXIT_FAILURE, errno, "Memory exhausted");
}


void
aa_weight_teardown (void)
{
  gl_list_free (res_weight_seq_wins.seqs);
  gsl_vector_free (pos_res_counts);
  gsl_matrix_free (res_counts);
  gsl_matrix_free (res_weights);
}


void
nuc_weight_teardown (void)
{
  gl_list_free (nuc_weight_seq_wins.seqs);
  gsl_vector_free (pos_nuc_counts);
  gsl_matrix_free (nuc_counts);
  gsl_matrix_free (nuc_weights);
}


START_TEST(test_read_seq_wins)
{
  seq_set seq_wins;
  size_t win_len = read_seq_wins (&seq_wins, "data/test-seq-wins.txt", SEQ_GUESS);
  ck_assert_uint_eq (gl_list_size (seq_wins.seqs), 5);
  ck_assert_uint_eq (win_len, 5);
  ck_assert_uint_eq (seq_wins.type, SEQ_AA);
  for (size_t i=0; i<gl_list_size (seq_wins.seqs); i++)
    {
      seq_win *win = (seq_win *) gl_list_get_at (seq_wins.seqs, i);
      ck_assert_uint_eq (strlen (win->seq), win_len);
      ck_assert_double_eq_tol (win->weight, 1.0, TOL);
    }
  clear_seq_win_set (&seq_wins);
}
END_TEST


START_TEST(test_read_nuc_seq_wins)
{
  seq_set seq_wins;
  size_t win_len = read_seq_wins (&seq_wins, "data/test-nuc-seq-wins.txt", SEQ_GUESS);
  ck_assert_uint_eq (gl_list_size (seq_wins.seqs), 4);
  ck_assert_uint_eq (win_len, 5);
  ck_assert_uint_eq (seq_wins.type, SEQ_NUC);
  for (size_t i=0; i<gl_list_size (seq_wins.seqs); i++)
    {
      seq_win *win = (seq_win *) gl_list_get_at (seq_wins.seqs, i);
      ck_assert_uint_eq (strlen (win->seq), win_len);
      ck_assert_double_eq_tol (win->weight, 1.0, TOL);
    }
  clear_seq_win_set (&seq_wins);
}
END_TEST


START_TEST(test_count_res)
{
  double exp_m[] = {0.0, 0.0, 0.0, 0.0, 0.0, /* ARG */
                    0.0, 0.0, 0.0, 0.0, 0.0, /* HIS */
                    0.0, 0.0, 0.0, 0.0, 0.0, /* LYS */
                    0.0, 0.0, 2.0, 0.0, 0.0, /* ASP */
                    0.0, 0.0, 0.0, 0.0, 0.0, /* GLU */
                    0.0, 0.0, 0.0, 0.0, 1.0, /* SER */
                    0.0, 0.0, 0.0, 0.0, 0.0, /* THR */
                    0.0, 0.0, 0.0, 0.0, 0.0, /* ASN */
                    0.0, 0.0, 1.0, 0.0, 0.0, /* GLN */
                    0.0, 0.0, 0.0, 0.0, 0.0, /* CYS */
                    4.0, 0.0, 0.0, 4.0, 1.0, /* GLY */
                    0.0, 0.0, 0.0, 0.0, 0.0, /* PRO */
                    0.0, 0.0, 0.0, 0.0, 0.0, /* ALA */
                    0.0, 0.0, 1.0, 0.0, 0.0, /* VAL */
                    0.0, 0.0, 0.0, 0.0, 0.0, /* ILE */
                    0.0, 0.0, 0.0, 0.0, 0.0, /* LEU */
                    0.0, 0.0, 0.0, 0.0, 0.0, /* MET */
                    0.0, 1.0, 0.0, 0.0, 2.0, /* PHE */
                    0.0, 3.0, 0.0, 0.0, 0.0, /* TYR */
                    0.0, 0.0, 0.0, 0.0, 0.0}; /* TRP */
  gsl_matrix_view expected = gsl_matrix_view_array (exp_m, NUM_AA, 5);
  count_pos_residues (&res_weight_seq_wins, pos_res_counts, res_counts);
  assert_gsl_matrix_eq_tol (res_counts, &expected.matrix, TOL);
}
END_TEST


START_TEST(test_count_pos_res)
{
  double exp_v[] = {1.0, 2.0, 3.0, 1.0, 3.0};
  gsl_vector_view expected = gsl_vector_view_array (exp_v, 5);
  count_pos_residues (&res_weight_seq_wins, pos_res_counts, res_counts);
  assert_gsl_vector_eq_tol (pos_res_counts, &expected.vector, TOL);
}
END_TEST


START_TEST(test_calc_pos_res_weights)
{
  count_pos_residues (&res_weight_seq_wins, pos_res_counts, res_counts);
  double exp_m[] = {0.0,  0.0,     0.0,     0.0,  0.0,     /* ARG */
                    0.0,  0.0,     0.0,     0.0,  0.0,     /* HIS */
                    0.0,  0.0,     0.0,     0.0,  0.0,     /* LYS */
                    0.0,  0.0,     1.0/6.0, 0.0,  0.0,     /* ASP */
                    0.0,  0.0,     0.0,     0.0,  0.0,     /* GLU */
                    0.0,  0.0,     0.0,     0.0,  1.0/3.0, /* SER */
                    0.0,  0.0,     0.0,     0.0,  0.0,     /* THR */
                    0.0,  0.0,     0.0,     0.0,  0.0,     /* ASN */
                    0.0,  0.0,     1.0/3.0, 0.0,  0.0,     /* GLN */
                    0.0,  0.0,     0.0,     0.0,  0.0,     /* CYS */
                    0.25, 0.0,     0.0,     0.25, 1.0/3.0, /* GLY */
                    0.0,  0.0,     0.0,     0.0,  0.0,     /* PRO */
                    0.0,  0.0,     0.0,     0.0,  0.0,     /* ALA */
                    0.0,  0.0,     1.0/3.0, 0.0,  0.0,     /* VAL */
                    0.0,  0.0,     0.0,     0.0,  0.0,     /* ILE */
                    0.0,  0.0,     0.0,     0.0,  0.0,     /* LEU */
                    0.0,  0.0,     0.0,     0.0,  0.0,     /* MET */
                    0.0,  0.5,     0.0,     0.0,  1.0/6.0, /* PHE */
                    0.0,  1.0/6.0, 0.0,     0.0,  0.0,     /* TYR */
                    0.0,  0.0,     0.0,     0.0,  0.0};    /* TRP */
  gsl_matrix_view expected = gsl_matrix_view_array (exp_m, NUM_AA, 5);
  calc_pos_res_weights (&res_weight_seq_wins, pos_res_counts, res_counts,
                        res_weights);
  assert_gsl_matrix_eq_tol (res_weights, &expected.matrix, TOL);
}
END_TEST


START_TEST(test_calc_seq_weights)
{
  count_pos_residues (&res_weight_seq_wins, pos_res_counts, res_counts);
  calc_pos_res_weights (&res_weight_seq_wins, pos_res_counts, res_counts,
                        res_weights);
  double exp_v[] = {4.0/3.0, 4.0/3.0, 1.0, 4.0/3.0};
  gsl_vector_view expected = gsl_vector_view_array (exp_v, 4);
  double weight_sum = calc_seq_weights (&res_weight_seq_wins, res_weights);
  gsl_vector *weights = gsl_vector_calloc (4);
  if (!weights)
    error (EXIT_FAILURE, errno, "Memory exhausted");
  for (size_t i=0; i<gl_list_size (res_weight_seq_wins.seqs); i++)
    {
      seq_win *win = (seq_win *) gl_list_get_at (res_weight_seq_wins.seqs, i);
      if (!strcmp (win->seq, "GYVGS"))
        gsl_vector_set (weights, 0, win->weight);
      else if (!strcmp (win->seq, "GFDGF"))
        gsl_vector_set (weights, 1, win->weight);
      else if (!strcmp (win->seq, "GYDGF"))
        gsl_vector_set (weights, 2, win->weight);
      else if (!strcmp (win->seq, "GYQGG"))
        gsl_vector_set (weights, 3, win->weight);
    }
  assert_gsl_vector_eq_tol (weights, &expected.vector, TOL);
  gsl_vector_free (weights);
  ck_assert_double_eq_tol (weight_sum, 5.0, TOL);
}
END_TEST


START_TEST(test_normalize_weights)
{
  count_pos_residues (&res_weight_seq_wins, pos_res_counts, res_counts);
  calc_pos_res_weights (&res_weight_seq_wins, pos_res_counts, res_counts,
                        res_weights);
  double weight_sum = calc_seq_weights (&res_weight_seq_wins, res_weights);
  double exp_v[] = {0.267, 0.267, 0.2, 0.267};
  gsl_vector_view expected = gsl_vector_view_array (exp_v, 4);
  normalize_weights (&res_weight_seq_wins, weight_sum);
  gsl_vector *weights = gsl_vector_calloc (4);
  if (!weights)
    error (EXIT_FAILURE, errno, "Memory exhausted");
  for (size_t i=0; i<gl_list_size (res_weight_seq_wins.seqs); i++)
    {
      seq_win *win = (seq_win *) gl_list_get_at (res_weight_seq_wins.seqs, i);
      if (!strcmp (win->seq, "GYVGS"))
        gsl_vector_set (weights, 0, win->weight);
      else if (!strcmp (win->seq, "GFDGF"))
        gsl_vector_set (weights, 1, win->weight);
      else if (!strcmp (win->seq, "GYDGF"))
        gsl_vector_set (weights, 2, win->weight);
      else if (!strcmp (win->seq, "GYQGG"))
        gsl_vector_set (weights, 3, win->weight);
    }
  assert_gsl_vector_eq_tol (weights, &expected.vector, TOL);
  gsl_vector_free (weights);
}
END_TEST


START_TEST(test_weight_seq_windows)
{
  weight_seq_windows (&res_weight_seq_wins);
  double exp_v[] = {0.267, 0.267, 0.2, 0.267};
  gsl_vector_view expected = gsl_vector_view_array (exp_v, 4);
  gsl_vector *weights = gsl_vector_calloc (4);
  if (!weights)
    error (EXIT_FAILURE, errno, "Memory exhausted");
  for (size_t i=0; i<gl_list_size (res_weight_seq_wins.seqs); i++)
    {
      seq_win *win = (seq_win *) gl_list_get_at (res_weight_seq_wins.seqs, i);
      if (!strcmp (win->seq, "GYVGS"))
        gsl_vector_set (weights, 0, win->weight);
      else if (!strcmp (win->seq, "GFDGF"))
        gsl_vector_set (weights, 1, win->weight);
      else if (!strcmp (win->seq, "GYDGF"))
        gsl_vector_set (weights, 2, win->weight);
      else if (!strcmp (win->seq, "GYQGG"))
        gsl_vector_set (weights, 3, win->weight);
    }
  assert_gsl_vector_eq_tol (weights, &expected.vector, TOL);
  gsl_vector_free (weights);
}
END_TEST


START_TEST(test_count_nuc)
{
  double exp_m[] = {0.0, 0.0, 1.0, 4.0, 0.0, /* A */
                    2.0, 3.0, 2.0, 0.0, 0.0, /* C */
                    0.0, 0.0, 0.0, 0.0, 0.0, /* G */
                    2.0, 1.0, 1.0, 0.0, 4.0}; /* T */
  gsl_matrix_view expected = gsl_matrix_view_array (exp_m, NUM_NUC, 5);
  count_pos_residues (&nuc_weight_seq_wins, pos_nuc_counts, nuc_counts);
  assert_gsl_matrix_eq_tol (nuc_counts, &expected.matrix, TOL);
}
END_TEST


START_TEST(test_count_pos_nuc)
{
  double exp_v[] = {2.0, 2.0, 3.0, 1.0, 1.0};
  gsl_vector_view expected = gsl_vector_view_array (exp_v, 5);
  count_pos_residues (&nuc_weight_seq_wins, pos_nuc_counts, nuc_counts);
  assert_gsl_vector_eq_tol (pos_nuc_counts, &expected.vector, TOL);
}
END_TEST


START_TEST(test_calc_pos_nuc_weights)
{
  count_pos_residues (&nuc_weight_seq_wins, pos_nuc_counts, nuc_counts);
  double exp_m[] = {0.0,   0.0,     1.0/3.0, 0.25, 0.0,     /* A */
                    0.25,  1.0/6.0, 1.0/6.0, 0.0,  0.0,     /* C */
                    0.0,   0.0,     0.0,     0.0,  0.0,     /* G */
                    0.25,  0.5,     1.0/3.0, 0.0,  0.25};    /* T */
  gsl_matrix_view expected = gsl_matrix_view_array (exp_m, NUM_NUC, 5);
  calc_pos_res_weights (&nuc_weight_seq_wins, pos_nuc_counts, nuc_counts,
                        nuc_weights);
  assert_gsl_matrix_eq_tol (nuc_weights, &expected.matrix, TOL);
}
END_TEST


START_TEST(test_calc_nuc_seq_weights)
{
  count_pos_residues (&nuc_weight_seq_wins, pos_nuc_counts, nuc_counts);
  calc_pos_res_weights (&nuc_weight_seq_wins, pos_nuc_counts, nuc_counts,
                        nuc_weights);
  double exp_v[] = {0.25 + 1.0/6.0 + 1.0/3.0 + 0.25 + 0.25, /* TCAAT */
                    0.25 + 0.5 + 1.0/6.0 + 0.25 + 0.25,     /* CTCAT */
                    0.25 + 1.0/6.0 + 1.0/6.0 + 0.25 + 0.25, /* TCCAT */
                    0.25 + 1.0/6.0 + 1.0/3.0 + 0.25 + 0.25};/* CCTAT */
  gsl_vector_view expected = gsl_vector_view_array (exp_v, 4);
  double weight_sum = calc_seq_weights (&nuc_weight_seq_wins, nuc_weights);
  gsl_vector *weights = gsl_vector_calloc (4);
  if (!weights)
    error (EXIT_FAILURE, errno, "Memory exhausted");
  for (size_t i=0; i<gl_list_size (nuc_weight_seq_wins.seqs); i++)
    {
      seq_win *win = (seq_win *) gl_list_get_at (nuc_weight_seq_wins.seqs, i);
      if (!strcmp (win->seq, "TCAAT"))
        gsl_vector_set (weights, 0, win->weight);
      else if (!strcmp (win->seq, "CTCAT"))
        gsl_vector_set (weights, 1, win->weight);
      else if (!strcmp (win->seq, "TCCAT"))
        gsl_vector_set (weights, 2, win->weight);
      else if (!strcmp (win->seq, "CCTAT"))
        gsl_vector_set (weights, 3, win->weight);
    }
  assert_gsl_vector_eq_tol (weights, &expected.vector, TOL);
  gsl_vector_free (weights);
  ck_assert_double_eq_tol (weight_sum, 5.0, TOL);
}
END_TEST


START_TEST(test_weight_nuc_seq_windows)
{
  weight_seq_windows (&nuc_weight_seq_wins);
  double exp_v[] = {0.25, 0.2833, 0.2167, 0.25};
  gsl_vector_view expected = gsl_vector_view_array (exp_v, 4);
  gsl_vector *weights = gsl_vector_calloc (4);
  if (!weights)
    error (EXIT_FAILURE, errno, "Memory exhausted");
  for (size_t i=0; i<gl_list_size (nuc_weight_seq_wins.seqs); i++)
    {
      seq_win *win = (seq_win *) gl_list_get_at (nuc_weight_seq_wins.seqs, i);
      if (!strcmp (win->seq, "TCAAT"))
        gsl_vector_set (weights, 0, win->weight);
      else if (!strcmp (win->seq, "CTCAT"))
        gsl_vector_set (weights, 1, win->weight);
      else if (!strcmp (win->seq, "TCCAT"))
        gsl_vector_set (weights, 2, win->weight);
      else if (!strcmp (win->seq, "CCTAT"))
        gsl_vector_set (weights, 3, win->weight);
    }
  assert_gsl_vector_eq_tol (weights, &expected.vector, TOL);
  gsl_vector_free (weights);
}
END_TEST


Suite *
seq_wins_suite (void)
{
  Suite *s = suite_create ("sequence windows");

  TCase *tc_read = tcase_create ("reading from files");
  tcase_add_test (tc_read, test_read_seq_wins);
  tcase_add_test (tc_read, test_read_nuc_seq_wins);
  suite_add_tcase (s, tc_read);

  TCase *tc_aa_weight = tcase_create ("amino acid sequence weighting");
  tcase_add_checked_fixture (tc_aa_weight, aa_weight_setup, aa_weight_teardown);
  tcase_add_test (tc_aa_weight, test_count_res);
  tcase_add_test (tc_aa_weight, test_count_pos_res);
  tcase_add_test (tc_aa_weight, test_calc_pos_res_weights);
  tcase_add_test (tc_aa_weight, test_calc_seq_weights);
  tcase_add_test (tc_aa_weight, test_normalize_weights);
  tcase_add_test (tc_aa_weight, test_weight_seq_windows);
  suite_add_tcase (s, tc_aa_weight);

  TCase *tc_nuc_weight = tcase_create ("nucleotide sequence weighting");
  tcase_add_checked_fixture (tc_nuc_weight, nuc_weight_setup, nuc_weight_teardown);
  tcase_add_test (tc_nuc_weight, test_count_nuc);
  tcase_add_test (tc_nuc_weight, test_count_pos_nuc);
  tcase_add_test (tc_nuc_weight, test_calc_pos_nuc_weights);
  tcase_add_test (tc_nuc_weight, test_calc_nuc_seq_weights);
  tcase_add_test (tc_nuc_weight, test_weight_nuc_seq_windows);
  suite_add_tcase (s, tc_nuc_weight);

  return s;
}


int main (void)
{
  int n_fail;
  Suite *s;
  SRunner *sr;

  s = seq_wins_suite ();
  sr = srunner_create (s);

  srunner_run_all (sr, CK_NORMAL);
  n_fail = srunner_ntests_failed (sr);
  srunner_free (sr);
  return (n_fail == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}
