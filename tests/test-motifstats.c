/* Copyright (C) 2019, 2020, 2021 Brandon Invergo <b.invergo@exeter.ac.uk> */

/* This program is free software: you can redistribute it and/or modify */
/* it under the terms of the GNU General Public License as published by */
/* the Free Software Foundation, either version 3 of the License, or */
/* (at your option) any later version. */

/* This program is distributed in the hope that it will be useful, */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the */
/* GNU General Public License for more details. */

/* You should have received a copy of the GNU General Public License */
/* along with this program.  If not, see <http://www.gnu.org/licenses/>. */


#include <config.h>

#include <check.h>

#include <math.h>
#include <errno.h>
#include "error.h"
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_vector.h>
#include <hdf5.h>

#include "../src/motifstats.h"
#include "../src/pssm.h"
#include "test-util.h"

#define TOL 1e-3


START_TEST(test_read_motif_stats)
{
  hid_t file_id = H5Fopen ("data/test-motif.h5", H5F_ACC_RDONLY, H5P_DEFAULT);
  if (file_id < 0)
    error (EXIT_FAILURE, errno, "Failed to open test PSSM file");
  motifstats motif;
  read_motif_stats (file_id, &motif);
  H5Fclose (file_id);
  ck_assert_uint_eq (motif.pssm->size1, 20);
  ck_assert_uint_eq (motif.pssm->size2, 5);
  ck_assert_uint_eq (motif.pfm->size1, 20);
  ck_assert_uint_eq (motif.pfm->size2, 5);
  free_motif_stats (motif, true);
}
END_TEST


START_TEST(test_read_motif_stats_feats)
{
  hid_t file_id = H5Fopen ("data/test-motif-feats.h5", H5F_ACC_RDONLY, H5P_DEFAULT);
  if (file_id < 0)
    error (EXIT_FAILURE, errno, "Failed to open test PSSM file");
  motifstats motif;
  read_motif_stats (file_id, &motif);
  H5Fclose (file_id);
  ck_assert_uint_eq (motif.pssm->size1, 20);
  ck_assert_uint_eq (motif.pssm->size2, 5);
  ck_assert_uint_eq (motif.pfm->size1, 20);
  ck_assert_uint_eq (motif.pfm->size2, 5);
  ck_assert_uint_eq (motif.n_feats, 5);
  ck_assert_uint_eq (motif.n_bernoulli, 2);
  ck_assert_uint_eq (motif.n_gaussian, 2);
  ck_assert_uint_eq (motif.n_beta, 1);
  ck_assert_double_eq_tol (motif.bernoulli_feats[0], log10(0.6), TOL);
  ck_assert_double_eq_tol (motif.bernoulli_feats[1], log10(0.4), TOL);
  ck_assert_double_eq_tol (motif.gaussian_feats[0].mean, 3.0, TOL);
  ck_assert_double_eq_tol (motif.gaussian_feats[1].mean, 0.3, TOL);
  ck_assert_double_eq_tol (motif.gaussian_feats[0].sd, 1.581139, TOL);
  ck_assert_double_eq_tol (motif.gaussian_feats[1].sd, 0.1581139, TOL);
  ck_assert_double_eq_tol (motif.beta_feats[0].a, 0.9023226, TOL);
  ck_assert_double_eq_tol (motif.beta_feats[0].b, 0.7686452, TOL);
  free_motif_stats (motif, true);
}
END_TEST


Suite *
motif_stats_suite (void)
{
  Suite *s = suite_create ("motifstats structs");

  TCase *tc_store = tcase_create ("motifstats storage");
  tcase_add_test (tc_store, test_read_motif_stats);
  tcase_add_test (tc_store, test_read_motif_stats_feats);
  suite_add_tcase (s, tc_store);

  return s;
}


int
main (void)
{
  int n_fail;
  Suite *s;
  SRunner *sr;

  s = motif_stats_suite ();
  sr = srunner_create (s);

  srunner_run_all (sr, CK_NORMAL);
  n_fail = srunner_ntests_failed (sr);
  srunner_free (sr);
  return (n_fail == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}
