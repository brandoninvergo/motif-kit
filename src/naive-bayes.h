/* Copyright (C) 2021 Brandon M. Invergo <b.invergo@exeter.ac.uk> */

/* This program is free software: you can redistribute it and/or modify */
/* it under the terms of the GNU General Public License as published by */
/* the Free Software Foundation, either version 3 of the License, or */
/* (at your option) any later version. */

/* This program is distributed in the hope that it will be useful, */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the */
/* GNU General Public License for more details. */

/* You should have received a copy of the GNU General Public License */
/* along with this program.  If not, see <http://www.gnu.org/licenses/>. */

#include <config.h>

#include <errno.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <regex.h>
#include "error.h"
#include "math.h"
#include <float.h>
#include "gl_linkedhash_list.h"
#include "gl_xlist.h"
#include "safe-alloc.h"
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_statistics_double.h>
#include <gsl/gsl_randist.h>
#include <gsl/gsl_sf_psi.h>
#include <gsl/gsl_linalg.h>
#include <gsl/gsl_permutation.h>
#include <gsl/gsl_cdf.h>

#include "sequence.h"
#include "seq-wins.h"

#ifndef NAIVE_BAYES_H
#define NAIVE_BAYES_H

typedef enum bayes_feat {FEATURE_BERNOULLI, FEATURE_GAUSSIAN, FEATURE_BETA} bayes_feat;

typedef struct gaussian_dist {
  double mean;
  double sd;
} gaussian_dist;

typedef struct beta_dist {
  double a;
  double b;
} beta_dist;

size_t read_features (seq_set *seq_wins, bool ***bernoulli_counts,
                      double ***gaussian_dat, double ***beta_dat,
                      const char *feature_file, bayes_feat *feat_types,
                      size_t n_feats, size_t *n_bernoulli, size_t *n_gaussian,
                      size_t *n_beta, seq_type type, char delim,
                      seq_set *feat_exclude);
double bernoulli_param (const bool *dat, size_t n, double alpha);
gaussian_dist gaussian_param (const double *dat, size_t n);
beta_dist beta_param (const double *dat, size_t n, bool squeeze, double s);
double score_bernoulli (bool value, double p);
double score_gaussian (double value, gaussian_dist feat);
double score_beta (double value, beta_dist feat);
double score_naive_bayes (double seq_likl, double compl_seq_likl,
                          const double *bernoulli_likl,
                          const double *compl_bernoulli_likl,
                          size_t n_bernoulli, const double *gaussian_likl,
                          const double *compl_gaussian_likl, size_t n_gaussian,
                          const double *beta_likl, const double *compl_beta_likl,
                          size_t n_beta, double prior, double compl_prior);
#endif
