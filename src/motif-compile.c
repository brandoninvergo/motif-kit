/* Copyright (C) 2019, 2020, 2021 Brandon Invergo <b.invergo@exeter.ac.uk> */

/* This program is free software: you can redistribute it and/or modify */
/* it under the terms of the GNU General Public License as published by */
/* the Free Software Foundation, either version 3 of the License, or */
/* (at your option) any later version. */

/* This program is distributed in the hope that it will be useful, */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the */
/* GNU General Public License for more details. */

/* You should have received a copy of the GNU General Public License */
/* along with this program.  If not, see <http://www.gnu.org/licenses/>. */



#include <config.h>

#include <errno.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "argp.h"
#include "error.h"
#include <gsl/gsl_math.h>
#include "gl_linkedhash_list.h"
#include "gl_xlist.h"
#include "safe-alloc.h"

#include "motifstats.h"
#include "pssm.h"
#include "sequences.h"
#include "seq-wins.h"
#include "sequence.h"
#include "naive-bayes.h"


const char *argp_program_version = PACKAGE_STRING;
const char *argp_program_bug_address = PACKAGE_BUGREPORT;

static char doc[] =
  "motif-compile -- compile motif statistics"
  "\v"
  "This program compiles motif statistics, including a Position-Specific "
  "Scoring Matrix (PSSM), from the sequence windows in the file SEQFILE.  "
  "If SEQFILE isn't provided or is -, then the sequence windows will be read "
  "from stdin.  All statistics are stored in an HDF5-formatted file at OUTFILE."
  "\n\n"
  "--score-method=bayes requires --bg-type=pos and the provided --bg-file=FILE "
  "should consist of a superset of SEQFILE (that is, all sequences in SEQFILE "
  "should be in the background FILE).  You must also specify COMPLEMENT_OUTFILE, "
  "where the complementary motif (i.e. the motif of all the background sequences "
  "not in SEQFILE) will be stored."
  "\n\n"
  "For --score-method=bayes, SEQFILE should consist of two or more columns: an ID"
  "column, a sequence window column, plus any number of additional Bernoulli, Beta, or "
  "Gaussian-distributed feature columns (specified via the --features option).  "
  "For all other score methods, SEQFILE should contain only a single column of "
  "sequence windows.  All sequence windows should have the same length.";

static char args_doc[] = "SEQFILE OUTFILE [COMPLEMENT_OUTFILE]";

static struct argp_option options[] =
  {
   {0, 0, 0, 0, "Background", 1},
   {"bg-type", 'b', "TYPE", 0,
    "The type of background frequencies to use: 'none' (do not consider "
    "background frequencies; default), 'global' (the same background "
    "frequencies are applied to all columns), 'pos' (position-specific "
    "frequencies are applied)", 0},
   {"bg-file", 'f', "FILE", 0,
    "The file from which background frequencies should be calculated.  If "
    "--bg-type=global, FILE must be a FASTA file.  If --bg-type=pos, FILE "
    "must contain sequence windows (one sequence per line) of the same number "
    "of columns as the PSSM will have.", 0},
   {0, 0, 0, 0, "Pseudocounts", 2},
   {"pseudo-dist", 'd', "METHOD", 0,
    "How pseudocounts are distributed: 'equal' (pseudocounts distributed "
    "equally; default), 'freq' (pseudocounts are distributed according to "
    "a background frequency; requires option --bg-type to be 'global' or "
    "'pos' and is the default if a background is specified)", 0},
   {"pseudo-n", 'n', "METHOD", 0,
    "How the number of pseudocounts for each column is determined: 'const' "
    "(a constant number of pseudocounts are added to each column), "
    "'pos' (the number of pseudocounts increases with the empirical diversity "
    "of a column; default), 'none'", 0},
   {"pseudo-param", 'p', "NUM", 0,
    "A parameter (>= 0) for calculating the number of pseudocounts, depending "
    "on the method: for --pseudo-n=const, NUM is simply the number of "
    "pseudocounts (default: 1); for --pseudo-n=pos, NUM is multiplied by the "
    "diversity count (default: 1)", 0},
   {0, 0, 0, 0, "Methods", 3},
   {"no-weight-bg", 'v', 0, 0, "Do not perform sequence weighting on background "
    "sequences (for --bg-type=pos only)", 0},
   {"no-weight", 'w', 0, 0, "Do not perform sequence weighting", 0},
   {"ic-base", 301, "VALUE", 0, "Logarithmic base > 0.0 for calculating "
    "information content, determining the units of the measurement (e.g. 2: "
    "bits (default), e: nats).", 0},
   {"score-method", 's', "METHOD", 0,
    "The position-specific scoring method: 'loglik' (log-likelihood ratio, "
    "i.e. fold-change relative to background frequencies, log-transformed with "
    "base specified by --ic-base; default); 'diff' (scaled difference from "
    "background frequencies); 'freq' (likelihood, i.e. position-specific "
    "frequencies, log10-transformed); or 'bayes' (Naive Bayesian posterior "
    "probability -- see documentation below)", 0},
   {"diff-scale", 'l', "VALUE", 0, "Exponential scale value to apply to the "
    "absolute difference from the background frequencies in difference-based "
    "scoring (default: 1.2)", 0},
   {"name", 302, "NAME", 0, "A name to associate with the PSSM (e.g. protein "
    "kinase name.  This has no effect on the analyses and it is only a "
    "convenience.  If not specified, the name stored with the motif will be "
    "the name of the sequence window file", 0},
   {"print-weights", 303, 0, 0, "Print the sequence weights used in calculating "
    "frequencies.", 0},
   {"features", 304, "DISTRIBUTION[,DISTRIBUTION,...]", 0, "Read extra features "
    "from SEQ_FILE and the --bg-file FILE.  For each column in SEQ_FILE/FILE, a "
    "DISTRIBUTION is specified (in order), from which probabilities will be "
    "estimated.  Requires --score-method=bayes.  Available DISTRIBUTION values "
    "are: 'bernoulli', 'gaussian', 'beta'.", 0},
   {"feature-delim", 305, "CHAR", 0, "Character used to deliminate columns in "
    "SEQ_FILE and --bg-file FILE when other features are present.  Default: '\\t' "
    "(tab).", 0},
   {"smoothing", 306, "ALPHA", 0, "Additive smoothing parameter (i.e. constant "
    "pseudocount) for Bernoulli feature parameter estimation. Must be >=0 (0.0 "
    "means no smoothing). Default: 1.0.", 0},
   {"squeeze", 307, "S", OPTION_ARG_OPTIONAL, "Squeeze Beta-distributed data with "
    "0 or 1 values to be in the interval (0, 1).  The transformation is "
    "(x*(N - 1) + s)/N, where x is the value, N is the number of samples, and s "
    "is a value between 0 and 1. Default: 0.5", 0},
   {0}
  };

struct arguments
{
  char *args[3];
  pc_dist_method pseudo_dist;
  pc_n_method pseudo_n;
  score_method method;
  double pseudo_p;
  double diff_scale;
  bool weight;
  bool weight_bg;
  double ic_base;
  bg_type bg;
  char *bg_file;
  char *name;
  bool print_weights;
  char feat_delim;
  bayes_feat *features;
  size_t n_feats;
  double alpha;
  bool squeeze;
  double s;
};

static error_t
parse_opt (int key, char *arg, struct argp_state *state)
{
  struct arguments *arguments = state->input;
  switch  (key)
    {
    case 'b':
      if (!strcmp (arg, "none"))
        {
        arguments->bg = BG_NONE;
        }
      else if (!strcmp (arg, "global"))
        {
          arguments->bg = BG_GLOBAL;
        }
      else if (!strcmp (arg, "pos"))
        {
          arguments->bg = BG_POS;
        }
      else
        {
          argp_usage (state);
        }
      break;
    case 'd':
      if (!strcmp (arg, "equal"))
        {
        arguments->pseudo_dist = PC_DIST_EQUAL;
        }
      else if (!strcmp (arg, "freq"))
        {
          arguments->pseudo_dist = PC_DIST_FREQ;
        }
      else
        {
          argp_usage (state);
        }
      break;
    case 'f':
      if (!arg || strlen (arg) == 0)
        argp_usage (state);
      else
        {
          arguments->bg_file = arg;
          if (!arguments->bg_file)
            error (EXIT_FAILURE, errno, "Memory exhausted");
        }
      break;
    case 'l':
      if (!arg || strlen (arg) == 0)
        arguments->diff_scale = 1.2;
      else
        {
          arguments->diff_scale = strtod (arg, NULL);
          if (arguments->diff_scale == HUGE_VAL)
            error (EXIT_FAILURE, errno, "Overflow or underflow when parsing "
                   "the difference scale parameter");
          if (!arguments->diff_scale)
            argp_usage (state);
        }
      break;
    case 'n':
      if (!strcmp (arg, "const"))
        {
        arguments->pseudo_n = PC_N_CONST;
        }
      else if (!strcmp (arg, "pos"))
        {
          arguments->pseudo_n = PC_N_POS;
        }
      else if (!strcmp (arg, "none"))
        {
          arguments->pseudo_n = PC_N_NONE;
        }
      else
        {
          argp_usage (state);
        }
      break;
    case 'p':
      arguments->pseudo_p = strtod (arg, NULL);
      if (arguments->pseudo_p == HUGE_VAL)
        error (EXIT_FAILURE, errno, "Overflow or underflow when parsing "
               "the pseudocounts parameter");
      if (arguments->pseudo_p < 0)
        argp_usage (state);
      break;
    case 's':
      if (!arg || strlen (arg) == 0)
        arguments->method = SCORE_LOGLIK;
      else if (!strcmp (arg, "loglik"))
        {
          arguments->method = SCORE_LOGLIK;
        }
      else if (!strcmp (arg, "diff"))
        {
          arguments->method = SCORE_DIFF;
        }
      else if (!strcmp (arg, "freq"))
        {
          arguments->method = SCORE_FREQ;
        }
      else if (!strcmp (arg, "bayes"))
        {
          arguments->method = SCORE_BAYES;
        }
      else
        argp_usage (state);
      break;
    case 'v':
      arguments->weight_bg = false;
      break;
    case 'w':
      arguments->weight = false;
      break;
    case 301:
      if (!arg || strlen (arg) == 0 || !strcmp (arg, "e"))
        arguments->ic_base = M_E;
      else
        {
          arguments->ic_base = strtod (arg, NULL);
          if (arguments->ic_base <= 0.0)
            argp_usage (state);
        }
      break;
    case 302:
      arguments->name = arg;
      break;
    case 303:
      arguments->print_weights = true;
      break;
    case 304:
      {
        char *feature = strtok (arg, ",");
        if (feature == NULL)
          argp_usage (state);
        size_t max_feats = 3;
        if (ALLOC_N (arguments->features, max_feats))
          error (EXIT_FAILURE, errno, "Memory exhausted");
        size_t n_feats = 0;
        while (feature)
          {
            if (n_feats >= max_feats)
              {
                max_feats += 3;
                if (REALLOC_N (arguments->features, max_feats))
                  error (EXIT_FAILURE, errno, "Memory exhausted");
              }
            if (!strcmp (feature, "bernoulli"))
              arguments->features[n_feats++] = FEATURE_BERNOULLI;
            else if (!strcmp (feature, "gaussian"))
              arguments->features[n_feats++] = FEATURE_GAUSSIAN;
            else if (!strcmp (feature, "beta"))
              arguments->features[n_feats++] = FEATURE_BETA;
            else
              argp_usage (state);
            feature = strtok (NULL, ",");
          }
        arguments->n_feats = n_feats;
        break;
      }
    case 305:
      arguments->feat_delim = arg[0];
      break;
    case 306:
      arguments->alpha = strtod (arg, NULL);
      if (arguments->alpha == HUGE_VAL)
        error (EXIT_FAILURE, errno, "Overflow or underflow when parsing "
               "the alpha smoothing parameter");
      if (arguments->alpha < 0)
        argp_usage (state);
      break;
    case 307:
      arguments->squeeze = true;
      if (arg)
        {
          arguments->s = strtod (arg, NULL);
          if (arguments->s == HUGE_VAL)
            error (EXIT_FAILURE, errno, "Overflow or underflow when parsing "
                   "the Beta squeeze parameter");
          if (arguments->s < 0 || arguments->s > 1)
            argp_usage (state);
        }
      break;
    case ARGP_KEY_ARG:
      if (state->arg_num > 2)
        argp_usage (state);
      arguments->args[state->arg_num] = arg;
      break;
    case ARGP_KEY_END:
      break;
    default:
      return ARGP_ERR_UNKNOWN;
    }
  return 0;
}

static struct argp argp = {options, parse_opt, args_doc, doc};
char *fail_msg = NULL;


void
init_args (struct arguments *arguments)
{
  arguments->args[0] = NULL;
  arguments->args[1] = NULL;
  arguments->args[2] = NULL;
  arguments->pseudo_dist = PC_DIST_EQUAL;
  arguments->pseudo_n = PC_N_POS;
  arguments->pseudo_p = 1.0;
  /* Safei et al. use 1.2 as their chosen difference scale */
  arguments->diff_scale = 1.2;
  arguments->method = SCORE_LOGLIK;
  arguments->weight = true;
  arguments->weight_bg = true;
  arguments->ic_base = 2.0;
  arguments->bg = BG_NONE;
  arguments->bg_file = NULL;
  arguments->name = NULL;
  arguments->print_weights = false;
  arguments->feat_delim = '\t';
  arguments->features = NULL;
  arguments->n_feats = 0;
  arguments->alpha = 1.0;
  arguments->squeeze = false;
  arguments->s = 0.5;
}


void
print_seq_weights (seq_set *seq_wins)
{
  gl_list_iterator_t i = gl_list_iterator (seq_wins->seqs);
  const void *eltp;
  while (gl_list_iterator_next (&i, &eltp, NULL))
    {
      seq_win *win = (seq_win *) eltp;
      printf ("%s\t%f\n", win->seq, win->weight);
    }
  gl_list_iterator_free (&i);
}


void
setup_bg_motif (motifstats *bg_motif, seq_set *bg_seqs, bool **bernoulli_dat,
                double **gaussian_dat, double **beta_dat, size_t win_len,
                size_t n_bernoulli, size_t n_gaussian, size_t n_beta,
                size_t num_recs, struct arguments arguments)
{
  bg_motif->name = NULL;
  bg_motif->pssm = NULL;
  bg_motif->pfm = NULL;
  bg_motif->bg_freqs = NULL;
  bg_motif->col_ic = NULL;
  bg_motif->col_relh = NULL;
  bg_motif->ic_base = 2;
  bg_motif->type = bg_seqs->type;
  bg_motif->method = SCORE_FREQ;
  bg_motif->bg = BG_NONE;
  bg_motif->pc_n = PC_N_NONE;
  bg_motif->pc_dist = PC_DIST_EQUAL;
  bg_motif->n = gl_list_size (bg_seqs->seqs);
  bg_motif->length = win_len;
  bg_motif->weighted = arguments.weight_bg;
  calc_motif_stats (bg_motif, bg_seqs, NULL, 0.0, 0.0);
  bg_motif->n_feats = arguments.n_feats;
  if (arguments.n_feats == 0)
    {
      bg_motif->feature_cols = NULL;
      bg_motif->bernoulli_feats = NULL;
      bg_motif->n_bernoulli = 0;
      bg_motif->gaussian_feats = NULL;
      bg_motif->n_gaussian = 0;
      bg_motif->beta_feats = NULL;
      bg_motif->n_beta = 0;
    }
  else
    {
      if (ALLOC_N (bg_motif->feature_cols, bg_motif->n_feats))
        error (EXIT_FAILURE, errno, "Memory exhausted");
      memcpy (bg_motif->feature_cols, arguments.features,
              bg_motif->n_feats * (sizeof (bayes_feat)));
      bg_motif->n_bernoulli = n_bernoulli;
      if (ALLOC_N (bg_motif->bernoulli_feats, n_bernoulli))
        error (EXIT_FAILURE, errno, "Memory exhausted");
      for (size_t i=0; i<n_bernoulli; i++)
        {
          const bool *dat = bernoulli_dat[i];
          bg_motif->bernoulli_feats[i] = bernoulli_param (dat, num_recs,
                                                          arguments.alpha);
        }
      bg_motif->n_gaussian = n_gaussian;
      if (ALLOC_N (bg_motif->gaussian_feats, n_gaussian))
        error (EXIT_FAILURE, errno, "Memory exhausted");
      for (size_t i=0; i<n_gaussian; i++)
        {
          const double *dat = gaussian_dat[i];
          bg_motif->gaussian_feats[i] = gaussian_param (dat, num_recs);
        }
      bg_motif->n_beta = n_beta;
      if (ALLOC_N (bg_motif->beta_feats, n_beta))
        error (EXIT_FAILURE, errno, "Memory exhausted");
      for (size_t i=0; i<n_beta; i++)
        {
          const double *dat = beta_dat[i];
          bg_motif->beta_feats[i] = beta_param (dat, num_recs, arguments.squeeze,
                                                arguments.s);
        }
    }
}


void
setup_motif (motifstats *motif, seq_set *seq_wins, bool **bernoulli_dat,
             double **gaussian_dat, double **beta_dat, size_t win_len,
             size_t n_bernoulli, size_t n_gaussian, size_t n_beta,
             motifstats bg_motif, struct arguments arguments)
{
  motif->name = arguments.name;
  motif->pssm = NULL;
  motif->pfm = NULL;
  motif->bg_freqs = NULL;
  motif->col_ic = NULL;
  motif->col_relh = NULL;
  motif->ic_base = arguments.ic_base;
  motif->type = seq_wins->type;
  motif->method = arguments.method;
  motif->bg = arguments.bg;
  motif->pc_n = arguments.pseudo_n;
  motif->pc_dist = arguments.pseudo_dist;
  motif->n = gl_list_size(seq_wins->seqs);
  motif->length = win_len;
  motif->weighted = arguments.weight;
  if (arguments.method == SCORE_BAYES)
    motif->prior = ((double) motif->n) / ((double) bg_motif.n);
  else
    motif->prior = 0;
  motif->n_feats = arguments.n_feats;
  if (arguments.n_feats == 0)
    {
      motif->feature_cols = NULL;
      motif->n_bernoulli = 0;
      motif->bernoulli_feats = NULL;
      motif->n_gaussian = 0;
      motif->gaussian_feats = NULL;
      motif->n_beta = 0;
      motif->beta_feats = NULL;
    }
  else
    {
      if (ALLOC_N (motif->feature_cols, motif->n_feats))
        error (EXIT_FAILURE, errno, "Memory exhausted");
      memcpy (motif->feature_cols, arguments.features,
              motif->n_feats * (sizeof (bayes_feat)));
      motif->n_bernoulli = n_bernoulli;
      if (ALLOC_N (motif->bernoulli_feats, n_bernoulli))
        error (EXIT_FAILURE, errno, "Memory exhausted");
      for (size_t i=0; i<n_bernoulli; i++)
        {
          const bool *dat = bernoulli_dat[i];
          motif->bernoulli_feats[i] = bernoulli_param (dat, motif->n,
                                                       arguments.alpha);
        }
      motif->n_gaussian = n_gaussian;
      if (ALLOC_N (motif->gaussian_feats, n_gaussian))
        error (EXIT_FAILURE, errno, "Memory exhausted");
      for (size_t i=0; i<n_gaussian; i++)
        {
          const double *dat = gaussian_dat[i];
          motif->gaussian_feats[i] = gaussian_param (dat, motif->n);
        }
      motif->n_beta = n_beta;
      if (ALLOC_N (motif->beta_feats, n_beta))
        error (EXIT_FAILURE, errno, "Memory exhausted");
      for (size_t i=0; i<n_beta; i++)
        {
          const double *dat = beta_dat[i];
          motif->beta_feats[i] = beta_param (dat, motif->n, arguments.squeeze,
                                             arguments.s);
        }
    }
}


void
setup_compl_motif (motifstats *compl_motif, seq_set compl_seqs, size_t win_len,
                   const motifstats *bg_motif, struct arguments arguments)
{
  char *compl_name;
  if (asprintf (&compl_name, "%s complement", arguments.name) < 0)
    error (EXIT_FAILURE, errno, "Error constructing complement motif name");
  compl_motif->name = compl_name;
  compl_motif->pssm = NULL;
  compl_motif->pfm = NULL;
  compl_motif->bg_freqs = NULL;
  compl_motif->col_ic = NULL;
  compl_motif->col_relh = NULL;
  compl_motif->ic_base = arguments.ic_base;
  compl_motif->type = compl_seqs.type;
  compl_motif->method = arguments.method;
  compl_motif->bg = arguments.bg;
  compl_motif->pc_n = arguments.pseudo_n;
  compl_motif->pc_dist = arguments.pseudo_dist;
  compl_motif->n = gl_list_size(compl_seqs.seqs);
  compl_motif->length = win_len;
  compl_motif->weighted = arguments.weight;
  if (arguments.method == SCORE_BAYES)
    compl_motif->prior = ((double) compl_motif->n) / ((double) bg_motif->n);
  else
    compl_motif->prior = 0.0;
  compl_motif->n_feats = arguments.n_feats;
  if (arguments.n_feats == 0)
    {
      compl_motif->feature_cols = NULL;
      compl_motif->bernoulli_feats = NULL;
      compl_motif->n_bernoulli = 0;
      compl_motif->gaussian_feats = NULL;
      compl_motif->n_gaussian = 0;
      compl_motif->beta_feats = NULL;
      compl_motif->n_beta = 0;
    }
  else
    {
      if (ALLOC_N (compl_motif->feature_cols, compl_motif->n_feats))
        error (EXIT_FAILURE, errno, "Memory exhausted");
      memmove (compl_motif->feature_cols, arguments.features,
              compl_motif->n_feats * (sizeof (bayes_feat)));

      compl_motif->n_bernoulli = bg_motif->n_bernoulli;
      if (ALLOC_N (compl_motif->bernoulli_feats, compl_motif->n_bernoulli))
        error (EXIT_FAILURE, errno, "Memory exhausted");
      memmove (compl_motif->bernoulli_feats, bg_motif->bernoulli_feats,
               compl_motif->n_bernoulli * (sizeof (double)));

      compl_motif->n_gaussian = bg_motif->n_gaussian;
      if (ALLOC_N (compl_motif->gaussian_feats, compl_motif->n_gaussian))
        error (EXIT_FAILURE, errno, "Memory exhausted");
      memmove (compl_motif->gaussian_feats, bg_motif->gaussian_feats,
               compl_motif->n_gaussian * (sizeof (gaussian_dist)));

      compl_motif->n_beta = bg_motif->n_beta;
      if (ALLOC_N (compl_motif->beta_feats, compl_motif->n_beta))
        error (EXIT_FAILURE, errno, "Memory exhausted");
      memmove (compl_motif->beta_feats, bg_motif->beta_feats,
               compl_motif->n_beta * (sizeof (beta_dist)));
    }
}


int
main (int argc, char **argv)
{
  struct arguments arguments;
  init_args (&arguments);
  argp_parse (&argp, argc, argv, 0, 0, &arguments);

  if (!arguments.name)
    {
      if (!arguments.args[0] || !strcmp (arguments.args[0], "-"))
        arguments.name = "unknown";
      else
        arguments.name = basename (arguments.args[0]);
    }
  if (!arguments.args[1] || !strcmp (arguments.args[1], ""))
    error (EXIT_FAILURE, 0, "An output file must be specified");
  if (arguments.method == SCORE_BAYES)
    {
      if (!arguments.args[2] || !strcmp (arguments.args[2], ""))
        error (EXIT_FAILURE, 0, "A complementary output file must be specified for "
               "Bayes scoring.");
      if (arguments.bg != BG_POS)
        error (EXIT_FAILURE, 0, "Bayes scoring requires a position-specific "
               "background.");
    }

  if (arguments.bg != BG_NONE)
    arguments.pseudo_dist = PC_DIST_FREQ;

  seq_set seq_wins;
  size_t win_len;
  bool **bernoulli_dat = NULL;
  double **gaussian_dat = NULL;
  double **beta_dat = NULL;
  size_t n_bernoulli = 0;
  size_t n_gaussian = 0;
  size_t n_beta = 0;
  size_t num_recs = 0;
  /* Read in the sequence windows or windows + features*/
  if (arguments.method == SCORE_BAYES)
    {
      num_recs = read_features (&seq_wins, &bernoulli_dat, &gaussian_dat, &beta_dat,
                                arguments.args[0], arguments.features, arguments.n_feats,
                                &n_bernoulli, &n_gaussian, &n_beta, SEQ_GUESS,
                                arguments.feat_delim, NULL);
      win_len = seq_wins.win_len;
    }
  else
    {
      win_len = read_seq_wins (&seq_wins, arguments.args[0], SEQ_GUESS);
    }
  if (seq_wins.type == SEQ_GUESS)
    error (EXIT_FAILURE, 0, "Somehow failed to guess the sequence type in the "
           "sequence windows file");

  /* Weight the sequence windows */
  if (arguments.weight)
    {
      weight_seq_windows (&seq_wins);
      if (arguments.print_weights)
        print_seq_weights (&seq_wins);
    }

  seq_set bg_seqs;
  gsl_matrix *bg_freqs = NULL;
  motifstats bg_motif;
  size_t bg_num_recs = 0;
  if (arguments.bg != BG_NONE)
    {
      /* Read in the background.  How this is done depends on whether
         it's a global background (e.g. proteome in FASTA format) or a
         positional background (i.e. more sequence windows) */
      if (!arguments.bg_file)
        error (EXIT_FAILURE, 0, "No background file specified (option --bg-file).");
      if (arguments.bg == BG_POS)
        {
          /* Positional background */
          size_t bg_win_len;
          bool **bg_bernoulli_dat = NULL;
          double **bg_gaussian_dat = NULL;
          double **bg_beta_dat = NULL;
          size_t bg_n_bernoulli = 0;
          size_t bg_n_gaussian = 0;
          size_t bg_n_beta = 0;
          /* Read in the sequence windows or windows + features */
          if (arguments.method == SCORE_BAYES)
            {
              bg_num_recs = read_features (&bg_seqs, &bg_bernoulli_dat, &bg_gaussian_dat,
                                           &bg_beta_dat, arguments.bg_file,
                                           arguments.features, arguments.n_feats,
                                           &bg_n_bernoulli, &bg_n_gaussian, &bg_n_beta,
                                           seq_wins.type, arguments.feat_delim, &seq_wins);
              bg_win_len = bg_seqs.win_len;
            }
          else
            {
              bg_win_len = read_seq_wins (&bg_seqs, arguments.bg_file, seq_wins.type);
            }
          if (bg_win_len != win_len)
            {
              error (EXIT_FAILURE, 0, "Background sequence windows are of length "
                     "%zd, but length %zd was expected", bg_win_len, win_len);
            }
          /* Weight the windows */
          if (arguments.weight_bg)
            weight_seq_windows (&bg_seqs);
          /* Setup the motifstats structure for the background windows */
          setup_bg_motif (&bg_motif, &bg_seqs, bg_bernoulli_dat, bg_gaussian_dat,
                          bg_beta_dat, win_len, n_bernoulli, n_gaussian,
                          n_beta, bg_num_recs, arguments);
          for (size_t i=0; i<n_bernoulli; i++)
            {
              FREE (bg_bernoulli_dat[i]);
            }
          FREE (bg_bernoulli_dat);
          for (size_t i=0; i<n_gaussian; i++)
            {
              FREE (bg_gaussian_dat[i]);
            }
          FREE (bg_gaussian_dat);
          for (size_t i=0; i<n_beta; i++)
            {
              FREE (bg_beta_dat[i]);
            }
          FREE (bg_beta_dat);
        }
      else                      /* BG_GLOBAL */
        {
          /* Global background.  Read the FASTA file and calculate the
             global frequencies */
          read_sequences (&bg_seqs, arguments.bg_file, SEQ_GUESS);
          bg_freqs = calc_global_bg (bg_seqs);
        }
    }

  /* Set up the main motifstats structure */
  motifstats motif;
  setup_motif (&motif, &seq_wins, bernoulli_dat, gaussian_dat, beta_dat,
               win_len, n_bernoulli, n_gaussian, n_beta, bg_motif, arguments);
  if (n_bernoulli > 0)
    {
      for (size_t i=0; i<n_bernoulli; i++)
        {
          FREE (bernoulli_dat[i]);
        }
      FREE (bernoulli_dat);
    }
  if (n_gaussian > 0)
    {
      for (size_t i=0; i<n_gaussian; i++)
        {
          FREE (gaussian_dat[i]);
        }
      FREE (gaussian_dat);
    }
  if (n_beta > 0)
    {
      for (size_t i=0; i<n_beta; i++)
        {
          FREE (beta_dat[i]);
        }
      FREE (beta_dat);
    }

  /* Calculate the motif stats */
  switch (arguments.bg)
    {
    case BG_NONE:
      calc_motif_stats (&motif, &seq_wins, NULL, arguments.pseudo_p,
                        arguments.diff_scale);
      break;
    case BG_POS:
      calc_motif_stats (&motif, &seq_wins, bg_motif.pfm, arguments.pseudo_p,
                        arguments.diff_scale);
      break;
    case BG_GLOBAL:
      calc_motif_stats (&motif, &seq_wins, bg_freqs, arguments.pseudo_p,
                        arguments.diff_scale);
      break;
    }

  /* Write the motif to a file */
  hid_t file_id = H5Fcreate (arguments.args[1], H5F_ACC_TRUNC, H5P_DEFAULT,
                             H5P_DEFAULT);
  if (file_id < 0)
    error (EXIT_FAILURE, errno, "Error creating motif file");
  write_motif_stats (file_id, motif, PACKAGE_STRING);
  H5Fclose (file_id);

  /* If doing Naive Bayes scoring, setup and write the complementary motif */
  if (arguments.method == SCORE_BAYES)
    {
      /* Calculate the complementary motif for Naive Bayes scoring */
      /* Get the complementary sequence set */
      seq_set compl_seqs;
      seq_set_diff (bg_seqs, seq_wins, &compl_seqs);
      if (arguments.weight)
        {
          /* Re-weight the sequence windows */
          weight_seq_windows (&compl_seqs);
        }
      /* Set up the motifstats struct for the complementary motif */
      motifstats compl_motif;
      setup_compl_motif (&compl_motif, compl_seqs, win_len, &bg_motif, arguments);
      calc_motif_stats (&compl_motif, &compl_seqs, bg_motif.pfm, arguments.pseudo_p,
                        arguments.diff_scale);
      clear_seq_win_set (&compl_seqs);
      /* Write the complementary motif to a file */
      hid_t compl_file_id = H5Fcreate (arguments.args[2], H5F_ACC_TRUNC, H5P_DEFAULT,
                                      H5P_DEFAULT);
      if (compl_file_id < 0)
        error (EXIT_FAILURE, errno, "Error creating complementary motif file");
      write_motif_stats (compl_file_id, compl_motif, PACKAGE_STRING);
      H5Fclose (compl_file_id);
      free_motif_stats (compl_motif, true);
    }

  clear_seq_win_set (&seq_wins);
  if (arguments.bg != BG_NONE)
    {
      if (arguments.bg == BG_POS)
        clear_seq_win_set (&bg_seqs);
      else
        {
          clear_global_seq_set (&bg_seqs);
          gsl_matrix_free (bg_freqs);
        }
    }
  free_motif_stats (motif, false);
  if (arguments.bg == BG_POS)
    {
      free_motif_stats (bg_motif, false);
    }
  if (arguments.features)
    FREE (arguments.features);
  return EXIT_SUCCESS;
}
