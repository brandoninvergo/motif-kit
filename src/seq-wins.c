/* Copyright (C) 2019, 2020, 2021 Brandon Invergo <b.invergo@exeter.ac.uk> */

/* This program is free software: you can redistribute it and/or modify */
/* it under the terms of the GNU General Public License as published by */
/* the Free Software Foundation, either version 3 of the License, or */
/* (at your option) any later version. */

/* This program is distributed in the hope that it will be useful, */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the */
/* GNU General Public License for more details. */

/* You should have received a copy of the GNU General Public License */
/* along with this program.  If not, see <http://www.gnu.org/licenses/>. */


#include "seq-wins.h"


/* Sequence window equality function for use in gnulib's set data
   structure */
bool
seq_wins_equal (const void *elt1, const void *elt2)
{
  seq_win *win1 = (seq_win *)elt1;
  seq_win *win2 = (seq_win *)elt2;
  if ((win1->id == NULL && win2->id != NULL) ||
      (win1->id != NULL && win2->id == NULL))
    return false;
  if (win1->id != NULL)
    {
      return (strcmp (win1->seq, win2->seq) == 0 &&
              strcmp (win1->id, win2->id) == 0);
    }
  return strcmp (win1->seq, win2->seq) == 0;
}

/* Sequence window hash function for use in gnulib's set data
   structure.  It uses the sdbm hash algorithm, adapted from:
   http://www.cse.yorku.ca/~oz/hash.html */
size_t
seq_win_hash (const void *elt)
{
  size_t hash = 0;
  int c;
  seq_win *win = (seq_win *)elt;
  char *hash_str = NULL;
  if (win->id == NULL)
    hash_str = win->seq;
  else
    hash_str = win->id;
  while ((c = *hash_str++))
    hash = c + (hash << 6) + (hash << 16) - hash;
  return hash;
}

void
seq_win_dispose (const void *elt)
{
  seq_win *win = (seq_win *) elt;
  if (win->seq)
    FREE (win->seq);
  if (win->id)
    FREE (win->id);
  if (win)
    FREE (win);
}


int
add_seq_win (seq_set *seq_wins, const char *seq, const char *id, seq_win *win,
             size_t win_len)
{
  size_t seq_len = strlen (seq);
  if (seq_len != win_len)
    {
      error (EXIT_FAILURE, 0, "Inconsistent window lengths: expected %zu got %zu", win_len, seq_len);
    }
  win->weight = 1.0;
  win->seq = strndup (seq, win_len);
  if (!win->seq)
    {
      error (EXIT_FAILURE, errno, "Memory exhausted");
    }
  if (id)
    {
      win->id = strdup(id);
      if (!win->id)
        error (EXIT_FAILURE, errno, "Memory exhausted");
    }
  else
    win->id = NULL;
  /* Add the sequence window to the sequence set */
  if (!gl_list_nx_add_last (seq_wins->seqs, win))
    {
      error (EXIT_FAILURE, errno, "Memory exhausted");
    }
}


/* Read in a file of sequence windows, one sequence per line.  Return
   the length of the window (which must be the same for each
   sequence). Duplicates are not allowed. */
size_t
read_seq_wins (seq_set *seq_wins, const char *seq_win_file, seq_type type)
{
  FILE *seq_win_stream;
  if (!seq_win_file || !strcmp (seq_win_file, "-"))
    seq_win_stream = stdin;
  else
    {
      seq_win_stream = fopen (seq_win_file, "r");
      if (!seq_win_stream)
        error (EXIT_FAILURE, errno, "Failed to open sequence window file %s",
               seq_win_file);
    }
  seq_wins->seqs = gl_list_nx_create_empty (GL_LINKEDHASH_LIST, seq_wins_equal,
                                            seq_win_hash, seq_win_dispose, true);
  if (!seq_wins->seqs)
    error (EXIT_FAILURE, errno, "Memory exhausted");
  char *line = NULL;
  size_t n = 0;
  size_t win_len = 0;
  seq_wins->type = type;
  regex_t aa_regex;
  if (seq_wins->type == SEQ_GUESS)
    {
      int regerr = regcomp (&aa_regex, "[RHKDESNQPVILMFYW]", REG_NOSUB | REG_ICASE);
      if (regerr)
        error (EXIT_FAILURE, errno, "Error compiling AA regex.  You shouldn't "
               "see this");
    }
  while (getline (&line, &n, seq_win_stream) >= 0)
    {
      /* Assume that there is more than one sequence in the file.
         However, do not assume that there is a newline after the last
         sequence. */
      size_t line_len = strlen (line);
      if (line_len == 0)
        continue;
      /* Guess the window length from the first sequence */
      if (win_len == 0)
        {
          /* getline includes the newline */
          win_len = line_len - 1;
        }
      /* If necessary, guess the sequence type from the first sequence */
      if (seq_wins->type == SEQ_GUESS)
        {
          int regres = regexec (&aa_regex, line, 0, NULL, 0);
          if (regres == REG_ESPACE)
            error (EXIT_FAILURE, errno, "Memory exhausted");
          if (regres == 0)
            seq_wins->type = SEQ_AA;
          else
            seq_wins->type = SEQ_NUC;
          regfree (&aa_regex);
        }
      /* Get rid of the newline */
      line[line_len - 1] = '\0';
      seq_win *win = NULL;
      if (ALLOC (win))
        {
          error (EXIT_FAILURE, errno, "Memory exhausted");
        };
      /* No sequence IDs in this case */
      add_seq_win (seq_wins, line, NULL, win, win_len);
    }
  fclose (seq_win_stream);
  if (line)
    FREE (line);
  if (gl_list_size (seq_wins->seqs) == 0 || win_len == 0)
    {
      error (EXIT_FAILURE, 0, "No sequences found in sequence window file %s",
             seq_win_file);
    }
  seq_wins->win_len = win_len;
  return win_len;
}


/* Calculate the set difference A - B and store it in diff */
void
seq_set_diff (const seq_set A, const seq_set B, seq_set *diff)
{
  diff->seqs = gl_list_nx_create_empty (GL_LINKEDHASH_LIST, seq_wins_equal,
                                        seq_win_hash, seq_win_dispose, true);
  if (!diff->seqs)
    error (EXIT_FAILURE, errno, "Memory exhausted");
  diff->win_len = A.win_len;
  diff->type = A.type;
  gl_list_iterator_t i = gl_list_iterator (A.seqs);
  const void *eltp;
  while (gl_list_iterator_next (&i, &eltp, NULL))
    {
      seq_win *win = (seq_win *) eltp;
      if (gl_list_search (B.seqs, win))
        {
          continue;
        }
      seq_win *cpy;
      if (ALLOC (cpy))
        error (EXIT_FAILURE, errno, "Memory exhausted");
      cpy->seq = strdup (win->seq);
      if (!cpy->seq)
        error (EXIT_FAILURE, errno, "Memory exhausted");
      if (win->id)
        {
          cpy->id = strdup (win->id);
          if (!cpy->id)
            error (EXIT_FAILURE, errno, "Memory exhausted");
        }
      else
        cpy->id = NULL;
      cpy->weight = win->weight;
      if (!gl_list_nx_add_last (diff->seqs, cpy))
        {
          error (EXIT_FAILURE, errno, "Memory exhausted");
        }
    }
  gl_list_iterator_free (&i);
}


void
seq_set_union (const seq_set A, const seq_set B, seq_set *set_union)
{
  set_union->seqs = gl_list_nx_create_empty (GL_LINKEDHASH_LIST, seq_wins_equal,
                                             seq_win_hash, seq_win_dispose, true);
  if (!set_union->seqs)
    error (EXIT_FAILURE, errno, "Memory exhausted");
  set_union->win_len = A.win_len;
  set_union->type = A.type;
  gl_list_iterator_t i = gl_list_iterator (A.seqs);
  const void *eltp;
  while (gl_list_iterator_next (&i, &eltp, NULL))
    {
      seq_win *win = (seq_win *) eltp;
      seq_win *cpy;
      if (ALLOC (cpy))
        error (EXIT_FAILURE, errno, "Memory exhausted");
      cpy->seq = strdup (win->seq);
      if (!cpy->seq)
        error (EXIT_FAILURE, errno, "Memory exhausted");
      if (win->id)
        {
          cpy->id = strdup (win->id);
          if (!cpy->id)
            error (EXIT_FAILURE, errno, "Memory exhausted");
        }
      else
        cpy->id = NULL;
      cpy->weight = win->weight;
      if (!gl_list_nx_add_last (set_union->seqs, cpy) &&
          !gl_list_search (set_union->seqs, cpy))
        error (EXIT_FAILURE, errno, "Memory exhausted");
    }
  gl_list_iterator_free (&i);
  i = gl_list_iterator (B.seqs);
  while (gl_list_iterator_next (&i, &eltp, NULL))
    {
      seq_win *win = (seq_win *) eltp;
      seq_win *cpy;
      if (ALLOC (cpy))
        error (EXIT_FAILURE, errno, "Memory exhausted");
      cpy->seq = strdup (win->seq);
      if (!cpy->seq)
        error (EXIT_FAILURE, errno, "Memory exhausted");
      if (win->id)
        {
          cpy->id = strdup (win->id);
          if (!cpy->id)
            error (EXIT_FAILURE, errno, "Memory exhausted");
        }
      else
        cpy->id = NULL;
      cpy->weight = win->weight;
      if (!gl_list_nx_add_last (set_union->seqs, cpy) &&
          !gl_list_search (set_union->seqs, cpy))
        error (EXIT_FAILURE, errno, "Memory exhausted");
    }
  gl_list_iterator_free (&i);
}


/* Free the contents of a sequence-window set */
void
clear_seq_win_set (seq_set *seq_wins)
{
  if (seq_wins->seqs)
    gl_list_free (seq_wins->seqs);
}


/* Position-specific sequence weighting routine from Henikoff &
   Henikoff J. Mol. Biol. 1994 */

/* Count residue occurrences in each position */
void
count_pos_residues (const seq_set *seq_wins, gsl_vector *pos_res_counts,
                    gsl_matrix *res_counts)
{
  size_t nrow;
  /* Following the PSI-BLAST method for handling alignment gaps, we
     count tails ("_") as a distinct character for sequence weighting.
     They will later be added as pseudocounts when calculating residue
     counts and position frequencies */
  if (seq_wins->type == SEQ_AA)
    nrow = NUM_AA + 1;
  else if (seq_wins->type == SEQ_NUC)
    nrow = NUM_NUC + 1;
  else
    error (EXIT_FAILURE, 0, "Failure to guess sequence type");
  gsl_matrix *res_pres = gsl_matrix_calloc (nrow, seq_wins->win_len);
  if (!res_pres)
    {
      error (EXIT_FAILURE, errno, "Memory exhausted");
    }
  gl_list_iterator_t i = gl_list_iterator (seq_wins->seqs);
  const void *eltp;
  while (gl_list_iterator_next (&i, &eltp, NULL))
    {
      seq_win *win = (seq_win *) eltp;
      for (size_t j=0; j<seq_wins->win_len; j++)
        {
          if (seq_wins->type == SEQ_AA)
            {
              aminoacid a = char2aa[(size_t) win->seq[j]];
              if (a == AA_ERR)
                error (EXIT_FAILURE, 0, "Invalid character in sequence %s: %c",
                       win->seq, win->seq[j]);
              else if (aa_is_ambiguous (a))
                continue;
              else if (aa_is_tail (a))
                a = NUM_AA;     /* Adjust for the matrix indexing */
              double count = gsl_matrix_get (res_counts, a, j);
              gsl_matrix_set (res_counts, a, j, count + 1.0);
              gsl_matrix_set (res_pres, a, j, 1.0);
            }
          else
            {
              nucleotide n = char2nuc[(size_t) win->seq[j]];
              if (n == NUC_ERR)
                error (EXIT_FAILURE, 0, "Invalid character in sequence %s: %c",
                       win->seq, win->seq[j]);
              else if (nuc_is_ambiguous (n))
                continue;
              else if (nuc_is_tail (n))
                n = NUM_NUC;    /* Adjust for the matrix indexing */
              double count = gsl_matrix_get (res_counts, n, j);
              gsl_matrix_set (res_counts, n, j, count + 1.0);
              gsl_matrix_set (res_pres, n, j, 1.0);
            }
        }
    }
  gl_list_iterator_free (&i);
  gsl_vector *ones = gsl_vector_alloc (nrow);
  if (!ones)
    {
      error (EXIT_FAILURE, errno, "Memory exhausted");
    }
  gsl_vector_set_all (ones, 1.0);
  gsl_blas_dgemv (CblasTrans, 1.0, res_pres, ones, 0.0, pos_res_counts);
  gsl_vector_free (ones);
  gsl_matrix_free (res_pres);
}

/* Calculate position-specific residue weights */
void
calc_pos_res_weights (const seq_set *seq_wins, const gsl_vector *pos_res_counts,
                      const gsl_matrix *res_counts, gsl_matrix *res_weights)
{
  gl_list_iterator_t i = gl_list_iterator (seq_wins->seqs);
  const void *eltp;
  while (gl_list_iterator_next (&i, &eltp, NULL))
    {
      seq_win *win = (seq_win *) eltp;
      for (size_t j=0; j<seq_wins->win_len; j++)
        {
          if (seq_wins->type == SEQ_AA)
            {
              aminoacid a = char2aa[(size_t) win->seq[j]];
              if (a == AA_ERR)
                error (EXIT_FAILURE, 0, "Invalid character in sequence %s: %c",
                       win->seq, win->seq[j]);
              else if (aa_is_ambiguous (a))
                continue;
              else if (aa_is_tail (a))
                a = NUM_AA;     /* Adjust for the matrix indexing */
              double pos_res_count = gsl_vector_get (pos_res_counts, j);
              double res_count = gsl_matrix_get (res_counts, a, j);
              double weight = 1.0/(pos_res_count * res_count);
              gsl_matrix_set (res_weights, a, j, weight);
            }
          else
            {
              nucleotide n = char2nuc[(size_t) win->seq[j]];
              if (n == NUC_ERR)
                error (EXIT_FAILURE, 0, "Invalid character in sequence %s: %c",
                       win->seq, win->seq[j]);
              else if (nuc_is_ambiguous (n))
                continue;
              else if (nuc_is_tail (n))
                n = NUM_NUC;
              double pos_res_count = gsl_vector_get (pos_res_counts, j);
              double res_count = gsl_matrix_get (res_counts, n, j);
              double weight = 1.0/(pos_res_count * res_count);
              gsl_matrix_set (res_weights, n, j, weight);
            }
        }
    }
  gl_list_iterator_free (&i);
}

/* Calculate sequence weights from residue weights */
double
calc_seq_weights (seq_set *seq_wins, const gsl_matrix *res_weights)
{
  double weight_sum = 0.0;
  gl_list_iterator_t i = gl_list_iterator (seq_wins->seqs);
  const void *eltp;
  while (gl_list_iterator_next (&i, &eltp, NULL))
    {
      seq_win *win = (seq_win *) eltp;
      double weight = 0.0;
      for (size_t j=0; j<seq_wins->win_len; j++)
        {
          if (seq_wins->type == SEQ_AA)
            {
              aminoacid a = char2aa[(size_t) win->seq[j]];
              if (a == AA_ERR)
                error (EXIT_FAILURE, 0, "Invalid character in sequence %s: %c",
                       win->seq, win->seq[j]);
              else if (aa_is_ambiguous (a))
                continue;
              else if (aa_is_tail (a))
                a = NUM_AA;     /* Adjust for the matrix indexing */
              weight += gsl_matrix_get (res_weights, a, j);
            }
          else
            {
              nucleotide n = char2nuc[(size_t) win->seq[j]];
              if (n == NUC_ERR)
                error (EXIT_FAILURE, 0, "Invalid character in sequence %s: %c",
                       win->seq, win->seq[j]);
              else if (nuc_is_ambiguous (n))
                continue;
              else if (nuc_is_tail (n))
                n = NUM_NUC;
              weight += gsl_matrix_get (res_weights, n, j);
            }
        }
      win->weight = weight;
      weight_sum += weight;
    }
  gl_list_iterator_free (&i);
  return weight_sum;
}

/* Normalize sequence weights */
void
normalize_weights (seq_set *seq_wins, double weight_sum)
{
  gl_list_iterator_t i = gl_list_iterator (seq_wins->seqs);
  const void *eltp;
  while (gl_list_iterator_next (&i, &eltp, NULL))
    {
      seq_win *win = (seq_win *) eltp;
      win->weight /= weight_sum;
    }
  gl_list_iterator_free (&i);
}

/* Master sequence-weighting function */
void
weight_seq_windows (seq_set *seq_wins)
{
  /* First, count the total number of residues observed in a column
     and the number of occurrences of each residue in each column */
  gsl_vector *pos_res_counts = gsl_vector_calloc (seq_wins->win_len);
  if (!pos_res_counts)
    error (EXIT_FAILURE, errno, "Memory exhausted");
  /* Following how PSI-BLAST handles alignment gaps, during sequence
     weighting we treat window tails ('_') as distinct entities */
  size_t nrow;
  if (seq_wins->type == SEQ_AA)
    nrow = NUM_AA + 1;
  else if (seq_wins->type == SEQ_NUC)
    nrow = NUM_NUC + 1;
  else
    error (EXIT_FAILURE, 0, "Failure to guess sequence type");
  gsl_matrix *res_counts = gsl_matrix_calloc (nrow, seq_wins->win_len);
  if (!res_counts)
    error (EXIT_FAILURE, errno, "Memory exhausted");
  count_pos_residues (seq_wins, pos_res_counts, res_counts);

  /* Next calculate a weight for each residue observed in each
     column */
  gsl_matrix *res_weights = gsl_matrix_calloc (nrow, seq_wins->win_len);
  calc_pos_res_weights (seq_wins, pos_res_counts, res_counts, res_weights);
  gsl_matrix_free (res_counts);
  gsl_vector_free (pos_res_counts);

  /* Calculate sequence weights from the position-specific residue
     weights.  The weights are stored directly in the seq_win structs
     in the seq_wins set */
  double weight_sum = calc_seq_weights (seq_wins, res_weights);
  gsl_matrix_free (res_weights);

  /* Finally, normalize the sequence weights in place so that they sum
     to 1.0 */
  normalize_weights (seq_wins, weight_sum);
}
