/* Copyright (C) 2020 Brandon Invergo <b.invergo@exeter.ac.uk> */

/* This program is free software: you can redistribute it and/or modify */
/* it under the terms of the GNU General Public License as published by */
/* the Free Software Foundation, either version 3 of the License, or */
/* (at your option) any later version. */

/* This program is distributed in the hope that it will be useful, */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the */
/* GNU General Public License for more details. */

/* You should have received a copy of the GNU General Public License */
/* along with this program.  If not, see <http://www.gnu.org/licenses/>. */



#include <config.h>

#include <errno.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <stdint.h>
#include <string.h>
#include "argp.h"
#include "error.h"
#include <hdf5.h>

#include "motifstats.h"
#include "pssm.h"
#include "sequences.h"
#include "seq-wins.h"
#include "sequence.h"
#include "ecdf.h"


const char *argp_program_version = PACKAGE_STRING;
const char *argp_program_bug_address = PACKAGE_BUGREPORT;

static char doc[] =
  "motif-sim -- simulate sequence windows from motif"
  "\v"
  "This program generates random sequence windows from the position "
  "frequency matrix or the background frequencies associated with "
  "MOTIFFILE";

static char args_doc[] = "MOTIFFILE";

static struct argp_option options[] =
  {
   {"what", 'w', "SOURCE", 0,
    "The source of amino acid / nucleotide frequencies from which to simulate "
    "new sequences: pfm (position frequency matrix; default) or bg (background "
    "frequencies", 0},
   {"n", 'n', "NUM", 0, "The number of sequences to generate (default: 1000)",
    0},
   {0}
  };

enum source_opt {SOURCE_PFM, SOURCE_BG};

struct arguments
{
  char *motif_file;
  enum source_opt source;
  size_t n;
};

static error_t
parse_opt (int key, char *arg, struct argp_state *state)
{
  struct arguments *arguments = state->input;
  switch  (key)
    {
    case 'n':
      {
        unsigned long n = strtoul (arg, NULL, 0);
        if (n == ULONG_MAX && errno == ERANGE)
          error (EXIT_FAILURE, errno, "Overflow when parsing n");
        if (n > SIZE_MAX)
          error (EXIT_FAILURE, 0, "Overflow when parsing n");
        if (n <= 1)
          error (EXIT_FAILURE, 0, "n must be > 0");
        arguments->n = (size_t) n;
        break;
      }
    case 'w':
      if (!strcmp (arg, "pfm"))
        {
        arguments->source = SOURCE_PFM;
        }
      else if (!strcmp (arg, "bg"))
        {
          arguments->source = SOURCE_BG;
        }
      else
        {
          argp_usage (state);
        }
      break;
    case ARGP_KEY_ARG:
      if (state->arg_num >= 1)
        argp_usage (state);
      arguments->motif_file = arg;
      break;
    case ARGP_KEY_END:
      break;
    default:
      return ARGP_ERR_UNKNOWN;
    }
  return 0;
}

static struct argp argp = {options, parse_opt, args_doc, doc};
char *fail_msg = NULL;


void
init_args (struct arguments *arguments)
{
  arguments->motif_file = NULL;
  arguments->source = SOURCE_PFM;
  arguments->n = 1000;
}


int
main (int argc, char **argv)
{
  struct arguments arguments;
  init_args (&arguments);
  argp_parse (&argp, argc, argv, 0, 0, &arguments);

  if (access (arguments.motif_file, F_OK) == -1)
    error (EXIT_FAILURE, errno, "Motif file %s does not exist",
           arguments.motif_file);
  hid_t file_id;
  file_id = H5Fopen (arguments.motif_file, H5F_ACC_RDONLY, H5P_DEFAULT);
  if (file_id < 0)
    error (EXIT_FAILURE, errno, "Failed to open PSSM file %s",
           arguments.motif_file);
  motifstats motif;
  read_motif_stats (file_id, &motif);
  H5Fclose (file_id);

  gsl_matrix *source_freqs;
  switch (arguments.source)
    {
    case SOURCE_PFM:
      source_freqs = motif.pfm;
      break;
    case SOURCE_BG:
      source_freqs = motif.bg_freqs;
      break;
    }

  /* set up random number generator */
  gsl_rng_env_setup ();
  /* default is Mersenne Twister, but can be modified by the
     environment */
  gsl_rng *rng;
  rng = gsl_rng_alloc (gsl_rng_default);
  if (!rng)
    error (EXIT_FAILURE, errno, "Memory exhausted");

  char *seq_win;
  if (ALLOC_N(seq_win, motif.length + 1))
    error (EXIT_FAILURE, errno, "Memory exhausted");
  for (size_t i=0; i<arguments.n; i++)
    {
      random_seq_win (seq_win, motif.length, source_freqs, rng, motif.type);
      printf ("%s\n", seq_win);
    }
  FREE (seq_win);
  gsl_rng_free (rng);
  free_motif_stats (motif, true);
  return EXIT_SUCCESS;
}
