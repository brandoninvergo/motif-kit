/* Copyright (C) 2019, 2020, 2021 Brandon Invergo <b.invergo@exeter.ac.uk> */

/* This program is free software: you can redistribute it and/or modify */
/* it under the terms of the GNU General Public License as published by */
/* the Free Software Foundation, either version 3 of the License, or */
/* (at your option) any later version. */

/* This program is distributed in the hope that it will be useful, */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the */
/* GNU General Public License for more details. */

/* You should have received a copy of the GNU General Public License */
/* along with this program.  If not, see <http://www.gnu.org/licenses/>. */



#include <config.h>

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <unistd.h>
#include "argp.h"
#include "error.h"
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_statistics.h>
#include <hdf5.h>

#include "motifstats.h"
#include "pssm.h"
#include "sequence.h"
#include "naive-bayes.h"


const char *argp_program_version = PACKAGE_STRING;
const char *argp_program_bug_address = PACKAGE_BUGREPORT;

static char doc[] =
  "motif-view -- print motif statistics in a tabular format."
  "\v"
  "This program prints data stored in MOTIFFILE in a human-readable "
  "tabular format that is also suitable for importing into other programs.  ";

static char args_doc[] = "MOTIFFILE";

static struct argp_option options[] =
  {
   {"delim", 'd', "CHAR", 0,
    "The delimiter character used to separate columns in the table (default: \\t)", 0},
   {"prec", 'p', "VALUE", 0,
    "The integer-valued floating-point precision at which to display values "
    "(default: 6)", 0},
   {"start", 's', "VALUE", 0,
    "The numerical index at which to start column labels (default: 1)", 0},
   {"what", 'w', "VALUE", 0,
    "What aspect of the motif to view: 'pssm' (position-specific scoring matrix) "
    "'pfm' (position frequency matrix), 'bg' (background frequencies), "
    "'ic' (column information content), 'relh' (column relative entropy), "
    "'feat' (Naive Bayes features), 'info' (info about the motif and the methods "
    "used to construct it; default)",
    0},
   {0}
  };

enum print_opt {PRINT_PSSM, PRINT_PFM, PRINT_BG, PRINT_IC, PRINT_RELH, PRINT_FEATS,
  PRINT_INFO};

struct arguments
{
  char *pssm_file;
  char delim;
  unsigned int prec;
  int start;
  enum print_opt what;
};

static error_t
parse_opt (int key, char *arg, struct argp_state *state)
{
  struct arguments *arguments = state->input;
  switch  (key)
    {
    case 'd':
      if (strlen (arg) != 1)
        argp_usage (state);
      arguments->delim = arg[0];
      break;
    case 'p':
      {
        unsigned long int p = strtoul (arg, NULL, 0);
      if (p == ULONG_MAX || p > SIZE_MAX)
        error (EXIT_FAILURE, errno, "Failed to parse precision");
      if (p > 14)
        error (EXIT_FAILURE, 0, "Precision is limited to a maximum of 14");
        arguments->prec = (unsigned int) p;
        break;
      }
    case 's':
      {
        long int p = strtol (arg, NULL, 0);
        if (p == LONG_MIN || p == LONG_MAX)
          error (EXIT_FAILURE, errno, "Error parsing start value");
        if (p > INT_MAX)
          p = INT_MAX;
        else if (p < INT_MIN)
          p = INT_MIN;
        arguments->start = (int) p;
        break;
      }
    case 'w':
      if (!strcmp (arg, "pssm"))
        arguments->what = PRINT_PSSM;
      else if (!strcmp (arg, "pfm"))
        arguments->what = PRINT_PFM;
      else if (!strcmp (arg, "bg"))
        arguments->what = PRINT_BG;
      else if (!strcmp (arg, "ic"))
        arguments->what = PRINT_IC;
      else if (!strcmp (arg, "relh"))
        arguments->what = PRINT_RELH;
      else if (!strcmp (arg, "feat"))
        arguments->what = PRINT_FEATS;
      else if (!strcmp (arg, "info"))
        arguments->what = PRINT_INFO;
      else
        argp_usage (state);
      break;
    case ARGP_KEY_ARG:
      if (state->arg_num >= 1)
        argp_usage (state);
      arguments->pssm_file = arg;
      if (!arguments->pssm_file)
        error (EXIT_FAILURE, errno, "Memory exhausted");
      break;
    default:
      return ARGP_ERR_UNKNOWN;
    }
  return 0;
}

static struct argp argp = {options, parse_opt, args_doc, doc};
char *fail_msg = NULL;


void
init_args (struct arguments *arguments)
{
  arguments->delim = '\t';
  arguments->prec = 6;
  arguments->pssm_file = NULL;
  arguments->start = 1;
  arguments->what = PRINT_INFO;
}


void
print_motif_info (motifstats motif, char delim)
{
  printf ("name%c%s\n", delim, motif.name);
  printf ("sequence type%c", delim);
  if (motif.type == SEQ_AA)
    puts ("amino acid");
  else
    puts ("nucleotide");
  printf ("score method%c", delim);
  if (motif.method == SCORE_LOGLIK)
    puts ("log-likelihood");
  else if (motif.method == SCORE_DIFF)
    puts ("difference");
  else
    puts ("probability");
  printf ("background type%c", delim);
  if (motif.bg == BG_POS)
    puts ("position-specific");
  else if (motif.bg == BG_GLOBAL)
    puts ("global");
  else
    puts ("none");
  printf ("pseudocount n%c", delim);
  if (motif.pc_n == PC_N_CONST)
    puts ("constant");
  else if (motif.pc_n == PC_N_POS)
    puts ("position-specific");
  else
    puts ("none");
  printf ("pseudocount distribution%c", delim);
  if (motif.pc_dist == PC_DIST_EQUAL)
    puts ("uniform");
  else if (motif.pc_dist == PC_DIST_FREQ)
    puts ("background frequency");
  if (motif.weighted)
    printf ("sequence weighting%ctrue\n", delim);
  else
    printf ("sequence weighting%cfalse\n", delim);
  printf ("num. sequences%c%zu\n", delim, motif.n);
  printf ("length%c%zu\n", delim, motif.length);
  printf ("IC base%c%f\n", delim, motif.ic_base);
  printf ("median information content%c%f\n", delim,
          gsl_stats_median (motif.col_ic->data,
                            motif.col_ic->stride,
                            motif.col_ic->size));
  printf ("median relative entropy%c%f\n", delim,
          gsl_stats_median (motif.col_relh->data,
                            motif.col_relh->stride,
                            motif.col_relh->size));
  printf ("Naive Bayes prior probability%c%f\n", delim, motif.prior);
  printf ("Extra Naive Bayes features%c%zu\n", delim, motif.n_feats);
}


void
print_features (motifstats motif, char delim)
{
  if (motif.n_feats == 0)
    puts ("No extra Naive Bayes features.");
  size_t bernoulli_j = 0;
  size_t gaussian_j = 0;
  size_t beta_j = 0;
  for (size_t i=0; i<motif.n_feats; i++)
    {
      switch (motif.feature_cols[i])
        {
        case FEATURE_BERNOULLI:
          {
            double p = motif.bernoulli_feats[bernoulli_j++];
            printf ("%zu. Bernoulli: p = %g\n", i + 1, exp10 (p));
            break;
          }
        case FEATURE_GAUSSIAN:
          {
            double mean = motif.gaussian_feats[gaussian_j].mean;
            double sd = motif.gaussian_feats[gaussian_j++].sd;
            printf ("%zu. Gaussian: mean = %g, std dev. = %g\n", i + 1, mean, sd);
            break;
          }
        case FEATURE_BETA:
          {
            double a = motif.beta_feats[beta_j].a;
            double b = motif.beta_feats[beta_j++].b;
            printf ("%zu. Beta: a = %g, b = %g\n", i + 1, a, b);
            break;
          }
        }
    }
}


int
main (int argc, char **argv)
{
  struct arguments arguments;
  init_args (&arguments);
  argp_parse (&argp, argc, argv, 0, 0, &arguments);

  if (!arguments.pssm_file || !strcmp (arguments.pssm_file, ""))
    error (EXIT_FAILURE, 0, "No motif file specified");
  if (access (arguments.pssm_file, F_OK) == -1)
    error (EXIT_FAILURE, errno, "Motif file %s does not exist",
           arguments.pssm_file);
  hid_t file_id;
  file_id = H5Fopen (arguments.pssm_file, H5F_ACC_RDONLY, H5P_DEFAULT);
  if (!file_id)
    error (EXIT_FAILURE, errno, "Failed to open PSSM file %s",
           arguments.pssm_file);
  motifstats motif;
  read_motif_stats (file_id, &motif);
  H5Fclose (file_id);
  switch (arguments.what)
    {
    case PRINT_PSSM:
      print_pssm (motif.pssm, arguments.prec, arguments.delim, arguments.start);
      break;
    case PRINT_PFM:
      print_pssm (motif.pfm, arguments.prec, arguments.delim, arguments.start);
      break;
    case PRINT_BG:
      print_pssm (motif.bg_freqs, arguments.prec, arguments.delim, arguments.start);
      break;
    case PRINT_IC:
      print_vector (motif.col_ic, arguments.prec, arguments.delim, arguments.start);
      break;
    case PRINT_RELH:
      if (motif.col_relh)
        print_vector (motif.col_relh, arguments.prec, arguments.delim, arguments.start);
      else
        error (EXIT_FAILURE, 0, "Column relative entropy not calculated for this motif");
      break;
    case PRINT_FEATS:
      print_features (motif, arguments.delim);
      break;
    default:
      print_motif_info (motif, arguments.delim);
    }

  free_motif_stats (motif, true);
  return EXIT_SUCCESS;
}
