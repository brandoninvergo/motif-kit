/* Copyright (C) 2019, 2020, 2021 Brandon Invergo <b.invergo@exeter.ac.uk> */

/* This program is free software: you can redistribute it and/or modify */
/* it under the terms of the GNU General Public License as published by */
/* the Free Software Foundation, either version 3 of the License, or */
/* (at your option) any later version. */

/* This program is distributed in the hope that it will be useful, */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the */
/* GNU General Public License for more details. */

/* You should have received a copy of the GNU General Public License */
/* along with this program.  If not, see <http://www.gnu.org/licenses/>. */


#ifndef SEQUENCES_H
#define SEQUENCES_H

#include <config.h>

#include <stdio.h>

#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <regex.h>
#include "error.h"
#include "gl_linkedhash_list.h"
#include "gl_xlist.h"
#include "safe-alloc.h"
#include <gsl/gsl_matrix.h>

#include "kseq.h"
#include "sequence.h"

typedef struct seq_rec { char *seq; double weight; } seq_rec;

bool seq_recs_equal (const void *elt1, const void *elt2);
size_t seq_rec_hash (const void *elt);
void read_sequences (seq_set *seq_recs, const char *fasta_file, seq_type type);
void clear_global_seq_set (seq_set *seq_recs);
gsl_matrix *calc_global_bg (const seq_set seqs);

#endif
