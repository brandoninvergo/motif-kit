/* Copyright (C) 2019, 2020 Brandon Invergo <b.invergo@exeter.ac.uk> */

/* This program is free software: you can redistribute it and/or modify */
/* it under the terms of the GNU General Public License as published by */
/* the Free Software Foundation, either version 3 of the License, or */
/* (at your option) any later version. */

/* This program is distributed in the hope that it will be useful, */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the */
/* GNU General Public License for more details. */

/* You should have received a copy of the GNU General Public License */
/* along with this program.  If not, see <http://www.gnu.org/licenses/>. */



#include <config.h>

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "argp.h"
#include "error.h"
#include <hdf5.h>

#include "motifstats.h"
#include "pssm.h"


const char *argp_program_version = PACKAGE_STRING;
const char *argp_program_bug_address = PACKAGE_BUGREPORT;

static char doc[] =
  "motif-dist -- calculate the distances between a set of motifs."
  "\v"
  "This program calculates and prints the pairwise distances between the motifs "
  "passed as arguments.  The distance is calulated as either: the Frobenius norm "
  "of the difference of the two motifs' PSSMs or PFMs; or as the total "
  "symmetrised Kullback-Leibler divergence across all positions of the two "
  "motifs' PFMs.";

static char args_doc[] = "MOTIFFILE1 MOTIFFILE2 [MOTIFFILE3 ...]";

static struct argp_option options[] =
  {
   {"what", 'w', "MATRIX", 0, "Which matrix to use: pssm (default), pfm", 0},
   {"method", 'm', "NAME", 0, "The distance metric to use: frob (frobenius "
    "norm; default), kl (total Kullback-Leibler divergence; implies "
    "--what=pfm)", 0},
   {0}
  };

struct arguments
{
  char **motif_files;
  size_t n;
  dist_method method;
  bool use_pfm;
};

static error_t
parse_opt (int key, char *arg, struct argp_state *state)
{
  struct arguments *arguments = state->input;
  switch  (key)
    {
    case 'm':
      if (!strcmp (arg, "frob"))
        arguments->method = DIST_FROB;
      else if (!strcmp (arg, "kl"))
        {
          arguments->method = DIST_KL;
          arguments->use_pfm = true;
        }
      else
        argp_usage (state);
      break;
    case 'w':
      arguments->use_pfm = true;
      break;
    case ARGP_KEY_ARG:
      {
        state->next--;
        arguments->motif_files = &state->argv[state->next];
        state->next = state->argc;
        break;
      }
    default:
      return ARGP_ERR_UNKNOWN;
    }
  return 0;
}

static struct argp argp = {options, parse_opt, args_doc, doc};
char *fail_msg = NULL;


void
init_args (struct arguments *arguments)
{
  arguments->method = DIST_FROB;
  arguments->use_pfm = false;
  return;
}


size_t
read_motifs (char **motif_files, motifstats **motifs)
{
  size_t n = 0;
  for (size_t i=0; motif_files[i]; i++)
    {
      if (access (motif_files[i], F_OK) == -1)
        error (EXIT_FAILURE, errno, "Motif file %s does not exist",
               motif_files[i]);
      hid_t file_id;
      file_id = H5Fopen (motif_files[i], H5F_ACC_RDONLY, H5P_DEFAULT);
      if (file_id < 0)
        error (EXIT_FAILURE, errno, "Failed to open motif file %s",
               motif_files[i]);
      if (n > 1)
        if (REALLOC_N (*motifs, n + 1))
          error (EXIT_FAILURE, errno, "Memory exhausted");
      read_motif_stats (file_id, &(*motifs)[i]);
      H5Fclose (file_id);
      n++;
    }
  return n;
}


int
main (int argc, char **argv)
{
  struct arguments arguments;
  init_args (&arguments);
  argp_parse (&argp, argc, argv, 0, 0, &arguments);
  motifstats *motifs;
  if (ALLOC_N (motifs, 2))
    error (EXIT_FAILURE, errno, "Memory exhausted");
  size_t n = read_motifs (arguments.motif_files, &motifs);
  if (n < 2)
    {
      free_motif_stats (motifs[0], true);
      free_motif_stats (motifs[1], true);
      FREE (motifs);
      error (EXIT_FAILURE, 0, "Need at least two motifs");
    }
  puts ("motif1\tmotif2\tdistance");
  for (size_t i=0; i<n; i++)
    {
      for (size_t j=i+1; j<n; j++)
        {
          gsl_matrix *m1, *m2;
          if (arguments.use_pfm)
            {
              m1 = motifs[i].pfm;
              m2 = motifs[j].pfm;
            }
          else
            {
              m1 = motifs[i].pssm;
              m2 = motifs[j].pssm;
            }
          double dist;
          if (arguments.method == DIST_FROB)
            dist = pssm_frob_distance (m1, m2);
          else
            dist = pfm_kl_distance (m1, m2, motifs[i].ic_base);
          printf ("%s\t%s\t%f\n", motifs[i].name, motifs[j].name, dist);
        }
    }

  for (size_t i=0; i<n; i++)
    {
      free_motif_stats (motifs[i], true);
    }
  FREE (motifs);
  return EXIT_SUCCESS;
}
