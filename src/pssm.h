/* Copyright (C) 2019, 2020, 2021 Brandon Invergo <b.invergo@exeter.ac.uk> */

/* This program is free software: you can redistribute it and/or modify */
/* it under the terms of the GNU General Public License as published by */
/* the Free Software Foundation, either version 3 of the License, or */
/* (at your option) any later version. */

/* This program is distributed in the hope that it will be useful, */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the */
/* GNU General Public License for more details. */

/* You should have received a copy of the GNU General Public License */
/* along with this program.  If not, see <http://www.gnu.org/licenses/>. */

#ifndef PSSM_H
#define PSSM_H

#include <config.h>
#include <errno.h>
#include <math.h>
#include <stdbool.h>
#include "error.h"
#include "gl_linkedhash_list.h"
#include "gl_xlist.h"
#include "safe-alloc.h"
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_math.h>

#include "seq-wins.h"
#include "sequence.h"

typedef enum pc_dist_method {PC_DIST_EQUAL, PC_DIST_FREQ} pc_dist_method;
typedef enum pc_n_method {PC_N_CONST, PC_N_POS, PC_N_NONE} pc_n_method;
typedef enum score_method {SCORE_LOGLIK, SCORE_DIFF, SCORE_FREQ, SCORE_BAYES} score_method;
typedef enum bg_type {BG_NONE, BG_GLOBAL, BG_POS} bg_type;
typedef enum scale_method {SCALE_NONE, SCALE_IC, SCALE_RELH, SCALE_GR} scale_method;
typedef enum dist_method {DIST_FROB, DIST_KL} dist_method;

void count_pos_res (const seq_set *seq_wins, gsl_vector *total_emp_counts,
                    gsl_vector *total_pos_counts, gsl_matrix *emp_counts,
                    const gsl_matrix *bg_freqs, bool weighted);
void calc_num_pos_pc (const gsl_vector *total_emp_counts, gsl_vector *num_pc,
                      double m);
void calc_pos_freqs (gsl_matrix *pfm, const gsl_vector *total_pos_counts,
                     const gsl_matrix *bg_freqs, const gsl_vector *num_pc_pos,
                     double num_pc, bg_type bg, pc_n_method pc_n_meth,
                     pc_dist_method pc_dist_meth);
void calc_col_ic (const gsl_matrix *pfm, gsl_vector *col_ic, double base);
void calc_col_relh (const gsl_matrix *pssm, const gsl_matrix *pfm,
                    gsl_vector *col_relh);
void calc_col_gr (gsl_vector *gain_ratio, const gsl_matrix *pfm,
                  const gsl_matrix *compl_pfm, double prior, double compl_prior,
                  const gsl_vector *relh, const gsl_vector *compl_relh,
                  const gsl_matrix *bg, double base);
void calc_fc_score (gsl_matrix *pssm, const gsl_matrix *pfm,
                    const gsl_matrix *bg_freqs, bg_type bg, double base);
void calc_diff_score (gsl_matrix *pssm, const gsl_matrix *pfm,
                      const gsl_matrix *bg_freqs, bg_type bg, double scale);
void calc_freq_score (gsl_matrix *pssm, const gsl_matrix *pfm, double base);
double score_sequence (const char *seq, const gsl_matrix *pssm,
                       const gsl_vector *col_weight, seq_type type,
                       score_method method, bool normalize, size_t *omit,
                       size_t n_omit);
void print_pssm (const gsl_matrix *pssm, unsigned int precision, char delim, int start_i);
void print_vector (const gsl_vector *v, unsigned int precision, char delim, int start_i);
double pssm_frob_distance (const gsl_matrix *pssm1, const gsl_matrix *pssm2);
double pfm_kl_distance (const gsl_matrix *pfm1, const gsl_matrix *pfm2,
                        double base);

#endif
