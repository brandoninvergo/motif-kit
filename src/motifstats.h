/* Copyright (C) 2019, 2020, 2021 Brandon Invergo <b.invergo@exeter.ac.uk> */

/* This program is free software: you can redistribute it and/or modify */
/* it under the terms of the GNU General Public License as published by */
/* the Free Software Foundation, either version 3 of the License, or */
/* (at your option) any later version. */

/* This program is distributed in the hope that it will be useful, */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the */
/* GNU General Public License for more details. */

/* You should have received a copy of the GNU General Public License */
/* along with this program.  If not, see <http://www.gnu.org/licenses/>. */


#ifndef MOTIFSTATS_H
#define MOTIFSTATS_H

#include <config.h>
#include <errno.h>
#include "error.h"
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_vector.h>
#include <hdf5.h>
#include <hdf5_hl.h>

#include "sequence.h"
#include "pssm.h"
#include "naive-bayes.h"

typedef struct motifstats {
  char *name;
  gsl_matrix *pssm;             /* position-specific scoring matrix */
  gsl_matrix *pfm;              /* position frequency matrix */
  gsl_matrix *bg_freqs;         /* background frequencies */
  gsl_vector *col_ic;           /* information content */
  gsl_vector *col_relh;        /* relative entropy */
  seq_type type;
  score_method method;
  bg_type bg;
  pc_n_method pc_n;
  pc_dist_method pc_dist;
  bool weighted;
  size_t n;                     /* # sequences counted */
  size_t length;                /* window length */
  double ic_base;
  double prior;                 /* prior probability for Naive Bayes */
  bayes_feat *feature_cols;
  size_t n_feats;
  double *bernoulli_feats;      /* parameters for
                                   Bernoulli-distributed features for
                                   N.B. */
  size_t n_bernoulli;
  gaussian_dist *gaussian_feats; /* parameters for
                                    Gaussian-distributed features for
                                    N.B. */
  size_t n_gaussian;
  beta_dist *beta_feats;        /* parameters for Beta-distributed
                                    features for N.B. */
  size_t n_beta;
} motifstats;

void write_motif_stats (hid_t file_id, motifstats motif, char *colophon);
void read_motif_stats (hid_t file_id, motifstats *pssm);
void free_motif_stats (motifstats motif, bool free_name);
void calc_motif_stats (motifstats *motif, const seq_set *seq_wins,
                       const gsl_matrix *bg_freqs, double pseudo_p,
                       double diff_scale);


#endif
