/* Copyright (C) 2019, 2020, 2021 Brandon Invergo <b.invergo@exeter.ac.uk> */

/* This program is free software: you can redistribute it and/or modify */
/* it under the terms of the GNU General Public License as published by */
/* the Free Software Foundation, either version 3 of the License, or */
/* (at your option) any later version. */

/* This program is distributed in the hope that it will be useful, */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the */
/* GNU General Public License for more details. */

/* You should have received a copy of the GNU General Public License */
/* along with this program.  If not, see <http://www.gnu.org/licenses/>. */



#include <config.h>

#include <errno.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <stdint.h>
#include <string.h>
#include "argp.h"
#include "error.h"
#include <gsl/gsl_math.h>
#include "gl_linkedhash_list.h"
#include "gl_xlist.h"
#include "safe-alloc.h"

#include "motifstats.h"
#include "pssm.h"
#include "sequences.h"
#include "seq-wins.h"
#include "sequence.h"
#include "naive-bayes.h"


const char *argp_program_version = PACKAGE_STRING;
const char *argp_program_bug_address = PACKAGE_BUGREPORT;

static char doc[] =
  "motif-cv -- perform k-fold cross validation on a motif"
  "\v"
  "This program performs k-fold cross validation on a motif statistics.  For  "
  "each iteration, a PSSM is constructed from a portion of the \"positive\" "
  "training data in POSFILE and then it uses this PSSM to score \"negative\" "
  "and \"positive\" sequences from NEGFILE and the remainder of POSFILE.  "
  "The results are provided in three columns: iteration number, correct label "
  "(TRUE: positive, FALSE: negative), and PSSM score.  These results can then "
  "used for ROC analysis, etc.  Both NEGFILE and POSFILE should only contain "
  "sequence windows.";

static char args_doc[] = "POSFILE NEGFILE";

static struct argp_option options[] =
  {
   {0, 0, 0, 0, "Background", 1},
   {"bg-type", 'b', "TYPE", 0,
    "The type of background frequencies to use: 'none' (do not consider "
    "background frequencies; default), 'global' (the same background "
    "frequencies are applied to all columns), 'pos' (position-specific "
    "frequencies are applied)", 0},
   {"bg-file", 'f', "FILE", 0,
    "The file from which background frequencies should be calculated.  If "
    "--bg-type=global, FILE must be a FASTA file.  If --bg-type=pos, FILE "
    "must contain sequence windows (one sequence per line) of the same number "
    "of columns as the PSSM will have.", 0},
   {0, 0, 0, 0, "Pseudocounts", 2},
   {"pseudo-dist", 'd', "METHOD", 0,
    "How pseudocounts are distributed: 'equal' (pseudocounts distributed "
    "equally; default), 'freq' (pseudocounts are distributed according to "
    "a background frequency; requires option --bg-type to be 'global' or "
    "'pos' and is the default if a background is specified)", 0},
   {"pseudo-n", 'n', "METHOD", 0,
    "How the number of pseudocounts for each column is determined: 'const' "
    "(a constant number of pseudocounts are added to each column), "
    "'pos' (the number of pseudocounts increases with the empirical diversity "
    "of a column; default), 'none'", 0},
   {"pseudo-param", 'p', "NUM", 0,
    "A parameter (>= 0) for calculating the number of pseudocounts, depending "
    "on the method: for --pseudo-n=const, NUM is simply the number of "
    "pseudocounts (default: 1); for --pseudo-n=pos, NUM is multiplied by the "
    "diversity count (default: 1)", 0},
   {0, 0, 0, 0, "Methods", 3},
   {"no-weight-bg", 'v', 0, 0, "Do not perform sequence weighting on background "
    "sequences (for --bg-type=pos only)", 0},
   {"no-weight", 'w', 0, 0, "Do not perform sequence weighting", 0},
   {"ic-base", 301, "VALUE", 0, "Logarithmic base > 0.0 for calculating "
    "information content, determining the units of the measurement (e.g. 2: "
    "bits (default), e: nats).", 0},
   {"score-method", 's', "METHOD", 0,
    "The position-specific scoring method: 'loglik' (log-likelihood ratio, "
    "i.e. fold-change relative to background frequencies, log-transformed with "
    "base specified by --ic-base; default); 'diff' (scaled difference from "
    "background frequencies); 'freq' (likelihood, i.e. position-specific "
    "frequencies, log10-transformed); or 'bayes' (Naive Bayesian posterior "
    "probability -- see documentation below)", 0},
   {"diff-scale", 'l', "VALUE", 0, "Exponential scale value to apply to the "
    "absolute difference from the background frequencies in difference-based "
    "scoring (default: 1.2)", 0},
   {"scale", 'c', "BY", 0, "Scale each column in the PSSM by: 'ic' (information "
    "content); 'relh' (relative entropy vs the background); 'gr' (gain ratio, "
    "Naive Bayes scoring only); or 'none' (default)",
    0},
   {"k", 'k', "VALUE", 0, "Number of folds to use in k-fold cross-validation "
    "(default: 5)", 0},
   {"omit", 'o', "POSITIONS", 0, "Omit the listed, comma-separated POSITIONS "
    "from the final score calculation", 0},
   {"features", 304, "DISTRIBUTION[,DISTRIBUTION,...]", 0, "Read extra features "
    "from SEQ_FILE and the --bg-file FILE.  For each column in SEQ_FILE/FILE, a "
    "DISTRIBUTION is specified (in order), from which probabilities will be "
    "estimated.  Requires --score-method=bayes.  Available DISTRIBUTION values "
    "are: 'bernoulli', 'gaussian', 'beta'.", 0},
   {"feature-delim", 305, "CHAR", 0, "Character used to deliminate columns in "
    "SEQ_FILE and --bg-file FILE when other features are present.  Default: '\\t' "
    "(tab).", 0},
   {"smoothing", 306, "ALPHA", 0, "Additive smoothing parameter (i.e. constant "
    "pseudocount) for Bernoulli feature parameter estimation. Must be >=0 (0.0 "
    "means no smoothing). Default: 1.0.", 0},
   {"squeeze", 307, "S", OPTION_ARG_OPTIONAL, "Squeeze Beta-distributed data with "
    "0 or 1 values to be in the interval (0, 1).  The transformation is "
    "(x*(N - 1) + s)/N, where x is the value, N is the number of samples, and s "
    "is a value between 0 and 1. Default: 0.5", 0},
   {0}
  };

struct arguments
{
  char *args[2];
  pc_dist_method pseudo_dist;
  pc_n_method pseudo_n;
  score_method method;
  double pseudo_p;
  double diff_scale;
  bool weight;
  bool weight_bg;
  double ic_base;
  bg_type bg;
  char *bg_file;
  scale_method scale;
  size_t k;
  size_t *omit;
  size_t n_omit;
  bayes_feat *features;
  size_t n_feats;
  char feat_delim;
  double alpha;
  bool squeeze;
  double s;
};

static error_t
parse_opt (int key, char *arg, struct argp_state *state)
{
  struct arguments *arguments = state->input;
  switch  (key)
    {
    case 'b':
      if (!strcmp (arg, "none"))
        {
        arguments->bg = BG_NONE;
        }
      else if (!strcmp (arg, "global"))
        {
          arguments->bg = BG_GLOBAL;
        }
      else if (!strcmp (arg, "pos"))
        {
          arguments->bg = BG_POS;
        }
      else
        {
          argp_usage (state);
        }
      break;
    case 'c':
      if (!strcmp (arg, "ic"))
        arguments->scale = SCALE_IC;
      else if (!strcmp (arg, "relh"))
        arguments->scale = SCALE_RELH;
      else if (!strcmp (arg, "gr"))
        arguments->scale = SCALE_GR;
      else if (!strcmp (arg, "none"))
        arguments->scale = SCALE_NONE;
      else
        argp_usage (state);
      break;
    case 'd':
      if (!strcmp (arg, "equal"))
        {
        arguments->pseudo_dist = PC_DIST_EQUAL;
        }
      else if (!strcmp (arg, "freq"))
        {
          arguments->pseudo_dist = PC_DIST_FREQ;
        }
      else
        {
          argp_usage (state);
        }
      break;
    case 'f':
      if (!arg || strlen (arg) == 0)
        argp_usage (state);
      else
        {
          arguments->bg_file = arg;
          if (!arguments->bg_file)
            error (EXIT_FAILURE, errno, "Memory exhausted");
        }
      break;
    case 'k':
      {
        unsigned long k = strtoul (arg, NULL, 0);
        if (k == ULONG_MAX && errno == ERANGE)
          error (EXIT_FAILURE, errno, "Overflow when parsing k");
        if (k > SIZE_MAX)
          error (EXIT_FAILURE, 0, "Overflow when parsing k");
        if (k <= 1)
          error (EXIT_FAILURE, 0, "k must be > 1");
        arguments->k = (size_t) k;
        break;
      }
    case 'l':
      if (!arg || strlen (arg) == 0)
        arguments->diff_scale = 1.2;
      else
        {
          arguments->diff_scale = strtod (arg, NULL);
          if (!arguments->diff_scale)
            argp_usage (state);
        }
      break;
    case 'n':
      if (!strcmp (arg, "const"))
        {
        arguments->pseudo_n = PC_N_CONST;
        }
      else if (!strcmp (arg, "pos"))
        {
          arguments->pseudo_n = PC_N_POS;
        }
      else if (!strcmp (arg, "none"))
        {
          arguments->pseudo_n = PC_N_NONE;
        }
      else
        {
          argp_usage (state);
        }
      break;
    case 'o':
      {
        size_t maxomit = 15;
        if (ALLOC_N (arguments->omit, maxomit))
          error (EXIT_FAILURE, errno, "Memory exhausted");
        char *index = strtok (arg, ",");
        if (index == NULL)
          argp_usage (state);
        size_t n_omit = 0;
        while (index)
          {
            unsigned long int i = strtoul (arg, NULL, 0);
            if (i == ULONG_MAX && errno == ERANGE)
              error (EXIT_FAILURE, errno, "Overflow when parsing a position");
            if (i > SIZE_MAX)
              error (EXIT_FAILURE, 0, "Overflow when parsing position");
            if (i == 0)
              error (EXIT_FAILURE, 0, "Positions to omit should be 1-indexed");
            if (n_omit >= maxomit)
              {
                maxomit += 10;
                if (REALLOC_N (arguments->omit, maxomit))
                  error (EXIT_FAILURE, errno, "Memory exhausted");
              }
            arguments->omit[n_omit++] = i - 1;
            index = strtok (NULL, ",");
          }
        arguments->n_omit = n_omit;
        break;
      }
    case 'p':
      arguments->pseudo_p = strtod (arg, NULL);
      if (arguments->pseudo_p == HUGE_VAL)
        error (EXIT_FAILURE, errno, "Overflow or underflow when parsing "
               "the pseudocounts parameter");
      if (arguments->pseudo_p < 0)
        argp_usage (state);
      break;
    case 's':
      if (!arg || strlen (arg) == 0)
        arguments->method = SCORE_LOGLIK;
      else if (!strcmp (arg, "loglik"))
        {
          arguments->method = SCORE_LOGLIK;
        }
      else if (!strcmp (arg, "diff"))
        {
          arguments->method = SCORE_DIFF;
        }
      else if (!strcmp (arg, "freq"))
        {
          arguments->method = SCORE_FREQ;
        }
      else if (!strcmp (arg, "bayes"))
        {
          arguments->method = SCORE_BAYES;
        }
      else
        argp_usage (state);
      break;
    case 'v':
      arguments->weight_bg = false;
      break;
    case 'w':
      arguments->weight = false;
      break;
    case 301:
      if (!arg || strlen (arg) == 0 || !strcmp (arg, "e"))
        arguments->ic_base = M_E;
      else
        {
          arguments->ic_base = strtod (arg, NULL);
          if (arguments->ic_base <= 0.0)
            argp_usage (state);
        }
      break;
    case 304:
      {
        char *feature = strtok (arg, ",");
        if (feature == NULL)
          argp_usage (state);
        size_t max_feats = 3;
        if (ALLOC_N (arguments->features, max_feats))
          error (EXIT_FAILURE, errno, "Memory exhausted");
        size_t n_feats = 0;
        while (feature)
          {
            if (n_feats >= max_feats)
              {
                max_feats += 3;
                if (REALLOC_N (arguments->features, max_feats))
                  error (EXIT_FAILURE, errno, "Memory exhausted");
              }
            if (!strcmp (feature, "bernoulli"))
              arguments->features[n_feats++] = FEATURE_BERNOULLI;
            else if (!strcmp (feature, "gaussian"))
              arguments->features[n_feats++] = FEATURE_GAUSSIAN;
            else if (!strcmp (feature, "beta"))
              arguments->features[n_feats++] = FEATURE_BETA;
            else
              argp_usage (state);
            feature = strtok (NULL, ",");
          }
        arguments->n_feats = n_feats;
        break;
      }
    case 305:
      arguments->feat_delim = arg[0];
      break;
    case 306:
      arguments->alpha = strtod (arg, NULL);
      if (arguments->alpha == HUGE_VAL)
        error (EXIT_FAILURE, errno, "Overflow or underflow when parsing "
               "the alpha smoothing parameter");
      if (arguments->alpha < 0)
        argp_usage (state);
      break;
    case 307:
      arguments->squeeze = true;
      if (arg)
        {
          arguments->s = strtod (arg, NULL);
          if (arguments->s == HUGE_VAL)
            error (EXIT_FAILURE, errno, "Overflow or underflow when parsing "
                   "the Beta squeeze parameter");
          if (arguments->s < 0 || arguments->s > 1)
            argp_usage (state);
        }
      break;
    case ARGP_KEY_ARG:
      if (state->arg_num >= 2)
        argp_usage (state);
      arguments->args[state->arg_num] = arg;
      if (!arguments->args[state->arg_num])
        error (EXIT_FAILURE, errno, "Memory exhausted");
      break;
    case ARGP_KEY_END:
      break;
    default:
      return ARGP_ERR_UNKNOWN;
    }
  return 0;
}

static struct argp argp = {options, parse_opt, args_doc, doc};
char *fail_msg = NULL;


void
init_args (struct arguments *arguments)
{
  arguments->args[0] = NULL;
  arguments->args[1] = NULL;
  arguments->pseudo_dist = PC_DIST_EQUAL;
  arguments->pseudo_n = PC_N_POS;
  arguments->pseudo_p = 1.0;
  /* Safei et al. use 1.2 as their chosen difference scale */
  arguments->diff_scale = 1.2;
  arguments->method = SCORE_LOGLIK;
  arguments->weight = true;
  arguments->weight_bg = true;
  arguments->ic_base = 2.0;
  arguments->bg = BG_NONE;
  arguments->bg_file = NULL;
  arguments->scale = SCALE_NONE;
  arguments->k = 5;
  arguments->omit = NULL;
  arguments->n_omit = 0;
  arguments->features = NULL;
  arguments->n_feats = 0;
  arguments->feat_delim = '\t';
  arguments->alpha = 1.0;
  arguments->squeeze = false;
  arguments->s = 0.5;
}


void
get_train_set (seq_set *test_set, seq_set *seq_wins, bool ***train_bernoulli_dat,
               bool **bernoulli_dat, double ***train_gaussian_dat,
               double **gaussian_dat, double ***train_beta_dat, double **beta_dat,
               size_t n_bernoulli, size_t n_gaussian, size_t n_beta,
               bayes_feat *features, size_t n_feats, size_t start, size_t end)
{
  size_t num_recs = gl_list_size (seq_wins->seqs) - (end - start);
  if (n_bernoulli > 0)
    {
      if (ALLOC_N (*train_bernoulli_dat, n_bernoulli))
        error (EXIT_FAILURE, errno, "Memory exhausted");
      for (size_t i=0; i<n_bernoulli; i++)
        {
          if (ALLOC_N ((*train_bernoulli_dat)[i], num_recs))
            error (EXIT_FAILURE, errno, "Memory exhausted");
        }
    }
  if (n_gaussian > 0)
    {
      if (ALLOC_N (*train_gaussian_dat, n_gaussian))
        error (EXIT_FAILURE, errno, "Memory exhausted");
      for (size_t i=0; i<n_gaussian; i++)
        {
          if (ALLOC_N ((*train_gaussian_dat)[i], num_recs))
            error (EXIT_FAILURE, errno, "Memory exhausted");
        }
    }
  if (n_beta > 0)
    {
      if (ALLOC_N (*train_beta_dat, n_beta))
        error (EXIT_FAILURE, errno, "Memory exhausted");
      for (size_t i=0; i<n_beta; i++)
        {
          if (ALLOC_N ((*train_beta_dat)[i], num_recs))
            error (EXIT_FAILURE, errno, "Memory exhausted");
        }
    }
  size_t j=0;
  gl_list_iterator_t i = gl_list_iterator (seq_wins->seqs);
  const void *eltp;
  size_t si = 0;
  while (gl_list_iterator_next (&i, &eltp, NULL))
    {
      seq_win *win = (seq_win *) eltp;
      seq_win *win_cpy;
      if (si < start || si >= end)
        {
          if (ALLOC (win_cpy))
            error (EXIT_FAILURE, errno, "Memory exhausted");
          win_cpy->seq = strdup (win->seq);
          if (!win_cpy->seq)
            error (EXIT_FAILURE, errno, "Memory exhausted");
          if (win->id)
            {
              win_cpy->id = strdup (win->id);
              if (!win_cpy->id)
                error (EXIT_FAILURE, errno, "Memory exhausted");
            }
          win_cpy->weight = win->weight;
          if (!gl_list_nx_add_last (test_set->seqs, win_cpy))
            error (EXIT_FAILURE, errno, "Memory exhausted");
          size_t bernoulli_j = 0;
          size_t gaussian_j = 0;
          size_t beta_j = 0;
          for (size_t feat=0; feat<n_feats; feat++)
            {
              switch (features[feat])
                {
                case FEATURE_BERNOULLI:
                  {
                    (*train_bernoulli_dat)[bernoulli_j][j] = bernoulli_dat[bernoulli_j][si];
                    bernoulli_j++;
                    break;
                  }
                case FEATURE_GAUSSIAN:
                  {
                    (*train_gaussian_dat)[gaussian_j][j] = gaussian_dat[gaussian_j][si];
                    gaussian_j++;
                    break;
                  }
                case FEATURE_BETA:
                  {
                    (*train_beta_dat)[beta_j][j] = beta_dat[beta_j][si];
                    beta_j++;
                    break;
                  }
                }
            }
          j++;
        }
      si++;
    }
  gl_list_iterator_free (&i);
}


void
get_test_set (seq_set *test_set, seq_set *seq_wins, bool ***test_bernoulli_dat,
              bool **bernoulli_dat, double ***test_gaussian_dat,
              double **gaussian_dat, double ***test_beta_dat,
              double **beta_dat, size_t n_bernoulli, size_t n_gaussian,
              size_t n_beta, bayes_feat *features, size_t n_feats,
              size_t start, size_t end)
{
  size_t num_recs = end - start;
  if (n_bernoulli > 0)
    {
      if (ALLOC_N (*test_bernoulli_dat, n_bernoulli))
        error (EXIT_FAILURE, errno, "Memory exhausted");
      for (size_t i=0; i<n_bernoulli; i++)
        {
          if (ALLOC_N ((*test_bernoulli_dat)[i], num_recs))
            error (EXIT_FAILURE, errno, "Memory exhausted");
        }
    }
  if (n_gaussian > 0)
    {
      if (ALLOC_N (*test_gaussian_dat, n_gaussian))
        error (EXIT_FAILURE, errno, "Memory exhausted");
      for (size_t i=0; i<n_gaussian; i++)
        {
          if (ALLOC_N ((*test_gaussian_dat)[i], num_recs))
            error (EXIT_FAILURE, errno, "Memory exhausted");
        }
    }
  if (n_beta > 0)
    {
      if (ALLOC_N (*test_beta_dat, n_beta))
        error (EXIT_FAILURE, errno, "Memory exhausted");
      for (size_t i=0; i<n_beta; i++)
        {
          if (ALLOC_N ((*test_beta_dat)[i], num_recs))
            error (EXIT_FAILURE, errno, "Memory exhausted");
        }
    }
  size_t j = 0, si = 0;
  gl_list_iterator_t i = gl_list_iterator (seq_wins->seqs);
  const void *eltp;
  while (gl_list_iterator_next (&i, &eltp, NULL))
    {
      seq_win *win = (seq_win *) eltp;
      seq_win *win_cpy;
      if (si >= start && si < end)
        {
          if (ALLOC (win_cpy))
            error (EXIT_FAILURE, errno, "Memory exhausted");
          win_cpy->seq = strdup (win->seq);
          if (!win_cpy->seq)
            error (EXIT_FAILURE, errno, "Memory exhausted");
          if (win->id)
            {
              win_cpy->id = strdup (win->id);
              if (!win_cpy->id)
                error (EXIT_FAILURE, errno, "Memory exhausted");
            }
          win_cpy->weight = win->weight;
          if (!gl_list_nx_add_last (test_set->seqs, win_cpy))
            error (EXIT_FAILURE, errno, "Memory exhausted");
          size_t bernoulli_j = 0;
          size_t gaussian_j = 0;
          size_t beta_j = 0;
          for (size_t feat=0; feat<n_feats; feat++)
            {
              switch (features[feat])
                {
                case FEATURE_BERNOULLI:
                  {
                    (*test_bernoulli_dat)[bernoulli_j][j] = bernoulli_dat[bernoulli_j][si];
                    bernoulli_j++;
                    break;
                  }
                case FEATURE_GAUSSIAN:
                  {
                    (*test_gaussian_dat)[gaussian_j][j] = gaussian_dat[gaussian_j][si];
                    gaussian_j++;
                    break;
                  }
                case FEATURE_BETA:
                  {
                    (*test_beta_dat)[beta_j][j] = beta_dat[beta_j][si];
                    beta_j++;
                    break;
                  }
                }
            }
          j++;
        }
      else if (j >= end)
        break;
      si++;
    }
  gl_list_iterator_free (&i);
}


void
setup_bg_freqs (gsl_matrix **bg_freqs, motifstats *bg_motif, seq_set bg_seqs,
                seq_set *bg_sub, seq_set pos_test_set, seq_set neg_test_set,
                bool **bg_bernoulli_dat, double **bg_gaussian_dat,
                double **bg_beta_dat, size_t bg_num_recs, size_t n_bernoulli,
                size_t n_gaussian, size_t n_beta, struct arguments arguments)
{

  if (arguments.weight_bg)
    weight_seq_windows (&bg_seqs);
  seq_set bg_sub_tmp;
  seq_set_diff (bg_seqs, pos_test_set, &bg_sub_tmp);
  seq_set_diff (bg_sub_tmp, neg_test_set, bg_sub);
  clear_seq_win_set (&bg_sub_tmp);
  if (arguments.weight_bg)
    weight_seq_windows (bg_sub);
  bg_motif->name = NULL;
  bg_motif->pssm = NULL;
  bg_motif->pfm = NULL;
  bg_motif->bg_freqs = NULL;
  bg_motif->col_ic = NULL;
  bg_motif->col_relh = NULL;
  bg_motif->ic_base = 2;
  bg_motif->type = bg_seqs.type;
  bg_motif->method = SCORE_FREQ;
  bg_motif->bg = BG_NONE;
  bg_motif->pc_n = PC_N_NONE;
  bg_motif->pc_dist = PC_DIST_EQUAL;
  bg_motif->n = gl_list_size (bg_seqs.seqs);
  bg_motif->length = bg_seqs.win_len;
  bg_motif->weighted = arguments.weight_bg;
  calc_motif_stats (bg_motif, bg_sub, NULL, 0.0, 0.0);
  *bg_freqs = gsl_matrix_alloc (bg_motif->pfm->size1, bg_motif->pfm->size2);
  if (!(*bg_freqs))
    error (EXIT_FAILURE, errno, "Memory exhausted");
  gsl_matrix_memcpy (*bg_freqs, bg_motif->pfm);
  bg_motif->n_feats = arguments.n_feats;
  if (arguments.n_feats == 0)
    {
      bg_motif->feature_cols = NULL;
      bg_motif->bernoulli_feats = NULL;
      bg_motif->n_bernoulli = 0;
      bg_motif->gaussian_feats = NULL;
      bg_motif->n_gaussian = 0;
      bg_motif->beta_feats = NULL;
      bg_motif->n_beta = 0;
    }
  else
    {
      /* Just allocate space for the background feature parameters
         now.  The values will be filled in later. */
      if (ALLOC_N (bg_motif->feature_cols, bg_motif->n_feats))
        error (EXIT_FAILURE, errno, "Memory exhausted");
      memcpy (bg_motif->feature_cols, arguments.features,
              bg_motif->n_feats * (sizeof (bayes_feat)));
      bg_motif->n_bernoulli = n_bernoulli;
      if (ALLOC_N (bg_motif->bernoulli_feats, n_bernoulli))
        error (EXIT_FAILURE, errno, "Memory exhausted");
      bg_motif->n_gaussian = n_gaussian;
      if (ALLOC_N (bg_motif->gaussian_feats, n_gaussian))
        error (EXIT_FAILURE, errno, "Memory exhausted");
      bg_motif->n_beta = n_beta;
      if (ALLOC_N (bg_motif->beta_feats, n_beta))
        error (EXIT_FAILURE, errno, "Memory exhausted");
    }
}


void
calc_compl_feats (seq_set test_train_wins, seq_set bg_wins,
                  bool ***compl_bernoulli, double ***compl_gaussian,
                  double ***compl_beta,
                  bool **bg_bernoulli_dat, double **bg_gaussian_dat,
                  double **bg_beta_dat, size_t n_bernoulli,
                  size_t n_gaussian, size_t n_beta,
                  size_t bg_num_recs)
{
  /* Calculate feature parameters for the "complementary motif".  To
     do this, use all of the instances that aren't in the test set and
     the training set. */
  size_t n_test_train = gl_list_size (test_train_wins.seqs);
  size_t n_bg = gl_list_size (bg_wins.seqs);
  if (n_test_train >= n_bg)
    error (EXIT_FAILURE, 0, "The test/train windows size is somehow bigger than "
           "the background set.  Bailing out.");
  /* Get the "complementary" set size */
  size_t n_compl = n_bg - n_test_train;
  /* Allocate space for subsets of the feature data */
  if (ALLOC_N (*compl_bernoulli, n_bernoulli))
    error (EXIT_FAILURE, errno, "Memory exhausted");
  for (size_t i=0; i<n_bernoulli; i++)
    {
      if (ALLOC_N ((*compl_bernoulli)[i], n_compl))
        error (EXIT_FAILURE, errno, "Memory exhausted");
    }
  if (ALLOC_N (*compl_gaussian, n_gaussian))
    error (EXIT_FAILURE, errno, "Memory exhausted");
  for (size_t i=0; i<n_gaussian; i++)
    {
      if (ALLOC_N ((*compl_gaussian)[i], n_compl))
        error (EXIT_FAILURE, errno, "Memory exhausted");
    }
  if (ALLOC_N (*compl_beta, n_beta))
    error (EXIT_FAILURE, errno, "Memory exhausted");
  for (size_t i=0; i<n_beta; i++)
    {
      if (ALLOC_N ((*compl_beta)[i], n_compl))
        error (EXIT_FAILURE, errno, "Memory exhausted");
    }
  size_t bernoulli_i = 0;
  size_t gaussian_i = 0;
  size_t beta_i = 0;
  size_t si = 0;
  /* For each sequence in the background... */
  gl_list_iterator_t i = gl_list_iterator (bg_wins.seqs);
  const void *eltp;
  while (gl_list_iterator_next (&i, &eltp, NULL))
    {
      /* If the sequence window isn't in the the test/training set... */
      const seq_win *win = (seq_win *) eltp;
      if (gl_list_search (test_train_wins.seqs, win))
        continue;
      /* Get the feature data */
      for (size_t j=0; j<n_bernoulli; j++)
        {
          bool value = bg_bernoulli_dat[j][si];
          (*compl_bernoulli)[j][bernoulli_i] = value;
        }
      bernoulli_i++;
      for (size_t j=0; j<n_gaussian; j++)
        {
          double value = bg_gaussian_dat[j][si];
          (*compl_gaussian)[j][gaussian_i] = value;
        }
      gaussian_i++;
      for (size_t j=0; j<n_beta; j++)
        {
          double value = bg_beta_dat[j][si];
          (*compl_beta)[j][beta_i] = value;
        }
      beta_i++;
      si++;
    }
  gl_list_iterator_free (&i);
}


void
setup_motif (motifstats *motif, seq_set *train_set, seq_set bg_sub,
             gsl_matrix *bg_freqs, bool **bernoulli_dat, double **gaussian_dat,
             double **beta_dat, size_t n_bernoulli, size_t n_gaussian,
             size_t n_beta, struct arguments arguments)
{
  if (arguments.weight)
    weight_seq_windows (train_set);
  motif->name = NULL;
  motif->pssm = NULL;
  motif->pfm = NULL;
  motif->bg_freqs = NULL;
  motif->col_ic = NULL;
  motif->col_relh = NULL;
  motif->ic_base = arguments.ic_base;
  motif->type = train_set->type;
  motif->method = arguments.method;
  motif->bg = arguments.bg;
  motif->pc_n = arguments.pseudo_n;
  motif->pc_dist = arguments.pseudo_dist;
  motif->n = gl_list_size(train_set->seqs);
  motif->length = train_set->win_len;
  motif->weighted = arguments.weight;
  calc_motif_stats (motif, train_set, bg_freqs, arguments.pseudo_p,
                    arguments.diff_scale);
  if (arguments.method == SCORE_BAYES)
    {
        size_t bg_n = gl_list_size (bg_sub.seqs);
        motif->prior = ((double) motif->n) / ((double) bg_n);
    }
  else
    motif->prior = 0.0;
  motif->n_feats = arguments.n_feats;
  if (arguments.n_feats == 0)
    {
      motif->feature_cols = NULL;
      motif->n_bernoulli = 0;
      motif->bernoulli_feats = NULL;
      motif->n_gaussian = 0;
      motif->gaussian_feats = NULL;
      motif->n_beta = 0;
      motif->beta_feats = NULL;
    }
  else
    {
      if (ALLOC_N (motif->feature_cols, motif->n_feats))
        error (EXIT_FAILURE, errno, "Memory exhausted");
      memcpy (motif->feature_cols, arguments.features,
              motif->n_feats * (sizeof (bayes_feat)));
      motif->n_bernoulli = n_bernoulli;
      if (ALLOC_N (motif->bernoulli_feats, n_bernoulli))
        error (EXIT_FAILURE, errno, "Memory exhausted");
      for (size_t i=0; i<n_bernoulli; i++)
        {
          const bool *dat = bernoulli_dat[i];
          motif->bernoulli_feats[i] = bernoulli_param (dat, motif->n,
                                                       arguments.alpha);
        }
      motif->n_gaussian = n_gaussian;
      if (ALLOC_N (motif->gaussian_feats, n_gaussian))
        error (EXIT_FAILURE, errno, "Memory exhausted");
      for (size_t i=0; i<n_gaussian; i++)
        {
          const double *dat = gaussian_dat[i];
          motif->gaussian_feats[i] = gaussian_param (dat, motif->n);
        }
      motif->n_beta = n_beta;
      if (ALLOC_N (motif->beta_feats, n_beta))
        error (EXIT_FAILURE, errno, "Memory exhausted");
      for (size_t i=0; i<n_beta; i++)
        {
          const double *dat = beta_dat[i];
          motif->beta_feats[i] = beta_param (dat, motif->n, arguments.squeeze,
                                             arguments.s);
        }
    }
}


void
setup_col_weights (gsl_vector **col_weight, gsl_vector **compl_col_weight,
                   motifstats motif, motifstats compl_motif,
                   struct arguments arguments)
{
  switch (arguments.scale)
    {
    case SCALE_IC:
      {
        *col_weight = motif.col_ic;
        if (motif.method == SCORE_BAYES)
          *compl_col_weight = motif.col_ic;
        break;
      }
    case SCALE_RELH:
      {
        *col_weight = motif.col_relh;
        if (motif.method == SCORE_BAYES)
          *compl_col_weight = motif.col_relh;
        break;
      }
    case SCALE_GR:
      {
        *col_weight = gsl_vector_alloc (motif.length);
        if (!col_weight)
          error (EXIT_FAILURE, errno, "Memory exhausted");
        calc_col_gr (*col_weight, motif.pfm, compl_motif.pfm, motif.prior,
                     compl_motif.prior, motif.col_relh, compl_motif.col_relh,
                     motif.bg_freqs, motif.ic_base);
        *compl_col_weight = gsl_vector_alloc (motif.length);
        if (!(*compl_col_weight))
          error (EXIT_FAILURE, errno, "Memory exhausted");
        gsl_vector_memcpy (*compl_col_weight, *col_weight);
        break;
      }
    }
}


void
score_sequences (motifstats motif, motifstats compl_motif, seq_set *test_set,
                 bool **bernoulli_dat, double **gaussian_dat, double **beta_dat,
                 struct arguments arguments, size_t fold, bool is_pos)
{
  gsl_vector *col_weight = NULL;
  gsl_vector *compl_col_weight = NULL;
  setup_col_weights (&col_weight, &compl_col_weight, motif, compl_motif, arguments);

  double *bernoulli_likl = NULL, *compl_bernoulli_likl = NULL;
  if (motif.n_bernoulli > 0)
    {
      if (ALLOC_N (bernoulli_likl, motif.n_bernoulli))
        error (EXIT_FAILURE, errno, "Memory exhausted");
      if (ALLOC_N (compl_bernoulli_likl, motif.n_bernoulli))
        error (EXIT_FAILURE, errno, "Memory exhausted");
    }
  double *gaussian_likl = NULL, *compl_gaussian_likl = NULL;
  if (motif.n_gaussian > 0)
    {
      if (ALLOC_N (gaussian_likl, motif.n_gaussian))
        error (EXIT_FAILURE, errno, "Memory exhausted");
      if (ALLOC_N (compl_gaussian_likl, motif.n_gaussian))
        error (EXIT_FAILURE, errno, "Memory exhausted");
    }
  double *beta_likl = NULL, *compl_beta_likl = NULL;
  if (motif.n_beta > 0)
    {
      if (ALLOC_N (beta_likl, motif.n_beta))
        error (EXIT_FAILURE, errno, "Memory exhausted");
      if (ALLOC_N (compl_beta_likl, motif.n_beta))
        error (EXIT_FAILURE, errno, "Memory exhausted");
    }

  char *label;
  if (is_pos)
    label = "TRUE";
  else
    label = "FALSE";

  gl_list_iterator_t i = gl_list_iterator (test_set->seqs);
  const void *eltp;
  size_t si = 0;
  while (gl_list_iterator_next (&i, &eltp, NULL))
    {
      double likl, compl_likl;
      seq_win *win = (seq_win *) eltp;
      double score = score_sequence (win->seq, motif.pssm, col_weight, motif.type,
                                     motif.method, false, arguments.omit,
                                     arguments.n_omit);
      if (motif.method == SCORE_BAYES)
        {
          likl = score;
          compl_likl = score_sequence (win->seq, compl_motif.pssm,
                                       compl_col_weight, compl_motif.type,
                                       compl_motif.method, false,
                                       arguments.omit, arguments.n_omit);
          for (size_t j=0; j<motif.n_bernoulli; j++)
            {
              bernoulli_likl[j] = score_bernoulli (bernoulli_dat[j][si],
                                                   motif.bernoulli_feats[j]);
              compl_bernoulli_likl[j] = score_bernoulli (bernoulli_dat[j][si],
                                                         compl_motif.bernoulli_feats[j]);
            }
          for (size_t j=0; j<motif.n_gaussian; j++)
            {
              gaussian_likl[j] = score_gaussian (gaussian_dat[j][si],
                                                 motif.gaussian_feats[j]);
              compl_gaussian_likl[j] = score_gaussian (gaussian_dat[j][si],
                                                       compl_motif.gaussian_feats[j]);
            }
          for (size_t j=0; j<motif.n_beta; j++)
            {
              beta_likl[j] = score_beta (beta_dat[j][si],
                                                 motif.beta_feats[j]);
              compl_beta_likl[j] = score_beta (beta_dat[j][si],
                                                       compl_motif.beta_feats[j]);
            }
          score = score_naive_bayes (likl, compl_likl, bernoulli_likl,
                                     compl_bernoulli_likl, motif.n_bernoulli,
                                     gaussian_likl, compl_gaussian_likl,
                                     motif.n_gaussian, beta_likl, compl_beta_likl,
                                     motif.n_beta, motif.prior, compl_motif.prior);
        }
      printf ("%s\t%zu\t%s\t%g\n", win->seq, fold + 1, label, score);
      si++;
    }
  gl_list_iterator_free (&i);
  if (bernoulli_likl)
    FREE (bernoulli_likl);
  if (compl_bernoulli_likl)
    FREE (compl_bernoulli_likl);
  if (gaussian_likl)
    FREE (gaussian_likl);
  if (compl_gaussian_likl)
    FREE (compl_gaussian_likl);
  if (beta_likl)
    FREE (beta_likl);
  if (compl_beta_likl)
    FREE (compl_beta_likl);
}


void
k_fold_cv (seq_set *pos_wins, seq_set *neg_wins, bool **pos_bernoulli_dat,
           double **pos_gaussian_dat, double **pos_beta_dat,
           size_t n_bernoulli, size_t n_gaussian, size_t n_beta,
           size_t pos_num_recs, bool **neg_bernoulli_dat,
           double **neg_gaussian_dat, double **neg_beta_dat,
           size_t neg_num_recs, seq_set *bg_seqs, bool **bg_bernoulli_dat,
           double **bg_gaussian_dat, double **bg_beta_dat, size_t bg_num_recs,
           gsl_matrix *glob_bg_freqs, struct arguments arguments)
{
  size_t pos_n = gl_list_size (pos_wins->seqs);
  size_t pos_fold = pos_n / arguments.k;
  size_t neg_n = gl_list_size (neg_wins->seqs);
  size_t neg_fold = neg_n / arguments.k;

  puts ("seq.win\tfold\tlabel\tscore");

  for (size_t i=0; i<arguments.k; i++)
    {
      /* First, define the indices that correspond to this fold.  Do
         not assume that the positive set and the negative set are the
         same size. */
      size_t pos_start = i * pos_fold;
      size_t pos_end = i * pos_fold + pos_fold;
      /* Either expand a just-slightly-too-small fold to encompass
         the rest of the data or shrink a just-slightly-too-large
         fold appropriately */
      if (pos_n - pos_end < 0.25 * pos_fold ||
          pos_end > pos_n)
        {
          if (i < arguments.k - 1)
            error (EXIT_FAILURE, 0, "Something went wrong defining the positive folds.");
          pos_end = pos_n;
        }
      size_t neg_start = i * neg_fold;
      size_t neg_end = i * neg_fold + neg_fold;
      if (neg_n - neg_end < 0.25 * neg_fold ||
          neg_end > neg_n)
        {
          if (i < arguments.k - 1)
            error (EXIT_FAILURE, 0, "Something went wrong defining the negative folds.");
          neg_end = neg_n;
        }
      /* Get the training set, which is everything outside of this
         fold.  For conventional PSSMs, there is no training on
         positive & negative cases, instead only positive cases are
         used, as reflected here.  For Naive Bayes scoring, where
         negative cases are considered, the negative set is formed
         below when "bg_sub" (removing the test cases) and
         "compl_seqs" (removing the positive cases) are created.
         Thus, in the case of Naive Bayes, we use the whole background
         as a negative set (partitioned over the folds), regardless of
         the sequences passed in as negative cases (which are only used for
         testing).  */
      seq_set train_set;
      train_set.seqs = gl_list_nx_create_empty (GL_LINKEDHASH_LIST, seq_wins_equal,
                                                seq_win_hash, seq_win_dispose, false);
      if (!train_set.seqs)
        error (EXIT_FAILURE, errno, "Memory exhausted");
      train_set.type = pos_wins->type;
      train_set.win_len = pos_wins->win_len;
      bool **train_bernoulli_dat = NULL;
      double **train_gaussian_dat = NULL;
      double **train_beta_dat = NULL;
      get_train_set (&train_set, pos_wins, &train_bernoulli_dat, pos_bernoulli_dat,
                     &train_gaussian_dat, pos_gaussian_dat, &train_beta_dat,
                     pos_beta_dat, n_bernoulli, n_gaussian, n_beta,
                     arguments.features, arguments.n_feats, pos_start, pos_end);

      /* Get the positive test cases within this fold. */
      seq_set pos_test_set;
      pos_test_set.seqs = gl_list_nx_create_empty (GL_LINKEDHASH_LIST, seq_wins_equal,
                                                   seq_win_hash, seq_win_dispose, false);
      if (!pos_test_set.seqs)
        error (EXIT_FAILURE, errno, "Memory exhausted");
      pos_test_set.type = pos_wins->type;
      pos_test_set.win_len = pos_wins->win_len;
      bool **pos_test_bernoulli_dat = NULL;
      double **pos_test_gaussian_dat = NULL;
      double **pos_test_beta_dat = NULL;
      get_test_set (&pos_test_set, pos_wins, &pos_test_bernoulli_dat, pos_bernoulli_dat,
                    &pos_test_gaussian_dat, pos_gaussian_dat, &pos_test_beta_dat,
                    pos_beta_dat, n_bernoulli, n_gaussian, n_beta,
                    arguments.features, arguments.n_feats, pos_start, pos_end);
      /* Get the negative test cases within this fold. */
      seq_set neg_test_set;
      neg_test_set.seqs = gl_list_nx_create_empty (GL_LINKEDHASH_LIST, seq_wins_equal,
                                                   seq_win_hash, seq_win_dispose, false);
      if (!neg_test_set.seqs)
        error (EXIT_FAILURE, errno, "Memory exhausted");
      neg_test_set.type = neg_wins->type;
      neg_test_set.win_len = neg_wins->win_len;
      bool **neg_test_bernoulli_dat = NULL;
      double **neg_test_gaussian_dat = NULL;
      double **neg_test_beta_dat = NULL;
      get_test_set (&neg_test_set, neg_wins, &neg_test_bernoulli_dat, neg_bernoulli_dat,
                    &neg_test_gaussian_dat, neg_gaussian_dat, &neg_test_beta_dat,
                    neg_beta_dat, n_bernoulli, n_gaussian, n_beta,
                    arguments.features, arguments.n_feats, neg_start, neg_end);

      /* Calculate background frequencies after removing any test
         cases from it. */
      seq_set bg_sub;
      motifstats bg_motif;
      gsl_matrix *bg_freqs;
      if (arguments.bg == BG_POS)
        {
          setup_bg_freqs (&bg_freqs, &bg_motif, *bg_seqs, &bg_sub, pos_test_set,
                          neg_test_set, bg_bernoulli_dat, bg_gaussian_dat,
                          bg_beta_dat, bg_num_recs, n_bernoulli, n_gaussian,
                          n_beta, arguments);
        }
      else if (arguments.bg == BG_GLOBAL)
        bg_freqs = glob_bg_freqs;

      /* Calculate the motif statistics for this training set. */
      motifstats motif;
      setup_motif (&motif, &train_set, bg_sub, bg_freqs, train_bernoulli_dat,
                   train_gaussian_dat, train_beta_dat, n_bernoulli, n_gaussian,
                   n_beta, arguments);

      /* Possibly calculate the complement motif for Naive Bayes scoring */
      motifstats compl_motif;
      if (arguments.method == SCORE_BAYES)
        {
          /* Get a union of all of the sequences used in testing and
             training.  This will be used when reading the background in
             order to get extra Naive Bayes features for the complementary
             motif. */
          seq_set test_train_union_tmp, test_train_union;
          seq_set_union (train_set, pos_test_set, &test_train_union_tmp);
          seq_set_union (neg_test_set, test_train_union_tmp, &test_train_union);
          clear_seq_win_set (&test_train_union_tmp);

          /* Get the negative training instances */
          seq_set compl_seqs;
          seq_set_diff (bg_sub, train_set, &compl_seqs);
          bool **compl_bernoulli;
          double **compl_gaussian;
          double **compl_beta;
          calc_compl_feats (test_train_union, *bg_seqs, &compl_bernoulli,
                            &compl_gaussian, &compl_beta, bg_bernoulli_dat,
                            bg_gaussian_dat, bg_beta_dat, n_bernoulli,
                            n_gaussian, n_beta, bg_num_recs);
          setup_motif (&compl_motif, &compl_seqs, bg_sub, bg_freqs, compl_bernoulli,
                       compl_gaussian, compl_beta, n_bernoulli, n_gaussian, n_beta,
                       arguments);
          clear_seq_win_set (&compl_seqs);
          clear_seq_win_set (&test_train_union);
          for (size_t i=0; i<n_bernoulli; i++)
            FREE (compl_bernoulli[i]);
          FREE (compl_bernoulli);
          for (size_t i=0; i<n_gaussian; i++)
            FREE (compl_gaussian[i]);
          FREE (compl_gaussian);
          for (size_t i=0; i<n_beta; i++)
            FREE (compl_beta[i]);
          FREE (compl_beta);
        }
      clear_seq_win_set (&train_set);
      if (bg_freqs && arguments.bg != BG_GLOBAL)
        gsl_matrix_free (bg_freqs);

      /* Score the test data */
      score_sequences (motif, compl_motif, &pos_test_set, pos_test_bernoulli_dat,
                       pos_test_gaussian_dat, pos_test_beta_dat, arguments, i, true);
      score_sequences (motif, compl_motif, &neg_test_set, neg_test_bernoulli_dat,
                       neg_test_gaussian_dat, neg_test_beta_dat, arguments, i, false);

      /* Clean up */
      clear_seq_win_set (&pos_test_set);
      clear_seq_win_set (&neg_test_set);
      if (arguments.bg == BG_POS)
        {
          clear_seq_win_set (&bg_sub);
          free_motif_stats (bg_motif, false);
        }
      free_motif_stats (motif, false);
      if (arguments.method == SCORE_BAYES)
        free_motif_stats (compl_motif, false);
      if (n_bernoulli > 0)
        {
          for (size_t j=0; j<n_bernoulli; j++)
            {
              FREE (train_bernoulli_dat[j]);
              FREE (pos_test_bernoulli_dat[j]);
              FREE (neg_test_bernoulli_dat[j]);
            }
          FREE (train_bernoulli_dat);
          FREE (pos_test_bernoulli_dat);
          FREE (neg_test_bernoulli_dat);
        }
      if (n_gaussian > 0)
        {
          for (size_t j=0; j<n_gaussian; j++)
            {
              FREE (train_gaussian_dat[j]);
              FREE (pos_test_gaussian_dat[j]);
              FREE (neg_test_gaussian_dat[j]);
            }
          FREE (train_gaussian_dat);
          FREE (pos_test_gaussian_dat);
          FREE (neg_test_gaussian_dat);
        }
      if (n_beta > 0)
        {
          for (size_t j=0; j<n_beta; j++)
            {
              FREE (train_beta_dat[j]);
              FREE (pos_test_beta_dat[j]);
              FREE (neg_test_beta_dat[j]);
            }
          FREE (train_beta_dat);
          FREE (pos_test_beta_dat);
          FREE (neg_test_beta_dat);
        }
    }
}


int
main (int argc, char **argv)
{
  struct arguments arguments;
  init_args (&arguments);
  argp_parse (&argp, argc, argv, 0, 0, &arguments);

  if (arguments.bg != BG_NONE)
    arguments.pseudo_dist = PC_DIST_FREQ;

  if (!arguments.args[1])
    error (EXIT_FAILURE, 0, "Two sequence window files must be provided.");

  if (arguments.method == SCORE_BAYES && arguments.bg != BG_POS)
    {
        error (EXIT_FAILURE, 0, "Bayes scoring requires a position-specific "
               "background.");
    }

  seq_set pos_wins;
  bool **pos_bernoulli_dat = NULL;
  double **pos_gaussian_dat = NULL;
  double **pos_beta_dat = NULL;
  size_t n_bernoulli = 0;
  size_t n_gaussian = 0;
  size_t n_beta = 0;
  size_t num_recs = 0;
  size_t win_len;
  if (arguments.method == SCORE_BAYES)
    {
      num_recs = read_features (&pos_wins, &pos_bernoulli_dat, &pos_gaussian_dat, &pos_beta_dat,
                                arguments.args[0], arguments.features, arguments.n_feats,
                                &n_bernoulli, &n_gaussian, &n_beta, SEQ_GUESS, arguments.feat_delim,
                                NULL);
      win_len = pos_wins.win_len;
    }
  else
    {
      win_len = read_seq_wins (&pos_wins, arguments.args[0], SEQ_GUESS);
    }
  if (pos_wins.type == SEQ_GUESS)
    error (EXIT_FAILURE, 0, "Somehow failed to guess the sequence type in the "
           "sequence windows file");

  seq_set neg_wins;
  bool **neg_bernoulli_dat = NULL;
  double **neg_gaussian_dat = NULL;
  double **neg_beta_dat = NULL;
  size_t neg_n_bernoulli = 0;
  size_t neg_n_gaussian = 0;
  size_t neg_n_beta = 0;
  size_t neg_num_recs = 0;
  size_t neg_win_len;
  if (arguments.method == SCORE_BAYES)
    {
      neg_num_recs = read_features (&neg_wins, &neg_bernoulli_dat, &neg_gaussian_dat,
                                    &neg_beta_dat, arguments.args[1], arguments.features,
                                    arguments.n_feats, &neg_n_bernoulli, &neg_n_gaussian,
                                    &neg_n_beta, SEQ_GUESS, arguments.feat_delim,
                                    NULL);
      neg_win_len = neg_wins.win_len;
    }
  else
    {
      neg_win_len = read_seq_wins (&neg_wins, arguments.args[1], SEQ_GUESS);
    }
  if (neg_wins.type == SEQ_GUESS)
    error (EXIT_FAILURE, 0, "Somehow failed to guess the sequence type in the "
           "sequence windows file");
  if (neg_win_len != win_len)
    error (EXIT_FAILURE, 0, "Positive and negative sequence window lengths must "
           "be the same");

  gsl_matrix *glob_bg_freqs = NULL;
  seq_set bg_seqs;
  bool **bg_bernoulli_dat = NULL;
  double **bg_gaussian_dat = NULL;
  double **bg_beta_dat = NULL;
  size_t bg_n_bernoulli = 0;
  size_t bg_n_gaussian = 0;
  size_t bg_n_beta = 0;
  size_t bg_num_recs = 0;
  size_t bg_win_len;
  if (arguments.bg == BG_POS)
    {
      if (arguments.method == SCORE_BAYES)
        {
          bg_num_recs = read_features (&bg_seqs, &bg_bernoulli_dat, &bg_gaussian_dat,
                                       &bg_beta_dat, arguments.bg_file, arguments.features,
                                       arguments.n_feats, &bg_n_bernoulli,
                                       &bg_n_gaussian, &bg_n_beta, pos_wins.type,
                                       arguments.feat_delim, NULL);
          bg_win_len = bg_seqs.win_len;
        }
      else
        {
          bg_win_len = read_seq_wins (&bg_seqs, arguments.bg_file,
                                      pos_wins.type);
        }
      if (bg_win_len != pos_wins.win_len)
        {
          error (EXIT_FAILURE, 0, "Background sequence windows are of length "
                 "%zd, but length %zd was expected", bg_win_len, pos_wins.win_len);
        }
    }
  else if (arguments.bg == BG_GLOBAL)
    {
        read_sequences (&bg_seqs, arguments.bg_file, SEQ_GUESS);
        glob_bg_freqs = calc_global_bg (bg_seqs);
    }

  k_fold_cv (&pos_wins, &neg_wins, pos_bernoulli_dat, pos_gaussian_dat,
             pos_beta_dat, n_bernoulli, n_gaussian, n_beta, num_recs,
             neg_bernoulli_dat, neg_gaussian_dat, neg_beta_dat,
             neg_num_recs, &bg_seqs, bg_bernoulli_dat, bg_gaussian_dat,
             bg_beta_dat, bg_num_recs, glob_bg_freqs, arguments);

  clear_seq_win_set (&pos_wins);
  clear_seq_win_set (&neg_wins);
  clear_seq_win_set (&bg_seqs);

  if (glob_bg_freqs)
    gsl_matrix_free (glob_bg_freqs);
  if (pos_bernoulli_dat)
    {
      for (size_t i=0; i<n_bernoulli; i++)
        FREE (pos_bernoulli_dat[i]);
      FREE (pos_bernoulli_dat);
    }
  if (pos_gaussian_dat)
    {
      for (size_t i=0; i<n_gaussian; i++)
        FREE (pos_gaussian_dat[i]);
      FREE (pos_gaussian_dat);
    }
  if (pos_beta_dat)
    {
      for (size_t i=0; i<n_beta; i++)
        FREE (pos_beta_dat[i]);
      FREE (pos_beta_dat);
    }
  if (neg_bernoulli_dat)
    {
      for (size_t i=0; i<n_bernoulli; i++)
        FREE (neg_bernoulli_dat[i]);
      FREE (neg_bernoulli_dat);
    }
  if (neg_gaussian_dat)
    {
      for (size_t i=0; i<n_gaussian; i++)
        FREE (neg_gaussian_dat[i]);
      FREE (neg_gaussian_dat);
    }
  if (neg_beta_dat)
    {
      for (size_t i=0; i<n_beta; i++)
        FREE (neg_beta_dat[i]);
      FREE (neg_beta_dat);
    }
  if (bg_bernoulli_dat)
    {
      for (size_t i=0; i<n_bernoulli; i++)
        FREE (bg_bernoulli_dat[i]);
      FREE (bg_bernoulli_dat);
    }
  if (bg_gaussian_dat)
    {
      for (size_t i=0; i<n_gaussian; i++)
        FREE (bg_gaussian_dat[i]);
      FREE (bg_gaussian_dat);
    }
  if (bg_beta_dat)
    {
      for (size_t i=0; i<n_beta; i++)
        FREE (bg_beta_dat[i]);
      FREE (bg_beta_dat);
    }
  if (arguments.omit)
    FREE (arguments.omit);
  if (arguments.features)
    FREE (arguments.features);

  return EXIT_SUCCESS;
}
