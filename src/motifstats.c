/* Copyright (C) 2019, 2020, 2021 Brandon Invergo <b.invergo@exeter.ac.uk> */

/* This program is free software: you can redistribute it and/or modify */
/* it under the terms of the GNU General Public License as published by */
/* the Free Software Foundation, either version 3 of the License, or */
/* (at your option) any later version. */

/* This program is distributed in the hope that it will be useful, */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the */
/* GNU General Public License for more details. */

/* You should have received a copy of the GNU General Public License */
/* along with this program.  If not, see <http://www.gnu.org/licenses/>. */


#include "motifstats.h"


void
free_motif_stats (motifstats motif, bool free_name)
{
  if (motif.pfm)
    gsl_matrix_free (motif.pfm);
  if (motif.pssm)
    gsl_matrix_free (motif.pssm);
  if (motif.bg_freqs)
    gsl_matrix_free (motif.bg_freqs);
  if (motif.col_ic)
    gsl_vector_free (motif.col_ic);
  if (motif.col_relh)
    gsl_vector_free (motif.col_relh);
  if (motif.n_feats > 0)
    {
      if (motif.bernoulli_feats)
        FREE (motif.bernoulli_feats);
      if (motif.gaussian_feats)
        FREE (motif.gaussian_feats);
      if (motif.beta_feats)
        FREE (motif.beta_feats);
      if (motif.feature_cols)
        FREE (motif.feature_cols);
    }
  if (free_name && motif.name)
    FREE (motif.name);
}


/* Master function for calculating PSSMs */
void
calc_motif_stats (motifstats *motif, const seq_set *seq_wins, const gsl_matrix *bg_freqs,
                  double pseudo_p, double diff_scale)
{
  size_t nrow;
  if (motif->type == SEQ_AA)
    nrow = NUM_AA;
  else
    nrow = NUM_NUC;

  /* Setup background frequencies */
  motif->bg_freqs = gsl_matrix_calloc (nrow, seq_wins->win_len);
  if (!motif->bg_freqs)
    error (EXIT_FAILURE, errno, "Memory exhausted");
  if (bg_freqs)
    {
      if (bg_freqs->size1 == nrow && bg_freqs->size2 == seq_wins->win_len)
        gsl_matrix_memcpy (motif->bg_freqs, bg_freqs);
      else
        {
          gsl_vector_const_view bg_freqs_v = gsl_matrix_const_column (bg_freqs, 0);
          for (size_t j=0; j<motif->bg_freqs->size2; j++)
            gsl_matrix_set_col (motif->bg_freqs, j, &bg_freqs_v.vector);
        }
    }
  else
    {
      gsl_matrix_set_all (motif->bg_freqs, 1.0);
      gsl_matrix_scale (motif->bg_freqs, 1.0 / (double) nrow);
    }

  /* Count residues at each position */
  motif->pfm = gsl_matrix_calloc (nrow, seq_wins->win_len);
  if (!motif->pfm)
    error (EXIT_FAILURE, errno, "Memory exhausted");
  gsl_vector *total_emp_counts = gsl_vector_calloc (seq_wins->win_len);
  if (!total_emp_counts)
    error (EXIT_FAILURE, errno, "Memory exhausted");
  gsl_vector *total_pos_counts = gsl_vector_calloc (seq_wins->win_len);
  if (!total_pos_counts)
    error (EXIT_FAILURE, errno, "Memory exhausted");
  count_pos_res (seq_wins, total_emp_counts, total_pos_counts, motif->pfm,
                 motif->bg_freqs, motif->weighted);

  /* Calculate the number of pseudocounts */
  gsl_vector *num_pos_pc;
  num_pos_pc = gsl_vector_calloc (seq_wins->win_len);
  if (motif->pc_n == PC_N_POS)
    calc_num_pos_pc (total_emp_counts, num_pos_pc, pseudo_p);
  gsl_vector_free (total_emp_counts);

  /* Calculate the position frequency matrix */
  calc_pos_freqs (motif->pfm, total_pos_counts, motif->bg_freqs, num_pos_pc,
                  pseudo_p, motif->bg, motif->pc_n,
                  motif->pc_dist);
  gsl_vector_free (total_pos_counts);
  gsl_vector_free (num_pos_pc);

  /* Calculate the position-specific information content */
  motif->col_ic = gsl_vector_calloc (seq_wins->win_len);
  if (!motif->col_ic)
    error (EXIT_FAILURE, errno, "Memory exhausted");
  calc_col_ic (motif->pfm, motif->col_ic, motif->ic_base);

  /* Calculate the scoring matrix and, possibly, position-specific
     relative entropy */
  motif->pssm = gsl_matrix_calloc (nrow, seq_wins->win_len);
  if (!motif->pssm)
    error (EXIT_FAILURE, errno, "Memory exhausted");
  motif->col_relh = gsl_vector_calloc (seq_wins->win_len);
  if (!motif->col_relh)
    error (EXIT_FAILURE, errno, "Memory exhausted");
  /* Calculate the log-fold change versus the background and use this
     to calculate relative entropy.  In the default case, this
     log-fold change becomes the PSSM... */
  calc_fc_score (motif->pssm, motif->pfm, motif->bg_freqs, motif->bg,
                 motif->ic_base);
  calc_col_relh (motif->pssm, motif->pfm, motif->col_relh);
  /* ...but then for the other scoring methods we replace the PSSM
     with the appropriate matrix now that relH has been calculated. */
  switch (motif->method)
    {
    case SCORE_DIFF:
      gsl_matrix_set_zero (motif->pssm);
      calc_diff_score (motif->pssm, motif->pfm, motif->bg_freqs, motif->bg,
                       diff_scale);
      break;
    case SCORE_FREQ:
    case SCORE_BAYES:
      gsl_matrix_set_zero (motif->pssm);
      calc_freq_score (motif->pssm, motif->pfm, motif->ic_base);
      break;
    case SCORE_LOGLIK:
    default:
      break;
    }
}


void
hdf5_annotate_root (hid_t file_id, motifstats motif, char *colophon)
{
  herr_t status;
  status = H5LTset_attribute_string (file_id, "/", "name", motif.name);
  if (status < 0)
    error (EXIT_FAILURE, 0, "Could not set name in motifstats file");
  if (motif.type == SEQ_AA)
    {
      status = H5LTset_attribute_string (file_id, "/", "type", "aminoacid");
      if (status < 0)
        error (EXIT_FAILURE, 0, "Could not set type in motifstats file");
      status = H5LTset_attribute_char (file_id, "/", "rownames", aa2char, NUM_AA);
      if (status < 0)
        error (EXIT_FAILURE, 0, "Could not add rownames in motifstats file");
    }
  else
    {
      status = H5LTset_attribute_string (file_id, "/", "type", "nucleotide");
      status = H5LTset_attribute_char (file_id, "/", "rownames", nuc2char, NUM_NUC);
      if (status < 0)
        error (EXIT_FAILURE, 0, "Could not add rownames in motifstats file");
    }
  status = H5LTset_attribute_ulong (file_id, "/", "length",
                                   (unsigned long int *) &motif.length, 1);
  if (status < 0)
    error (EXIT_FAILURE, 0, "Could not set length in motifstats file");
  status = H5LTset_attribute_ulong (file_id, "/", "number of sequences",
                                   (unsigned long int *) &motif.n, 1);
  if (status < 0)
    error (EXIT_FAILURE, 0, "Could not set n in motifstats file");
  status = H5LTset_attribute_uint (file_id, "/", "weighted",
                                   (unsigned int *) &motif.weighted, 1);
  if (status < 0)
    error (EXIT_FAILURE, 0, "Could not set weighted in motifstats file");
  status = H5LTset_attribute_double (file_id, "/", "prior", &motif.prior, 1);
  if (status < 0)
    error (EXIT_FAILURE, 0, "Could not set prior in motifstats file");
  status = H5LTset_attribute_ulong (file_id, "/", "number of features",
                                   (unsigned long int *) &motif.n_feats, 1);
  if (status < 0)
    error (EXIT_FAILURE, 0, "Could not set number of features in motifstats file");
}


void
hdf5_add_pfm (hid_t file_id, motifstats motif)
{
  hsize_t dims[2] = {motif.pfm->size1, motif.pfm->size2}; 
  herr_t status = H5LTmake_dataset_double (file_id, "/PFM", 2, dims,
                                           motif.pfm->data);
  if (status < 0)
    error (EXIT_FAILURE, 0, "Could not add PFM to motifstats file");
  switch (motif.pc_n)
    {
    case PC_N_CONST:
      status = H5LTset_attribute_string (file_id, "/PFM", "pseudocount number",
                                         "constant");
      break;
    case PC_N_POS:
      status = H5LTset_attribute_string (file_id, "/PFM", "pseudocount number",
                                         "positional");
      break;
    case PC_N_NONE:
      status = H5LTset_attribute_string (file_id, "/PFM", "pseudocount number",
                                         "none");
      break;
    }
  if (status < 0)
    error (EXIT_FAILURE, 0, "Could not set PFM pseudocount number in motifstats file");
  switch (motif.pc_dist)
    {
    case PC_DIST_EQUAL:
      status = H5LTset_attribute_string (file_id, "/PFM", "pseudocount distribution",
                                         "equal");
      break;
    case PC_DIST_FREQ:
      status = H5LTset_attribute_string (file_id, "/PFM", "pseudocount distribution",
                                         "background frequency");
      break;
    }
  if (status < 0)
    error (EXIT_FAILURE, 0, "Could not set PFM pseudocount distribution in motifstats file");
}


void
hdf5_add_bg (hid_t file_id, motifstats motif)
{
  hsize_t dims[2] = {motif.bg_freqs->size1, motif.bg_freqs->size2}; 
  herr_t status = H5LTmake_dataset_double (file_id, "/background", 2, dims,
                                           motif.bg_freqs->data);
  if (status < 0)
    error (EXIT_FAILURE, 0, "Could not add background to motifstats file");
  switch (motif.bg)
    {
    case BG_GLOBAL:
      status = H5LTset_attribute_string (file_id, "/background", "type",
                                         "global");
      break;
    case BG_POS:
      status = H5LTset_attribute_string (file_id, "/background", "type",
                                         "positional");
      break;
    case BG_NONE:
      status = H5LTset_attribute_string (file_id, "/background", "type",
                                         "uniform");
      break;
    }
  if (status < 0)
    error (EXIT_FAILURE, 0, "Could not set background type in motifstats file");
}


void
hdf5_add_pssm (hid_t file_id, motifstats motif)
{
  hsize_t dims[2] = {motif.pssm->size1, motif.pssm->size2}; 
  herr_t status = H5LTmake_dataset_double (file_id, "/PSSM", 2, dims,
                                           motif.pssm->data);
  if (status < 0)
    error (EXIT_FAILURE, 0, "Could not add PSSM to motifstats file");
  switch (motif.method)
    {
    case SCORE_FREQ:
      status = H5LTset_attribute_string (file_id, "/PSSM", "method", "frequency");
      break;
    case SCORE_LOGLIK:
      status = H5LTset_attribute_string (file_id, "/PSSM", "method", "log-likelihood");
      break;
    case SCORE_DIFF:
      status = H5LTset_attribute_string (file_id, "/PSSM", "method", "difference");
      break;
    case SCORE_BAYES:
      status = H5LTset_attribute_string (file_id, "/PSSM", "method", "bayes");
      break;
    }
  if (status < 0)
    error (EXIT_FAILURE, 0, "Could not set PSSM scoring method in motifstats file");
}


void
hdf5_add_ic (hid_t file_id, motifstats motif)
{
  herr_t status = H5LTmake_dataset_double (file_id, "/IC", 1,
                                           (hsize_t *) &motif.col_ic->size,
                                           motif.col_ic->data);
  if (status < 0)
    error (EXIT_FAILURE, 0, "Could not add column IC to motifstats file");
  status = H5LTset_attribute_double (file_id, "/IC", "logarithm base", 
                                     &motif.ic_base, 1);
  if (status < 0)
    error (EXIT_FAILURE, 0, "Could not set IC base in motifstats file");
}


void
hdf5_add_relh (hid_t file_id, motifstats motif)
{
  herr_t status = H5LTmake_dataset_double (file_id, "/relH", 1,
                                           (hsize_t *) &motif.col_relh->size,
                                           motif.col_relh->data);
  if (status < 0)
    error (EXIT_FAILURE, 0, "Could not add column relative entropy to motifstats file");
  status = H5LTset_attribute_double (file_id, "/relH", "logarithm base", 
                                     &motif.ic_base, 1);
  if (status < 0)
    error (EXIT_FAILURE, 0, "Could not set relative entropy base in motifstats file");
}


void
hdf5_add_feature_cols (hid_t file_id, motifstats motif)
{
  int *data;
  if (ALLOC_N (data, motif.n_feats))
    error (EXIT_FAILURE, errno, "Memory exhausted");
  /* This is verbose but I want to avoid making assumptions about
     specific compiler behavior around enums */
  for (size_t i=0; i<motif.n_feats; i++)
    {
      switch (motif.feature_cols[i])
        {
        case FEATURE_BERNOULLI:
          data[i] = 0;
          break;
        case FEATURE_GAUSSIAN:
          data[i] = 1;
          break;
        case FEATURE_BETA:
          data[i] = 2;
          break;
        }
    }
  herr_t status = H5LTmake_dataset_int (file_id, "/features/columns", 1,
                                        (hsize_t *) &motif.n_feats, data);
  if (status < 0)
    error (EXIT_FAILURE, 0, "Could not add feature columns to motifstats file");
  FREE (data);
}


void
hdf5_add_bernoulli (hid_t file_id, motifstats motif)
{
  herr_t status = H5LTmake_dataset_double (file_id, "/features/bernoulli", 1,
                                           (hsize_t *) &motif.n_bernoulli,
                                           motif.bernoulli_feats);
  if (status < 0)
    error (EXIT_FAILURE, 0, "Could not add Bernoulli features to motifstats file");
  status = H5LTset_attribute_ulong (file_id, "/features/bernoulli", "n",
                                   (unsigned long int *) &motif.n_bernoulli, 1);
  if (status < 0)
    error (EXIT_FAILURE, 0, "Could not set # Bernoulli feats in motifstats file");
}


void
hdf5_add_gaussian (hid_t file_id, motifstats motif)
{
  /* Move the Gaussian distribution stats from the gaussian_dist
     struct to a n-by-2 matrix, with means in the first column and
     standard deviations in the second column. */
  hsize_t dims[2] = {motif.n_gaussian, 2};
  double *distr_stats;
  if (ALLOC_N (distr_stats, motif.n_gaussian * 2))
    error (EXIT_FAILURE, errno, "Memory exhausted");
  size_t j = 0;
  for (size_t i=0; i<motif.n_gaussian; i++)
    {
      distr_stats[j++] = motif.gaussian_feats[i].mean;
      distr_stats[j++] = motif.gaussian_feats[i].sd;
    }
  herr_t status = H5LTmake_dataset_double (file_id, "/features/gaussian", 2, dims,
                                           distr_stats);
  if (status < 0)
    error (EXIT_FAILURE, 0, "Could not add Gaussian features to motifstats file");
  FREE (distr_stats);
  status = H5LTset_attribute_ulong (file_id, "/features/gaussian", "n",
                                   (unsigned long int *) &motif.n_gaussian, 1);
  if (status < 0)
    error (EXIT_FAILURE, 0, "Could not set # Gaussian feats in motifstats file");
}


void
hdf5_add_beta (hid_t file_id, motifstats motif)
{
  /* Move the Beta distribution stats from the beta_dist struct to a
     n-by-2 matrix, with parameter 'a' in the first column and 'b' in
     the second column. */
  hsize_t dims[2] = {motif.n_beta, 2};
  double *distr_stats;
  if (ALLOC_N (distr_stats, motif.n_beta * 2))
    error (EXIT_FAILURE, errno, "Memory exhausted");
  size_t j = 0;
  for (size_t i=0; i<motif.n_beta; i++)
    {
      distr_stats[j++] = motif.beta_feats[i].a;
      distr_stats[j++] = motif.beta_feats[i].b;
    }
  herr_t status = H5LTmake_dataset_double (file_id, "/features/beta", 2, dims,
                                           distr_stats);
  if (status < 0)
    error (EXIT_FAILURE, 0, "Could not add Beta features to motifstats file");
  FREE (distr_stats);
  status = H5LTset_attribute_ulong (file_id, "/features/beta", "n",
                                   (unsigned long int *) &motif.n_beta, 1);
  if (status < 0)
    error (EXIT_FAILURE, 0, "Could not set # Beta feats in motifstats file");
}


void
hdf5_add_features (hid_t file_id, motifstats motif)
{
  hid_t group_id = H5Gcreate (file_id, "/features", H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
  hdf5_add_feature_cols (file_id, motif);
  hdf5_add_bernoulli (file_id, motif);
  hdf5_add_gaussian (file_id, motif);
  hdf5_add_beta (file_id, motif);
  herr_t status = H5Gclose (group_id);
  if (status < 0)
    error (EXIT_FAILURE, 0, "Error closing the features group of the motifstats file");
}


/* Write a PSSM to a file stream in HDF5 format. */
void
write_motif_stats (hid_t file_id, motifstats motif, char *colophon)
{
  hdf5_annotate_root (file_id, motif, colophon);
  hdf5_add_pfm (file_id, motif);
  hdf5_add_bg (file_id, motif);
  hdf5_add_pssm (file_id, motif);
  hdf5_add_ic (file_id, motif);
  if (motif.col_relh)
    hdf5_add_relh (file_id, motif);
  if (motif.n_feats > 0)
    hdf5_add_features (file_id, motif);
}


void
hdf5_read_root (hid_t file_id, motifstats *motif)
{
  herr_t status;
  char buf[80] = {'\0'};
  /* Read the motif name */
  status = H5LTget_attribute_string (file_id, "/", "name", buf);
  if (status < 0)
    error (EXIT_FAILURE, 0, "Error reading motif name");
  motif->name = strdup (buf);
  /* Read the motif type */
  status = H5LTget_attribute_string (file_id, "/", "type", buf);
  if (status < 0)
    error (EXIT_FAILURE, 0, "Error reading motif type");
  if (!strcmp (buf, "aminoacid"))
    motif->type = SEQ_AA;
  else
    motif->type = SEQ_NUC;
  unsigned long int n;
  status = H5LTget_attribute_ulong (file_id, "/", "number of sequences", 
                                   &n);
  if (status < 0)
    error (EXIT_FAILURE, 0, "Error reading motif n");
  motif->n = (size_t) n;
  unsigned long int length;
  status = H5LTget_attribute_ulong (file_id, "/", "length", 
                                   &length);
  if (status < 0)
    error (EXIT_FAILURE, 0, "Error reading motif length");
  motif->length = (size_t) length;
  unsigned int weighted;
  status = H5LTget_attribute_uint (file_id, "/", "weighted", 
                                   &weighted);
  if (status < 0)
    error (EXIT_FAILURE, 0, "Error reading weighted");
  motif->weighted = (bool) weighted;
  double prior;
  status = H5LTget_attribute_double (file_id, "/", "prior", 
                                     &prior);
  if (status < 0)
    error (EXIT_FAILURE, 0, "Error reading motif prior");
  motif->prior = prior;
  unsigned long int n_feats;
  status = H5LTget_attribute_ulong (file_id, "/", "number of features", 
                                    &n_feats);
  if (status < 0)
    error (EXIT_FAILURE, 0, "Error reading number of features");
  motif->n_feats = (size_t) n_feats;
}


void
hdf5_read_pfm (hid_t file_id, motifstats *motif)
{
  size_t n = 0;
  motif->pfm = NULL;
  if (motif->type == SEQ_AA)
    {
      n = NUM_AA * motif->length;
      motif->pfm = gsl_matrix_alloc (NUM_AA, motif->length);
    }
  else
    {
      n = NUM_NUC * motif->length;
      motif->pfm = gsl_matrix_alloc (NUM_NUC, motif->length);
    }
  if (!motif->pfm)
    error (EXIT_FAILURE, errno, "Memory exhausted");
  double *data = NULL;
  if (ALLOC_N (data, n))
    error (EXIT_FAILURE, errno, "Memory exhausted");
  herr_t status;
  status = H5LTread_dataset_double (file_id, "/PFM", data);
  if (status < 0)
    error (EXIT_FAILURE, 0, "Error reading PFM");
  gsl_matrix_view pfm_v = gsl_matrix_view_array (data, motif->pfm->size1,
                                                 motif->pfm->size2);
  gsl_matrix_memcpy (motif->pfm, &pfm_v.matrix);
  FREE (data);
  char buf[21] = {'\0'};
  status = H5LTget_attribute_string (file_id, "/PFM", "pseudocount number",
                                     buf);
  if (status < 0)
    error (EXIT_FAILURE, 0, "Error getting pseudocount number");
  if (!strcmp (buf, "constant"))
    motif->pc_n = PC_N_CONST;
  else if (!strcmp (buf, "positional"))
    motif->pc_n = PC_N_POS;
  else if (!strcmp (buf, "none"))
    motif->pc_n = PC_N_NONE;
  else
    error (EXIT_FAILURE, 0, "Invalid pseudocount number");
  status = H5LTget_attribute_string (file_id, "/PFM", "pseudocount distribution",
                                     buf);
  if (status < 0)
    error (EXIT_FAILURE, 0, "Error getting pseudocount distribution");
  if (!strcmp (buf, "equal"))
    motif->pc_dist = PC_DIST_EQUAL;
  else if (!strcmp (buf, "background frequency"))
    motif->pc_dist = PC_DIST_FREQ;
  else
    error (EXIT_FAILURE, 0, "Invalid pseudocount distribution");
}


void
hdf5_read_bg (hid_t file_id, motifstats *motif)
{
  size_t n;
  if (motif->type == SEQ_AA)
    {
      n = NUM_AA * motif->length;
      motif->bg_freqs = gsl_matrix_alloc (NUM_AA, motif->length);
    }
  else
    {
      n = NUM_NUC * motif->length;
      motif->bg_freqs = gsl_matrix_alloc (NUM_NUC, motif->length);
    }
  if (!motif->bg_freqs)
    error (EXIT_FAILURE, errno, "Memory exhausted");
  double *data;
  if (ALLOC_N (data, n))
    error (EXIT_FAILURE, errno, "Memory exhausted");
  herr_t status;
  status = H5LTread_dataset_double (file_id, "/background", data);
  if (status < 0)
    error (EXIT_FAILURE, 0, "Error reading background frequencies");
  gsl_matrix_view bg_v = gsl_matrix_view_array (data, motif->bg_freqs->size1,
                                                 motif->bg_freqs->size2);
  gsl_matrix_memcpy (motif->bg_freqs, &bg_v.matrix);
  FREE (data);
  char buf[11] = {'\0'};
  status = H5LTget_attribute_string (file_id, "/background", "type", buf);
  if (status < 0)
    error (EXIT_FAILURE, 0, "Error getting background type");
  if (!strcmp (buf, "global"))
    motif->bg = BG_GLOBAL;
  else if (!strcmp (buf, "positional"))
    motif->bg = BG_POS;
  else if (!strcmp (buf, "uniform"))
    motif->bg = BG_NONE;
  else
    error (EXIT_FAILURE, 0, "Invalid background type");
}


void
hdf5_read_pssm (hid_t file_id, motifstats *motif)
{
  size_t n;
  if (motif->type == SEQ_AA)
    {
      n = NUM_AA * motif->length;
      motif->pssm = gsl_matrix_alloc (NUM_AA, motif->length);
    }
  else
    {
      n = NUM_NUC * motif->length;
      motif->pssm = gsl_matrix_alloc (NUM_NUC, motif->length);
    }
  if (!motif->pssm)
    error (EXIT_FAILURE, errno, "Memory exhausted");
  double *data;
  if (ALLOC_N (data, n))
    error (EXIT_FAILURE, errno, "Memory exhausted");
  herr_t status;
  status = H5LTread_dataset_double (file_id, "/PSSM", data);
  if (status < 0)
    error (EXIT_FAILURE, 0, "Error reading PSSM");
  gsl_matrix_view pssm_v = gsl_matrix_view_array (data, motif->pssm->size1,
                                                  motif->pssm->size2);
  gsl_matrix_memcpy (motif->pssm, &pssm_v.matrix);
  FREE (data);
  char buf[15] = {'\0'};
  status = H5LTget_attribute_string (file_id, "/PSSM", "method", buf);
  if (status < 0)
    error (EXIT_FAILURE, 0, "Error getting PSSM method");
  if (!strcmp (buf, "frequency"))
    motif->method = SCORE_FREQ;
  else if (!strcmp (buf, "log-likelihood"))
    motif->method = SCORE_LOGLIK;
  else if (!strcmp (buf, "difference"))
    motif->method = SCORE_DIFF;
  else if (!strcmp (buf, "bayes"))
    motif->method = SCORE_BAYES;
  else
    error (EXIT_FAILURE, 0, "Invalid PSSM method: %s", buf);
}


void
hdf5_read_ic (hid_t file_id, motifstats *motif)
{
  double *data;
  if (ALLOC_N (data, motif->length))
    error (EXIT_FAILURE, errno, "Memory exhausted");
  herr_t status;
  status = H5LTread_dataset_double (file_id, "/IC", data);
  if (status < 0)
    error (EXIT_FAILURE, 0, "Error reading column IC");
  motif->col_ic = gsl_vector_alloc (motif->length);
  if (!motif->col_ic)
    error (EXIT_FAILURE, errno, "Memory exhausted");
  gsl_vector_view ic_v = gsl_vector_view_array (data, motif->col_ic->size);
  gsl_vector_memcpy (motif->col_ic, &ic_v.vector);
  FREE (data);
  double base;
  status = H5LTget_attribute_double (file_id, "/IC", "logarithm base", &base);
  if (status < 0)
    error (EXIT_FAILURE, 0, "Error getting column IC base");
  motif->ic_base = base;
}


void
hdf5_read_relh (hid_t file_id, motifstats *motif)
{
  double *data;
  if (ALLOC_N (data, motif->length))
    error (EXIT_FAILURE, errno, "Memory exhausted");
  herr_t status;
  status = H5LTread_dataset_double (file_id, "/relH", data);
  if (status < 0)
    error (EXIT_FAILURE, 0, "Error reading column relH");
  motif->col_relh = gsl_vector_alloc (motif->length);
  if (!motif->col_relh)
    error (EXIT_FAILURE, errno, "Memory exhausted");
  gsl_vector_view relh_v = gsl_vector_view_array (data, motif->col_relh->size);
  gsl_vector_memcpy (motif->col_relh, &relh_v.vector);
  FREE (data);
  double base;
  status = H5LTget_attribute_double (file_id, "/relH", "logarithm base", &base);
  if (status < 0)
    error (EXIT_FAILURE, 0, "Error getting column IC base");
  if (base != motif->ic_base)
    error (EXIT_FAILURE, 0, "Inconsistent logarithmic bases");
}


void
hdf5_read_feature_cols (hid_t group_id, motifstats *motif)
{
  int *data;
  if (ALLOC_N (data, (size_t) motif->n_feats))
    error (EXIT_FAILURE, errno, "Memory exhausted");
  herr_t status = H5LTread_dataset_int (group_id, "/features/columns", data);
  if (status < 0)
    error (EXIT_FAILURE, 0, "Error reading feature columns");
  if (ALLOC_N (motif->feature_cols, (size_t) motif->n_feats))
    error (EXIT_FAILURE, errno, "Memory exhausted");
  for (size_t i=0; i<motif->n_feats; i++)
    {
      switch (data[i])
        {
        case 0:
          motif->feature_cols[i] = FEATURE_BERNOULLI;
          break;
        case 1:
          motif->feature_cols[i] = FEATURE_GAUSSIAN;
          break;
        case 2:
          motif->feature_cols[i] = FEATURE_BETA;
          break;
        }
    }
  FREE (data);
}


void
hdf5_read_bernoulli (hid_t group_id, motifstats *motif)
{
  unsigned long int n_bernoulli;
  herr_t status = H5LTget_attribute_ulong (group_id, "/features/bernoulli", "n", 
                                           &n_bernoulli);
  if (status < 0)
    error (EXIT_FAILURE, 0, "Error reading # Bernoulli features");
  motif->n_bernoulli = (size_t) n_bernoulli;
  if (n_bernoulli == 0)
    {
      motif->bernoulli_feats = NULL;
      return;
    }
  if (ALLOC_N (motif->bernoulli_feats, (size_t) n_bernoulli))
    error (EXIT_FAILURE, errno, "Memory exhausted");
  status = H5LTread_dataset_double (group_id, "/features/bernoulli", motif->bernoulli_feats);
  if (status < 0)
    error (EXIT_FAILURE, 0, "Error reading Bernoulli features");
}


void
hdf5_read_gaussian (hid_t group_id, motifstats *motif)
{
  unsigned long int n_gaussian;
  herr_t status = H5LTget_attribute_ulong (group_id, "/features/gaussian", "n", 
                                           &n_gaussian);
  if (status < 0)
    error (EXIT_FAILURE, 0, "Error reading # Gaussian features");
  motif->n_gaussian = (size_t) n_gaussian;
  if (n_gaussian == 0)
    {
      motif->gaussian_feats = NULL;
      return;
    }
  double *data;
  if (ALLOC_N (data, (size_t) n_gaussian * 2))
    error (EXIT_FAILURE, errno, "Memory exhausted");
  status = H5LTread_dataset_double (group_id, "/features/gaussian", data);
  if (status < 0)
    error (EXIT_FAILURE, 0, "Error reading Gaussian features");
  if (ALLOC_N (motif->gaussian_feats, (size_t) n_gaussian))
    error (EXIT_FAILURE, errno, "Memory exhausted");
  size_t j = 0;
  for (size_t i=0; i<n_gaussian; i++)
    {
      motif->gaussian_feats[i].mean = data[j++];
      motif->gaussian_feats[i].sd = data[j++];
    }
  FREE (data);
}


void
hdf5_read_beta (hid_t group_id, motifstats *motif)
{
  unsigned long int n_beta;
  herr_t status = H5LTget_attribute_ulong (group_id, "/features/beta", "n", 
                                           &n_beta);
  if (status < 0)
    error (EXIT_FAILURE, 0, "Error reading # Beta features");
  motif->n_beta = (size_t) n_beta;
  if (n_beta == 0)
    {
      motif->beta_feats = NULL;
      return;
    }
  double *data;
  if (ALLOC_N (data, (size_t) n_beta * 2))
    error (EXIT_FAILURE, errno, "Memory exhausted");
  status = H5LTread_dataset_double (group_id, "/features/beta", data);
  if (status < 0)
    error (EXIT_FAILURE, 0, "Error reading Beta features");
  if (ALLOC_N (motif->beta_feats, (size_t) n_beta))
    error (EXIT_FAILURE, errno, "Memory exhausted");
  size_t j = 0;
  for (size_t i=0; i<n_beta; i++)
    {
      motif->beta_feats[i].a = data[j++];
      motif->beta_feats[i].b = data[j++];
    }
  FREE (data);
}


void
hdf5_read_features (hid_t file_id, motifstats *motif)
{
  hid_t group_id = H5Gopen (file_id, "/features", H5P_DEFAULT);
  hdf5_read_feature_cols (file_id, motif);
  hdf5_read_bernoulli (file_id, motif);
  hdf5_read_gaussian (file_id, motif);
  hdf5_read_beta (file_id, motif);
  herr_t status = H5Gclose (group_id);
  if (status < 0)
    error (EXIT_FAILURE, 0, "Error closing the features group of the motifstats file");
}


/* Read a PSSM in JSON format from a file stream */
void
read_motif_stats (hid_t file_id, motifstats *motif)
{
  hdf5_read_root (file_id, motif);
  hdf5_read_pfm (file_id, motif);
  hdf5_read_bg (file_id, motif);
  hdf5_read_pssm (file_id, motif);
  hdf5_read_ic (file_id, motif);
  hdf5_read_relh (file_id, motif);
  if (motif->n_feats > 0)
    hdf5_read_features (file_id, motif);
}
