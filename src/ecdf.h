/* Copyright (C) 2019, 2020 Brandon Invergo <b.invergo@exeter.ac.uk> */

/* This program is free software: you can redistribute it and/or modify */
/* it under the terms of the GNU General Public License as published by */
/* the Free Software Foundation, either version 3 of the License, or */
/* (at your option) any later version. */

/* This program is distributed in the hope that it will be useful, */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the */
/* GNU General Public License for more details. */

/* You should have received a copy of the GNU General Public License */
/* along with this program.  If not, see <http://www.gnu.org/licenses/>. */


#ifndef ECDF_H
#define ECDF_H

#include <config.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_sort_vector.h>

#include "pssm.h"
#include "sequence.h"

void random_seq_win (char *seq_win, size_t win_len, gsl_matrix *freqs, gsl_rng *rng,
                     seq_type type);
void calc_ecdf (gsl_vector *ecdf, gsl_matrix *pm, gsl_matrix *bg_freqs,
                gsl_vector *col_weight, seq_type type, bg_type bg,
                score_method method, bool normalize, size_t n, size_t *omit,
                size_t n_omit);
double ecdf_prob (double value, gsl_vector *ecdf);

#endif
