/* Copyright (C) 2019, 2020, 2021 Brandon Invergo <b.invergo@exeter.ac.uk> */

/* This program is free software: you can redistribute it and/or modify */
/* it under the terms of the GNU General Public License as published by */
/* the Free Software Foundation, either version 3 of the License, or */
/* (at your option) any later version. */

/* This program is distributed in the hope that it will be useful, */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the */
/* GNU General Public License for more details. */

/* You should have received a copy of the GNU General Public License */
/* along with this program.  If not, see <http://www.gnu.org/licenses/>. */


#ifndef SEQUENCE_H
#define SEQUENCE_H

#include "gl_xlist.h"

typedef enum seq_type {SEQ_AA, SEQ_NUC, SEQ_GUESS} seq_type;

typedef struct seq_set {gl_list_t seqs; gl_list_t ids; seq_type type; size_t win_len;} seq_set;

typedef enum
  {
   ARG = 0, HIS, LYS, ASP, GLU, SER, THR, ASN, GLN, CYS, GLY, PRO, ALA, VAL, ILE,
   LEU, MET, PHE, TYR, TRP, NUM_AA, AA_TAIL, AA_ANY, STOP, AA_ERR
  } aminoacid;

static char aa2char [20] =
  {
   'R', 'H', 'K', 'D', 'E', 'S', 'T', 'N', 'Q', 'C',
   'G', 'P', 'A', 'V', 'I', 'L', 'M', 'F', 'Y', 'W'
  };

static char *aa2string [20] =
  {
   "R", "H", "K", "D", "E", "S", "T", "N", "Q", "C",
   "G", "P", "A", "V", "I", "L", "M", "F", "Y", "W"
  };

  static aminoacid char2aa [128] =
  {
   AA_ERR, AA_ERR, AA_ERR, AA_ERR, AA_ERR, AA_ERR, AA_ERR, AA_ERR,
   AA_ERR, AA_ERR, AA_ERR, AA_ERR, AA_ERR, AA_ERR, AA_ERR, AA_ERR,
   AA_ERR, AA_ERR, AA_ERR, AA_ERR, AA_ERR, AA_ERR, AA_ERR, AA_ERR,
   AA_ERR, AA_ERR, AA_ERR, AA_ERR, AA_ERR, AA_ERR, AA_ERR, AA_ERR,
   AA_ERR, AA_ERR, AA_ERR, AA_ERR, AA_ERR, AA_ERR, AA_ERR, AA_ERR,
   AA_ERR, AA_ERR, STOP, AA_ERR, AA_ERR, AA_ERR, AA_ERR, AA_ERR,
   AA_ERR, AA_ERR, AA_ERR, AA_ERR, AA_ERR, AA_ERR, AA_ERR, AA_ERR,
   AA_ERR, AA_ERR, AA_ERR, AA_ERR, AA_ERR, AA_ERR, AA_ERR, AA_ERR,
   AA_ERR, ALA, AA_ERR, CYS, ASP, GLU, PHE, GLY,
   HIS, ILE, AA_ERR, LYS, LEU, MET, ASN, AA_ERR,
   PRO, GLN, ARG, SER, THR, CYS, VAL, TRP,
   AA_ANY, TYR, AA_ERR, AA_ERR, AA_ERR, AA_ERR, AA_ERR, AA_TAIL,
   AA_ERR, ALA, AA_ERR, CYS, ASP, GLU, PHE, GLY,
   HIS, ILE, AA_ERR, LYS, LEU, MET, ASN, AA_ERR,
   PRO, GLN, ARG, SER, THR, CYS, VAL, TRP,
   AA_ANY, TYR, AA_ERR, AA_ERR, AA_ERR, AA_ERR, AA_ERR, AA_ERR,
  };

typedef enum
  {
   ADENINE = 0, CYTOSINE, GUANINE, THYMINE, NUM_NUC, NUC_TAIL, NUC_ANY,
   NUC_ERR
  } nucleotide;

static char nuc2char [4] = {'A', 'C', 'G', 'T'};

static char *nuc2string [4] = {"A", "C", "G", "T"};

static nucleotide char2nuc [128] =
  {
   NUC_ERR, NUC_ERR, NUC_ERR, NUC_ERR, NUC_ERR, NUC_ERR, NUC_ERR, NUC_ERR,
   NUC_ERR, NUC_ERR, NUC_ERR, NUC_ERR, NUC_ERR, NUC_ERR, NUC_ERR, NUC_ERR,
   NUC_ERR, NUC_ERR, NUC_ERR, NUC_ERR, NUC_ERR, NUC_ERR, NUC_ERR, NUC_ERR,
   NUC_ERR, NUC_ERR, NUC_ERR, NUC_ERR, NUC_ERR, NUC_ERR, NUC_ERR, NUC_ERR,
   NUC_ERR, NUC_ERR, NUC_ERR, NUC_ERR, NUC_ERR, NUC_ERR, NUC_ERR, NUC_ERR,
   NUC_ERR, NUC_ERR, NUC_ERR, NUC_ERR, NUC_ERR, NUC_ERR, NUC_ERR, NUC_ERR,
   NUC_ERR, NUC_ERR, NUC_ERR, NUC_ERR, NUC_ERR, NUC_ERR, NUC_ERR, NUC_ERR,
   NUC_ERR, NUC_ERR, NUC_ERR, NUC_ERR, NUC_ERR, NUC_ERR, NUC_ERR, NUC_ERR,
   NUC_ERR, ADENINE, NUC_ERR, CYTOSINE, NUC_ERR, NUC_ERR, NUC_ERR, GUANINE,
   NUC_ERR, NUC_ERR, NUC_ERR, NUC_ERR, NUC_ERR, NUC_ERR, NUC_ANY, NUC_ERR,
   NUC_ERR, NUC_ERR, NUC_ERR, NUC_ERR, THYMINE, NUC_ERR, NUC_ERR, NUC_ERR,
   NUC_ERR, NUC_ERR, NUC_ERR, NUC_ERR, NUC_ERR, NUC_ERR, NUC_ERR, NUC_TAIL,
   NUC_ERR, ADENINE, NUC_ERR, CYTOSINE, NUC_ERR, NUC_ERR, NUC_ERR, GUANINE,
   NUC_ERR, NUC_ERR, NUC_ERR, NUC_ERR, NUC_ERR, NUC_ERR, NUC_ANY, NUC_ERR,
   NUC_ERR, NUC_ERR, NUC_ERR, NUC_ERR, THYMINE, NUC_ERR, NUC_ERR, NUC_ERR,
   NUC_ERR, NUC_ERR, NUC_ERR, NUC_ERR, NUC_ERR, NUC_ERR, NUC_ERR, NUC_ERR
  };

#define aa_is_tail(A) (A == AA_TAIL)
#define aa_is_ambiguous(A) (A == AA_ANY || A == STOP)

#define nuc_is_tail(A) (A == NUC_TAIL)
#define nuc_is_ambiguous(A) (A == NUC_ANY)


#endif
