/* Copyright (C) 2019, 2020, 2021 Brandon Invergo <b.invergo@exeter.ac.uk> */

/* This program is free software: you can redistribute it and/or modify */
/* it under the terms of the GNU General Public License as published by */
/* the Free Software Foundation, either version 3 of the License, or */
/* (at your option) any later version. */

/* This program is distributed in the hope that it will be useful, */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the */
/* GNU General Public License for more details. */

/* You should have received a copy of the GNU General Public License */
/* along with this program.  If not, see <http://www.gnu.org/licenses/>. */


#include "sequences.h"

KSEQ_INIT(int, read)


/* Sequence equality function for use in gnulib's set data
   structure */
bool
seq_recs_equal (const void *elt1, const void *elt2)
{
  seq_rec *seq1 = (seq_rec *)elt1;
  seq_rec *seq2 = (seq_rec *)elt2;
  return strcmp (seq1->seq, seq2->seq) == 0;
}

/* Sequence hash function for use in gnulib's set data
   structure.  It uses the sdbm hash algorithm, adapted from:
   http://www.cse.yorku.ca/~oz/hash.html */
size_t
seq_rec_hash (const void *elt)
{
  size_t hash = 0;
  int c;
  seq_rec *rec = (seq_rec *)elt;
  char *seq = rec->seq;
  while ((c = *seq++))
    hash = c + (hash << 6) + (hash << 16) - hash;
  return hash;
}

void
seq_rec_dispose (const void *elt)
{
  seq_rec *win = (seq_rec *) elt;
  if (win->seq)
    FREE (win->seq);
  if (win)
    FREE (win);
}


/* Read sequences from a FASTA file.  This is only for calculating
   background frequencies, so we're only interested in the sequences
   themselves and not any of the metadata.  All of the sequences
   get stuffed into a set to ignore duplicates. */
void
read_sequences (seq_set *seq_recs, const char *fasta_file, seq_type type)
{
  int fasta_fd = open (fasta_file, O_RDONLY);
  if (fasta_fd < 0)
    error (EXIT_FAILURE, errno, "Failed to open FASTA file %s", fasta_file);
  kseq_t *seq_in = kseq_init (fasta_fd);
  int res;
  unsigned int dups = 0;
  seq_rec *rec;
  seq_recs->seqs = gl_list_nx_create_empty (GL_LINKEDHASH_LIST, seq_recs_equal, seq_rec_hash,
                                            seq_rec_dispose, false);
  if (!seq_recs->seqs)
    error (EXIT_FAILURE, errno, "Memory exhausted");
  seq_recs->win_len = 0;
  seq_recs->type = type;
  regex_t aa_regex;
  int regerr = regcomp (&aa_regex, "[RHKDESNQPVILMFYW]", REG_NOSUB | REG_ICASE);
  if (regerr)
    error (EXIT_FAILURE, errno, "Error compiling AA regex.  You shouldn't "
           "see this");
  while ((res = kseq_read (seq_in)) >= 0)
    {
      if (!seq_in->seq.s)
        continue;
      if (ALLOC (rec))
        error (EXIT_FAILURE, errno, "Memory exhausted");
      rec->seq = strdup (seq_in->seq.s);
      if (!rec->seq)
        error (EXIT_FAILURE, errno, "Memory exhausted");
      rec->weight = 1.0;
      if (seq_recs->type == SEQ_GUESS)
        {
          int regres = regexec (&aa_regex, rec->seq, 0, NULL, 0);
          if (regres == REG_ESPACE)
            error (EXIT_FAILURE, errno, "Memory exhausted");
          if (regres == 0)
            seq_recs->type = SEQ_AA;
          else
            seq_recs->type = SEQ_NUC;
        }
      if (!gl_list_nx_add_last (seq_recs->seqs, rec))
        {
          if (!gl_list_search (seq_recs->seqs, rec))
            error (EXIT_FAILURE, errno, "Memory exhausted");
          else
            {
              dups++;
              FREE (rec);
            }
        }
    }
  regfree (&aa_regex);
  if (res == -3)
    {
      error (EXIT_FAILURE, 0, "Error reading FASTA file %s", fasta_file);
    }
  if (dups > 0)
    {
      error (0, 0, "%u duplicate sequences skipped in FASTA file %s",
             dups, fasta_file);
    }
  kseq_destroy (seq_in);
}


void
clear_global_seq_set (seq_set *seq_recs)
{
  if (seq_recs->seqs)
    gl_list_free (seq_recs->seqs);
}


/* Calculate background frequencies from an arbitrary number of
   arbitrary-length sequences. The frequencies are given in a
   single-column matrix (one row per amino acid)*/
gsl_matrix *
calc_global_bg (const seq_set seqs)
{
  gsl_matrix *bg_freqs;
  if (seqs.type == SEQ_AA)
    bg_freqs = gsl_matrix_calloc (NUM_AA, 1);
  else if (seqs.type == SEQ_NUC)
    bg_freqs = gsl_matrix_calloc (NUM_NUC, 1);
  else
    error (EXIT_FAILURE, 0, "Sequence type was never guessed");
  if (!bg_freqs)
    error (EXIT_FAILURE, errno, "Memory exhausted");
  double num_res = 0;
  gl_list_iterator_t i = gl_list_iterator (seqs.seqs);
  const void *eltp;
  while (gl_list_iterator_next (&i, &eltp, NULL))
    {
      seq_rec *rec = (seq_rec *) eltp;
      char *seq = rec->seq;
      for (size_t j=0; j<strlen (seq); j++)
        {
          if (seqs.type == SEQ_AA)
            {
              aminoacid a = char2aa[(size_t) seq[j]];
              if (a == AA_ERR)
                error (EXIT_FAILURE, 0, "Invalid character in background sequence: %c",
                       seq[j]);
              else if (aa_is_ambiguous (a))
                continue;
              double count = gsl_matrix_get (bg_freqs, a, 0);
              gsl_matrix_set (bg_freqs, a, 0, count + 1.0);
              num_res += 1.0;
            }
          else
            {
              nucleotide n = char2nuc[(size_t) seq[j]];
              if (n == NUC_ERR)
                error (EXIT_FAILURE, 0, "Invalid character in background sequence: %c",
                       seq[j]);
              else if (nuc_is_ambiguous (n))
                continue;
              double count = gsl_matrix_get (bg_freqs, n, 0);
              gsl_matrix_set (bg_freqs, n, 0, count + 1.0);
              num_res += 1.0;
            }
        }
    }
  gl_list_iterator_free (&i);
  gsl_matrix_scale (bg_freqs, 1.0 / num_res);
  return bg_freqs;
}
