/* Copyright (C) 2019, 2020, 2021 Brandon Invergo <b.invergo@exeter.ac.uk> */

/* This program is free software: you can redistribute it and/or modify */
/* it under the terms of the GNU General Public License as published by */
/* the Free Software Foundation, either version 3 of the License, or */
/* (at your option) any later version. */

/* This program is distributed in the hope that it will be useful, */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the */
/* GNU General Public License for more details. */

/* You should have received a copy of the GNU General Public License */
/* along with this program.  If not, see <http://www.gnu.org/licenses/>. */


#ifndef SEQ_WINS_H
#define SEQ_WINS_H

#include <config.h>

#include <errno.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <regex.h>
#include "error.h"
#include "gl_linkedhash_list.h"
#include "gl_xlist.h"
#include "safe-alloc.h"
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_blas.h>

#include "sequence.h"


typedef struct seq_win {char *id; char *seq; double weight;} seq_win;

bool seq_wins_equal (const void *elt1, const void *elt2);
size_t seq_win_hash (const void *elt);
void seq_win_dispose (const void *elt);
int add_seq_win (seq_set *seq_wins, const char *seq, const char *id, seq_win *win, size_t win_len);
size_t read_seq_wins (seq_set *seq_wins, const char *seq_win_file, seq_type type);
void clear_seq_win_set (seq_set *seq_wins);
void seq_set_diff (const seq_set A, const seq_set B, seq_set *diff);
void seq_set_union (const seq_set A, const seq_set B, seq_set *set_union);
void count_pos_residues (const seq_set *seq_wins, gsl_vector *pos_res_counts,
                         gsl_matrix *res_counts);
void calc_pos_res_weights (const seq_set *seq_wins,
                           const gsl_vector *pos_res_counts,
                           const gsl_matrix *res_counts,
                           gsl_matrix *res_weights);
double calc_seq_weights (seq_set *seq_wins, const gsl_matrix *res_weights);
void normalize_weights (seq_set *seq_wins, double weight_sum);
void weight_seq_windows (seq_set *seq_wins);

#endif
