/* Copyright (C) 2021 Brandon M. Invergo <b.invergo@exeter.ac.uk> */

/* This program is free software: you can redistribute it and/or modify */
/* it under the terms of the GNU General Public License as published by */
/* the Free Software Foundation, either version 3 of the License, or */
/* (at your option) any later version. */

/* This program is distributed in the hope that it will be useful, */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the */
/* GNU General Public License for more details. */

/* You should have received a copy of the GNU General Public License */
/* along with this program.  If not, see <http://www.gnu.org/licenses/>. */

#include "naive-bayes.h"


size_t
read_bernoulli (const char *field, bool ***bernoulli_dat, size_t i, size_t j,
                size_t max_recs)
{
  size_t new_max = max_recs;
  if (j >= max_recs)
    {
      new_max += 100;
      if (REALLOC_N ((*bernoulli_dat)[i], new_max))
        error (EXIT_FAILURE, errno, "Memory exhausted");
    }
  if (!strcmp (field, "1") || !strcmp (field, "TRUE"))
    {
      (*bernoulli_dat)[i][j] = true;
    }
  else if (!strcmp (field, "0") || !strcmp (field, "FALSE"))
    {
      (*bernoulli_dat)[i][j] = false;
    }
  else
    error (EXIT_FAILURE, 0, "Bernoulli-distributed features should only have values "
           "of 0/FALSE or 1/TRUE.  Found '%s'.  Sorry for the inflexibility.", field);
  return new_max;
}


size_t
read_gaussian (const char *field, double ***gaussian_dat, size_t i, size_t j,
               size_t max_recs)
{
  errno = 0;
  double value = strtod (field, NULL);
  if (value == 0 && errno != 0)
    error (EXIT_FAILURE, errno, "Error parsing value in column %zu: %s", i + 2, field);
  if (value == -HUGE_VAL || value == HUGE_VAL)
    error (EXIT_FAILURE, errno, "Huge value found in column %zu: %s.  Cowardly "
           "running away from overflow.", i + 2, field);
  if (value <= -DBL_MAX)
    error (EXIT_FAILURE, errno, "Tiny value found in column %zu: %s.  Cowardly "
           "running away from underflow.", i + 2, field);
  size_t new_max = max_recs;
  if (j >= max_recs)
    {
      new_max += 100;
      if (REALLOC_N ((*gaussian_dat)[i], new_max))
        error (EXIT_FAILURE, errno, "Memory exhausted");
    }
  (*gaussian_dat)[i][j] = value;
  return new_max;
}


size_t
read_beta (const char *field, double ***beta_dat, size_t i, size_t j,
               size_t max_recs)
{
  errno = 0;
  double value = strtod (field, NULL);
  if (value == 0 && errno != 0)
    error (EXIT_FAILURE, errno, "Error parsing value in column %zu: %s", i + 2, field);
  if (value == -HUGE_VAL || value == HUGE_VAL)
    error (EXIT_FAILURE, errno, "Huge value found in column %zu: %s.  Cowardly "
           "running away from overflow.", i + 2, field);
  if (value <= -DBL_MAX)
    error (EXIT_FAILURE, errno, "Tiny value found in column %zu: %s.  Cowardly "
           "running away from underflow.", i + 2, field);
  if (value < 0.0 || value > 1.0)
    error (EXIT_FAILURE, errno, "Beta-distributed values must be between 0 and 1. Found %f",
           value);
  size_t new_max = max_recs;
  if (j >= max_recs)
    {
      new_max += 100;
      if (REALLOC_N ((*beta_dat)[i], new_max))
        error (EXIT_FAILURE, errno, "Memory exhausted");
    }
  (*beta_dat)[i][j] = value;
  return new_max;
}


/* Read in a file of sequence windows and features, one instance per
   line.  Return the number of records for which extra features were
   read. Duplicates sequences are not allowed. */
size_t
read_features (seq_set *seq_wins, bool ***bernoulli_dat, double ***gaussian_dat,
               double ***beta_dat, const char *feature_file, bayes_feat *feat_types,
               size_t n_feats, size_t *n_bernoulli, size_t *n_gaussian, size_t *n_beta,
               seq_type type, char delim, seq_set *feat_exclude)
{
  FILE *feature_stream;
  if (!feature_file || !strcmp (feature_file, "-"))
    feature_stream = stdin;
  else
    {
      feature_stream = fopen (feature_file, "r");
      if (!feature_stream)
        error (EXIT_FAILURE, errno, "Failed to open feature file %s",
               feature_file);
    }
  seq_wins->seqs = gl_list_nx_create_empty (GL_LINKEDHASH_LIST, seq_wins_equal,
                                            seq_win_hash, seq_win_dispose, true);
  if (!seq_wins->seqs)
    error (EXIT_FAILURE, errno, "Memory exhausted");
  for (size_t i=0; i<n_feats; i++)
    {
      switch (feat_types[i])
        {
        case FEATURE_BERNOULLI:
          (*n_bernoulli)++;
          break;
        case FEATURE_GAUSSIAN:
          (*n_gaussian)++;
          break;
        case FEATURE_BETA:
          (*n_beta)++;
          break;
        }
    }
  size_t num_recs = 0;
  size_t max_recs = 100;
  if (*n_bernoulli > 0)
    {
      if (ALLOC_N (*bernoulli_dat, *n_bernoulli))
        error (EXIT_FAILURE, errno, "Memory exhausted");
      for (size_t i=0; i<(*n_bernoulli); i++)
        {
          if (ALLOC_N ((*bernoulli_dat)[i], max_recs))
            error (EXIT_FAILURE, errno, "Memory exhausted");
        }
    }
  if (*n_gaussian > 0)
    {
      if (ALLOC_N (*gaussian_dat, *n_gaussian))
        error (EXIT_FAILURE, errno, "Memory exhausted");
      for (size_t i=0; i<(*n_gaussian); i++)
        {
          if (ALLOC_N ((*gaussian_dat)[i], max_recs))
            error (EXIT_FAILURE, errno, "Memory exhausted");
        }
    }
  if (*n_beta > 0)
    {
      if (ALLOC_N (*beta_dat, *n_beta))
        error (EXIT_FAILURE, errno, "Memory exhausted");
      for (size_t i=0; i<(*n_beta); i++)
        {
          if (ALLOC_N ((*beta_dat)[i], max_recs))
            error (EXIT_FAILURE, errno, "Memory exhausted");
        }
    }
  char *line = NULL;
  size_t n = 0;
  size_t win_len = 0;
  seq_wins->type = type;
  regex_t aa_regex;
  if (seq_wins->type == SEQ_GUESS)
    {
      int regerr = regcomp (&aa_regex, "[RHKDESNQPVILMFYW]", REG_NOSUB | REG_ICASE);
      if (regerr)
        error (EXIT_FAILURE, errno, "Error compiling AA regex.  You shouldn't "
               "see this");
    }
  const char delimiters[] = {delim, '\n'};
  while (getline (&line, &n, feature_stream) >= 0)
    {
      char *line_cp = strdup (line);
      if (!line_cp)
        error (EXIT_FAILURE, errno, "Memory exhausted");
      /* The first field is the sequence id */
      char *field = strtok (line_cp, delimiters);
      if (field == NULL)
        continue;
      char *id = strdup (field);
      if (!id)
        error (EXIT_FAILURE, errno, "Memory exhausted");
      /* The second field is the sequence window */
      field = strtok (NULL, delimiters);
      if (field == NULL)
        error (EXIT_FAILURE, errno, "Error reading sequence window");
      char *win_str = strdup (field);
      if (!win_str)
        error (EXIT_FAILURE, errno, "Memory exhausted");
      size_t field_len = strlen (win_str);
      /* Guess the window length from the first sequence */
      if (win_len == 0)
        {
          /* getline includes the newline */
          win_len = field_len;
        }
      /* If necessary, guess the sequence type from the first sequence */
      if (seq_wins->type == SEQ_GUESS)
        {
          int regres = regexec (&aa_regex, field, 0, NULL, 0);
          if (regres == REG_ESPACE)
            error (EXIT_FAILURE, errno, "Memory exhausted");
          if (regres == 0)
            seq_wins->type = SEQ_AA;
          else
            seq_wins->type = SEQ_NUC;
          regfree (&aa_regex);
        }
      seq_win *win = NULL;
      if (ALLOC (win))
        {
          error (EXIT_FAILURE, errno, "Memory exhausted");
        };
      add_seq_win (seq_wins, win_str, id, win, win_len);
      FREE (id);
      FREE (win_str);
      if (n_feats == 0)
        {
          FREE (line_cp);
          continue;
        }
      /* When reading the background data, we only need the other
         features for the complementary motif (whereas we read in all
         sequence windows, which are filtered elsewhere to form a
         complementary set).  So, for reading background data, the
         foreground sequence set is passed in, and if the currently
         read sequence window is in the foreground set, just skip
         reading the remaining fields. */
      if (feat_exclude != NULL && gl_list_search (feat_exclude->seqs, win))
        {
          FREE (line_cp);
          continue;
        }
      size_t bernoulli_i = 0;
      size_t gaussian_i = 0;
      size_t beta_i = 0;
      size_t new_max = max_recs;
      for (size_t i=0; i<n_feats; i++)
        {
          field = strtok (NULL, delimiters);
          if (field == NULL)
            error (EXIT_FAILURE, 0, "Not enough columns detected: "
                   "looking for %zd, found %zd", n_feats + 1, i + 1);
          switch (feat_types[i])
            {
            case FEATURE_BERNOULLI:
              new_max = read_bernoulli (field, bernoulli_dat, bernoulli_i++, num_recs, max_recs);
              break;
            case FEATURE_GAUSSIAN:
              new_max = read_gaussian (field, gaussian_dat, gaussian_i++, num_recs, max_recs);
              break;
            case FEATURE_BETA:
              new_max = read_beta (field, beta_dat, beta_i++, num_recs, max_recs);
              break;
            }
        }
      if (max_recs != new_max)
        max_recs = new_max;
      num_recs++;
    }
  fclose (feature_stream);
  if (line)
    FREE (line);
  if (bernoulli_dat)
    {
      for (size_t i=0; i<(*n_bernoulli); i++)
        if (REALLOC_N ((*bernoulli_dat)[i], num_recs))
          error (EXIT_FAILURE, errno, "Memory exhausted");
    }
  if (gaussian_dat)
    {
      for (size_t i=0; i<(*n_gaussian); i++)
        if (REALLOC_N ((*gaussian_dat)[i], num_recs))
          error (EXIT_FAILURE, errno, "Memory exhausted");
    }
  if (beta_dat)
    {
      for (size_t i=0; i<(*n_beta); i++)
        if (REALLOC_N ((*beta_dat)[i], num_recs))
          error (EXIT_FAILURE, errno, "Memory exhausted");
    }
  if (gl_list_size (seq_wins->seqs) == 0 || win_len == 0)
    {
      error (EXIT_FAILURE, 0, "No sequences found in feature file %s", feature_file);
    }
  seq_wins->win_len = win_len;
  return num_recs;
}


/* Parameterize a Bernoulli-distributed feature, with additive
   smoothing via parameter alpha. */
double
bernoulli_param (const bool *dat, size_t n, double alpha)
{
  double t = 0.0;
  for (size_t i=0; i<n; i++)
    {
      if (dat[i])
        t += 1.0;
    }
  return log10 (t + alpha) - log10 ((double) n + 2 * alpha);
}


/* Parameterize a Gaussian-distributed feature */
gaussian_dist
gaussian_param (const double *dat, size_t n)
{
  gaussian_dist feat = {.mean = 0, .sd = 1};
  feat.mean = gsl_stats_mean (dat, 1, n);
  feat.sd = gsl_stats_sd (dat, 1, n);
  return feat;
}


/* Parameterize a Beta-distributed feature via the maximum likelihood
   method.  This code was carefully adapted from "Parameter
   estimation for the beta distribution" by CEB Owen (thesis, Brigham
   Young University)
   https://scholarsarchive.byu.edu/cgi/viewcontent.cgi?article=2613&context=etd */
beta_dist
beta_param (const double *dat, size_t n, bool squeeze, double s)
{
  beta_dist feat = {.a = 1, .b = 1};
  gsl_vector *dat_v = gsl_vector_alloc (n);
  gsl_vector *log_dat = gsl_vector_alloc (n);
  gsl_vector *log_dat_p = gsl_vector_alloc (n);
  gsl_vector_const_view dat_view = gsl_vector_const_view_array (dat, n);
  gsl_vector_memcpy (dat_v, &dat_view.vector);
  if (squeeze)
    {
      for (size_t i = 0; i<n; i++)
        {
          double val = gsl_vector_get (dat_v, i);
          val = (val * ((double) n - 1.0) + s) / (double) n;
          gsl_vector_set (dat_v, i, val);
        }
    }
  gsl_vector_memcpy (log_dat, dat_v);
  for (size_t i=0; i<n; i++)
    {
      double val = gsl_vector_get (log_dat, i);
      double val_p = 1.0 - val;
      gsl_vector_set (log_dat, i, log (val));
      gsl_vector_set (log_dat_p, i, log (val_p));
    }
  double log_mean = gsl_stats_mean (log_dat->data, 1, n);
  double log_mean_p = gsl_stats_mean (log_dat_p->data, 1, n);
  double init_a = 0.5 + exp(log_mean)/(2*(1-exp(log_mean)-exp(log_mean_p)));
  double init_b = 0.5 + exp(log_mean_p)/(2*(1-exp(log_mean)-exp(log_mean_p)));
  /* The above initial values are good for a > 1 and b > 1. If that
     doesn't hold, switch to method of moments estimations for initial
     values */
  if (init_a < 1 || init_b < 1)
    {
      double pop_mean = gsl_stats_mean (dat_v->data, 1, n);
      double pop_var = gsl_stats_variance (dat_v->data, 1, n);
      if (pop_var == 0)
        {
          error (EXIT_FAILURE, 0, "Error: Beta distributed data has 0 variance");
        }
      double x = pop_mean * (1 - pop_mean);
      if (pop_var > x)
        {
          error (0, 0, "Warning: Beta-distributed data does not meet assumptions for "
                 "initial parameter estimations (variance too high).");
        }
      /* initial method of moments estimates */
      init_a = pop_mean * ((pop_mean * (1 - pop_mean))/pop_var - 1);
      init_b = (1 - pop_mean) * ((pop_mean * (1 - pop_mean))/pop_var - 1);
    }
  gsl_vector *cur_est = gsl_vector_alloc (2);
  gsl_vector_set (cur_est, 0, init_a);
  gsl_vector_set (cur_est, 1, init_b);
  gsl_matrix *gp = gsl_matrix_alloc (2, 2);
  gsl_vector *g = gsl_vector_alloc (2);
  gsl_permutation *p = gsl_permutation_alloc (2);
  gsl_vector *sol = gsl_vector_alloc (2);
  gsl_vector *new_est = gsl_vector_alloc (2);
  size_t maxiter = 1000;
  double tol = 1e-5, lim1 = 1e-8, lim2 = -5000, eps = 1;
  size_t i = 0;
  bool use_mom = false;
  while (tol < eps && i < maxiter)
    {
      double a = gsl_vector_get (cur_est, 0);
      double b = gsl_vector_get (cur_est, 1);
      if (-log (a) < (GSL_LOG_DBL_MIN + 1.0) || -log (a + b) < (GSL_LOG_DBL_MIN + 1.0))
        {
          error (0, 0, "Warning: underflow caught in MLE of Beta parameters. "
                 "Using MoM estimate.");
          use_mom = true;
          break;
        }
      /* gsl_blas_dasum computes the absolute sum of the vector.  All
         of our log-transformed values must be negative because the
         original values must be between 0 and 1. Given these
         assumptions, it's ok to just flip the sign on the dasum
         result.  I'll do that explicitly here (even though it's
         totally unnecessary) just to be clear that this is what's
         happening. */
      double g1 = gsl_sf_psi (a) - gsl_sf_psi (a + b) - -gsl_blas_dasum (log_dat)/n;
      double g2 = gsl_sf_psi (b) - gsl_sf_psi (a + b) - -gsl_blas_dasum (log_dat_p)/n;
      if (g1 < lim2 || g2 < lim2)
        {
          break;
        }
      double g1a = gsl_sf_psi_1 (a) - gsl_sf_psi_1 (a + b);
      double g1b = -gsl_sf_psi_1 (a + b);
      double g2a = g1b;
      double g2b = gsl_sf_psi_1 (b) - gsl_sf_psi_1 (a + b);
      if (fabs (g1a) < lim1 || fabs (g1b) < lim1 || fabs (g2b) < lim1)
        {
          break;
        }
      gsl_matrix_set (gp, 0, 0, g1a);
      gsl_matrix_set (gp, 0, 1, g1b);
      gsl_matrix_set (gp, 1, 0, g2a);
      gsl_matrix_set (gp, 1, 1, g2b);
      int signum;
      gsl_linalg_LU_decomp (gp, p, &signum);
      gsl_linalg_LU_invx (gp, p);
      gsl_vector_set (g, 0, g1);
      gsl_vector_set (g, 1, g2);
      /* Calculate cur_est - gp^-1 * g */
      gsl_blas_dgemv (CblasNoTrans, 1.0, gp, g, 0.0, sol);
      gsl_vector_memcpy (new_est, cur_est);
      gsl_vector_sub (new_est, sol);
      double a_i = gsl_vector_get (new_est, 0);
      double b_i = gsl_vector_get (new_est, 1);
      eps = GSL_MAX (fabs ((a - a_i)/a), fabs ((b - b_i)/b));
      gsl_vector_memcpy (cur_est, new_est);
      i++;
    }
  gsl_matrix_free (gp);
  gsl_vector_free (g);
  gsl_permutation_free (p);
  gsl_vector_free (sol);
  gsl_vector_free (new_est);
  gsl_vector_free (log_dat);
  gsl_vector_free (log_dat_p);
  if (gsl_vector_get (cur_est, 0) < 0 ||
      gsl_vector_get (cur_est, 1) < 0)
    {
      error (0, 0, "Warning: MLE inference of Beta parameters failed to converge "
             "to a sensible solution.  Falling back to initial estimates.");
      use_mom = true;
    }
  if (use_mom)
    {
      feat.a = init_a;
      feat.b = init_b;
    }
  else
    {
      feat.a = gsl_vector_get (cur_est, 0);
      feat.b = gsl_vector_get (cur_est, 1);
    }
  gsl_vector_free (cur_est);
  return feat;
}


double
score_bernoulli (bool value, double p)
{
  if (value)
    {
      return (p);
    }
  return (1.0 - p);
}


double
score_gaussian (double value, gaussian_dist distribution)
{
  return (log10 (gsl_ran_gaussian_pdf (value - distribution.mean, distribution.sd)));
}


double
score_beta (double value, beta_dist distribution)
{
  return (log10 (gsl_ran_beta_pdf (value, distribution.a, distribution.b)));
}

/* Use a transformation of Bayes' rule (divide numerator and
   denomonator by the likelihood), taking advantage of the fact that
   the likelihoods are already log10-transformed, to avoid possible
   problems with exceeding floating point precision */

double
score_naive_bayes (double seq_likl, double compl_seq_likl,
                   const double *bernoulli_likl, const double *compl_bernoulli_likl,
                   size_t n_bernoulli, const double *gaussian_likl,
                   const double *compl_gaussian_likl, size_t n_gaussian,
                   const double *beta_likl, const double *compl_beta_likl,
                   size_t n_beta, double prior, double compl_prior)
{
  double likl = seq_likl;
  double compl_likl = compl_seq_likl;
  for (size_t i=0; i<n_bernoulli; i++)
    {
      likl += bernoulli_likl[i];
      compl_likl += compl_bernoulli_likl[i];
    }
  for (size_t i=0; i<n_gaussian; i++)
    {
      likl += gaussian_likl[i];
      compl_likl += compl_gaussian_likl[i];
    }
  for (size_t i=0; i<n_beta; i++)
    {
      likl += beta_likl[i];
      compl_likl += compl_beta_likl[i];
    }
  /* Beta distribution can give probabilities of 0 */
  if (gsl_isnan (likl) || gsl_isinf (compl_likl))
    return 0.0;
  if (gsl_isnan (compl_likl) || gsl_isinf (likl))
    return 1.0;
  return prior / (prior + compl_prior * exp10 (compl_likl - likl));
}
