/* Copyright (C) 2019, 2020, 2021 Brandon Invergo <b.invergo@exeter.ac.uk> */

/* This program is free software: you can redistribute it and/or modify */
/* it under the terms of the GNU General Public License as published by */
/* the Free Software Foundation, either version 3 of the License, or */
/* (at your option) any later version. */

/* This program is distributed in the hope that it will be useful, */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the */
/* GNU General Public License for more details. */

/* You should have received a copy of the GNU General Public License */
/* along with this program.  If not, see <http://www.gnu.org/licenses/>. */



#include <config.h>

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <limits.h>
#include <float.h>
#include <unistd.h>
#include "argp.h"
#include "error.h"
#include "math.h"
#include "safe-alloc.h"
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_vector.h>
#include <hdf5.h>

#include "motifstats.h"
#include "pssm.h"
#include "ecdf.h"


const char *argp_program_version = PACKAGE_STRING;
const char *argp_program_bug_address = PACKAGE_BUGREPORT;

static char doc[] =
  "motif-score -- calculate scores for a set of sequence windows given a motif."
  "\v"
  "This program calculates a score for each sequence window in SEQFILE using "
  "the PSSM stored in MOTIFFILE.  If SEQFILE is not specified, the sequences "
  "are read from standard input.  If MOTIFFILE contains a motif constructed "
  "for Naive Bayes scoring, then the complementary motif stored in "
  "COMPLEMENT_MOTIFFILE must also be provided."
  "\n\n"
  "You may optionally specify a delimiter "
  "character and a column number; in this case, SEQFILE is assumed to be a "
  "delimiter-separated table file (e.g. CSV) and the sequence windows are "
  "contained in the specified column (starting from column 1).";

static char args_doc[] = "MOTIFFILE [COMPLEMENT_MOTIFFILE] SEQFILE";

static struct argp_option options[] =
  {
   {0, 0, 0, 0, "Method", 1},
   {"normalize", 'n', 0, 0,
    "Normalize the scores against the minimum and maximum possible scores for "
    "the PSSM", 0},
   {"pvals", 'p', "PRECISION", OPTION_ARG_OPTIONAL,
    "Calculate p-values using an empirical cumulative distribution function "
    "built from PRECISION samples (default: 3, i.e. 1e3 = 1000 samples)", 0},
   {"scale", 's', "BY", 0, "Scale each column in the PSSM by: 'ic' (information "
    "content); 'relh' (relative entropy vs the background); 'gr' (gain ratio, "
    "Naive Bayes scoring only); or 'none' (default)",
    0},
   {"omit", 'o', "POSITIONS", 0, "Omit the listed, comma-separated POSITIONS "
    "from the final score calculation", 0},
   {0, 0, 0, 0, "Input/Output", 2},
   {"append", 'a', 0, 0,
    "Append the scores as a new column to the table contained in SEQFILE", 0},
   {"seq-col", 'c', "VALUE", 0,
    "The column number (starting from 1) containing the sequence windows "
    "if SEQFILE contains a table (default: 1)", 0},
   {"feats-col", 'f', "VALUE", 0,
    "The column number (starting from 1) containing the first Naive Bayes feature "
    "(if any) if SEQFILE contains a table (default: 2).  It is assumed that all "
    "Naive Bayes feature columns are adjacent to each other in the same order as "
    "the training data.", 0},
   {"delim", 'd', "CHAR", 0,
    "The delimiter character used to separate columns if SEQFILE contains a "
    "table (default: \\t)", 0},
   {"header", 'H', 0, 0,
    "Ignore the first line in SEQFILE as a header line.  IF --append is also "
    "passed, the header will be included in the output", 0},
   {0}
  };

struct arguments
{
  char *args[3];
  char delim;
  size_t seq_col;
  size_t feat_col;
  bool normalize;
  bool append;
  bool header;
  size_t pval;
  scale_method scale;
  size_t *omit;
  size_t n_omit;
};

static error_t
parse_opt (int key, char *arg, struct argp_state *state)
{
  struct arguments *arguments = state->input;
  switch  (key)
    {
    case 'a':
      arguments->append = true;
      break;
    case 'c':
      {
        unsigned long long int c = strtoul (arg, NULL, 0);
        if (c == ULLONG_MAX)
          error (EXIT_FAILURE, errno, "Error parsing column number");
        else if (c == 0)
          c = 1;
        arguments->seq_col = (size_t) c - 1;
        break;
      }
    case 'd':
      if (strlen (arg) != 1)
        argp_usage (state);
      arguments->delim = arg[0];
      break;
    case 'f':
      {
        unsigned long long int c = strtoul (arg, NULL, 0);
        if (c == ULLONG_MAX)
          error (EXIT_FAILURE, errno, "Error parsing column number");
        else if (c == 0)
          c = 2;
        arguments->feat_col = (size_t) c - 1;
        break;
      }
    case 'H':
      arguments->header = true;
      break;
    case 'n':
      arguments->normalize = true;
      break;
    case 'o':
      {
        size_t maxomit = 15;
        if (ALLOC_N (arguments->omit, maxomit))
          error (EXIT_FAILURE, errno, "Memory exhausted");
        char *index = strtok (arg, ",");
        if (index == NULL)
          argp_usage (state);
        size_t n_omit = 0;
        while (index)
          {
            unsigned long int i = strtoul (index, NULL, 0);
            if (i == ULONG_MAX && errno == ERANGE)
              error (EXIT_FAILURE, errno, "Overflow when parsing a position");
            if (i > SIZE_MAX)
              error (EXIT_FAILURE, 0, "Overflow when parsing position");
            if (i == 0)
              error (EXIT_FAILURE, 0, "Positions to omit should be 1-indexed");
            if (n_omit >= maxomit)
              {
                maxomit += 10;
                if (REALLOC_N (arguments->omit, maxomit))
                  error (EXIT_FAILURE, errno, "Memory exhausted");
              }
            arguments->omit[n_omit++] = i - 1;
            index = strtok (NULL, ",");
          }
        arguments->n_omit = n_omit;
        break;
      }
    case 'p':
      if (!arg || strlen (arg) == 0)
        {
          arguments->pval = 3;
          break;
        }
      unsigned long int val = strtoul (arg, NULL, 0);
      if (val == ULONG_MAX || val > SIZE_MAX)
        error (EXIT_FAILURE, errno, "Failed to parse ECDF iterations");
      if (val > 14)
        error (EXIT_FAILURE, 0, "Precision is limited to a maximum of 14");
      arguments->pval = (size_t) val;
      break;
    case 's':
      if (!strcmp (arg, "ic"))
        arguments->scale = SCALE_IC;
      else if (!strcmp (arg, "relh"))
        arguments->scale = SCALE_RELH;
      else if (!strcmp (arg, "gr"))
        arguments->scale = SCALE_GR;
      else if (!strcmp (arg, "none"))
        arguments->scale = SCALE_NONE;
      else
        argp_usage (state);
      break;
    case ARGP_KEY_ARG:
      if (state->arg_num > 2)
        argp_usage (state);
      arguments->args[state->arg_num] = arg;
      break;
    default:
      return ARGP_ERR_UNKNOWN;
    }
  return 0;
}

static struct argp argp = {options, parse_opt, args_doc, doc};
char *fail_msg = NULL;


void
init_args (struct arguments *arguments)
{
  arguments->args[0] = NULL;
  arguments->args[1] = NULL;
  arguments->args[2] = NULL;
  arguments->delim = '\t';
  arguments->seq_col = 0;
  arguments->feat_col = 1;
  arguments->normalize = false;
  arguments->append = false;
  arguments->header = false;
  arguments->pval = 0;
  arguments->scale = SCALE_NONE;
  arguments->omit = NULL;
  arguments->n_omit = 0;
}


void
setup_col_weights (gsl_vector **col_weight, gsl_vector **compl_col_weight,
                   motifstats motif, motifstats compl_motif,
                   struct arguments arguments)
{
  switch (arguments.scale)
    {
    case SCALE_IC:
      {
        *col_weight = motif.col_ic;
        if (motif.method == SCORE_BAYES)
          *compl_col_weight = motif.col_ic;
        break;
      }
    case SCALE_RELH:
      {
        *col_weight = motif.col_relh;
        if (motif.method == SCORE_BAYES)
          *compl_col_weight = motif.col_relh;
        break;
      }
    case SCALE_GR:
      {
        *col_weight = gsl_vector_alloc (motif.length);
        if (!col_weight)
          error (EXIT_FAILURE, errno, "Memory exhausted");
        calc_col_gr (*col_weight, motif.pfm, compl_motif.pfm, motif.prior,
                     compl_motif.prior, motif.col_relh, compl_motif.col_relh,
                     motif.bg_freqs, motif.ic_base);
        *compl_col_weight = gsl_vector_alloc (motif.length);
        if (!(*compl_col_weight))
          error (EXIT_FAILURE, errno, "Memory exhausted");
        gsl_vector_memcpy (*compl_col_weight, *col_weight);
        break;
      }
    }
}


void
score_sequences (FILE *seq_stream, motifstats motif, motifstats compl_motif,
                 gsl_vector *ecdf, struct arguments arguments)
{
  char *line = NULL;
  size_t n = 0;
  const char delimiters[] = {arguments.delim, '\n'};
  bool header_handled = false;
  gsl_vector *col_weight = NULL;
  gsl_vector *compl_col_weight = NULL;
  setup_col_weights (&col_weight, &compl_col_weight, motif, compl_motif, arguments);
  double *bernoulli_likl = NULL, *compl_bernoulli_likl = NULL;
  if (motif.n_bernoulli > 0)
    {
      if (ALLOC_N (bernoulli_likl, motif.n_bernoulli))
        error (EXIT_FAILURE, errno, "Memory exhausted");
      if (ALLOC_N (compl_bernoulli_likl, motif.n_bernoulli))
        error (EXIT_FAILURE, errno, "Memory exhausted");
    }
  double *gaussian_likl = NULL, *compl_gaussian_likl = NULL;
  if (motif.n_gaussian > 0)
    {
      if (ALLOC_N (gaussian_likl, motif.n_gaussian))
        error (EXIT_FAILURE, errno, "Memory exhausted");
      if (ALLOC_N (compl_gaussian_likl, motif.n_gaussian))
        error (EXIT_FAILURE, errno, "Memory exhausted");
    }
  double *beta_likl = NULL, *compl_beta_likl = NULL;
  if (motif.n_beta > 0)
    {
      if (ALLOC_N (beta_likl, motif.n_beta))
        error (EXIT_FAILURE, errno, "Memory exhausted");
      if (ALLOC_N (compl_beta_likl, motif.n_beta))
        error (EXIT_FAILURE, errno, "Memory exhausted");
    }

  while (getline (&line, &n, seq_stream) >= 0)
    {
      char *line_cp = strdup (line);
      if (arguments.header && !header_handled)
        {
          char *header = strtok (line_cp, "\n");
          if (arguments.pval)
            printf ("%s%c%s.score%c%s.pval\n", header, arguments.delim,
                    motif.name, arguments.delim, motif.name);
          else
            printf ("%s%c%s.score\n", header, arguments.delim, motif.name);
          header_handled = true;
          continue;
        }
      size_t i = 0;
      double score;
      size_t bernoulli_j = 0;
      size_t gaussian_j = 0;
      size_t beta_j = 0;
      double likl, compl_likl;
      /* Iterate over the fields */
      char *field = strtok (line_cp, delimiters);
      while (field)
        {
          if (i == arguments.seq_col)
            {
              score = score_sequence (field, motif.pssm, col_weight,
                                      motif.type, motif.method,
                                      arguments.normalize, arguments.omit,
                                      arguments.n_omit);
              if (motif.method == SCORE_BAYES)
                {
                  likl = score;
                  compl_likl = score_sequence (field, compl_motif.pssm,
                                               compl_col_weight, compl_motif.type,
                                               compl_motif.method, false,
                                               arguments.omit, arguments.n_omit);
                }
            }
          else if (motif.n_feats > 0 &&
                   i >= arguments.feat_col &&
                   i < arguments.feat_col + motif.n_feats)
            {
              size_t j = i - arguments.feat_col;
              switch (motif.feature_cols[j])
                {
                case FEATURE_BERNOULLI:
                  {
                    bool value;
                    if (!strcmp (field, "1") || !strcmp (field, "TRUE"))
                      {
                        value = true;
                      }
                    else if (!strcmp (field, "0") || !strcmp (field, "FALSE"))
                      {
                        value = false;
                      }
                    else
                      {
                        error (EXIT_FAILURE, 0, "Bernoulli-distributed features should only have values "
                               "of 0/FALSE or 1/TRUE.  Found '%s'.  Sorry for the inflexibility.",
                               field);
                      }
                    bernoulli_likl[bernoulli_j] = score_bernoulli (value, motif.bernoulli_feats[bernoulli_j]);
                    compl_bernoulli_likl[bernoulli_j] = score_bernoulli (value, compl_motif.bernoulli_feats[bernoulli_j]);
                    bernoulli_j++;
                    break;
                  }
                case FEATURE_GAUSSIAN:
                  {
                    double value = strtod (field, NULL);
                    if (value == 0 && errno != 0)
                      error (EXIT_FAILURE, errno, "Error parsing value in column %zu.", i + 1);
                    if (value == -HUGE_VAL || value == HUGE_VAL)
                      error (EXIT_FAILURE, errno, "Huge value found in column %zu.  Cowardly "
                             "running away from overflow.", i + 1);
                    if (value <= -DBL_MAX)
                      error (EXIT_FAILURE, errno, "Tiny value found in column %zu.  Cowardly "
                             "running away from underflow.", i + 1);
                    gaussian_likl[gaussian_j] = score_gaussian (value, motif.gaussian_feats[gaussian_j]);
                    compl_gaussian_likl[gaussian_j] = score_gaussian (value, compl_motif.gaussian_feats[gaussian_j]);
                    gaussian_j++;
                    break;
                  }
                case FEATURE_BETA:
                  {
                    double value = strtod (field, NULL);
                    if (value == 0 && errno != 0)
                      error (EXIT_FAILURE, errno, "Error parsing value in column %zu.", i + 1);
                    if (value == -HUGE_VAL || value == HUGE_VAL)
                      error (EXIT_FAILURE, errno, "Huge value found in column %zu.  Cowardly "
                             "running away from overflow.", i + 1);
                    if (value <= -DBL_MAX)
                      error (EXIT_FAILURE, errno, "Tiny value found in column %zu.  Cowardly "
                             "running away from underflow.", i + 1);
                    if (value < 0 || value > 1)
                      error (EXIT_FAILURE, errno, "Beta-distributed data must be between "
                             "0 and 1. Found %f in column %zu.", value, i + 1);
                    beta_likl[beta_j] = score_beta (value, motif.beta_feats[beta_j]);
                    compl_beta_likl[beta_j] = score_beta (value, compl_motif.beta_feats[beta_j]);
                    beta_j++;
                    break;
                  }
                }
            }
          i++;
          field = strtok (NULL, delimiters);
        }
      FREE (line_cp);
      if (i <= arguments.seq_col)
        error (EXIT_FAILURE, 0, "The specified sequence window column is larger "
               "than the number of columns found in the file.");
      if (motif.n_feats > 0 && i <= arguments.feat_col)
        error (EXIT_FAILURE, 0, "The specified starting feature column is larger "
               "than the number of columns found in the file.");
      if (motif.method == SCORE_BAYES)
        {
          score = score_naive_bayes (likl, compl_likl, bernoulli_likl,
                                     compl_bernoulli_likl, motif.n_bernoulli,
                                     gaussian_likl, compl_gaussian_likl,
                                     motif.n_gaussian, beta_likl,
                                     compl_beta_likl, motif.n_beta, motif.prior,
                                     compl_motif.prior);
        }
      if (arguments.pval)
        {
          double pval = ecdf_prob (score, ecdf);
          if (arguments.append)
            {
              char *line_strip = strtok (line, "\n");
              printf ("%s%c%g%c%.*g\n", line, arguments.delim, score,
                      arguments.delim, (int) arguments.pval, pval);
            }
          else
            {
              printf ("%g%c%.*g\n", score, arguments.delim, (int) arguments.pval,
                      pval);
            }
        }
      else
        {
          if (arguments.append)
            {
              char *line_strip = strtok (line, "\n");
              printf ("%s%c%g\n", line, arguments.delim, score);
            }
          else
            {
              printf ("%g\n", score);
            }
        }
    }
  FREE (line);
  if (bernoulli_likl)
    FREE (bernoulli_likl);
  if (compl_bernoulli_likl)
    FREE (compl_bernoulli_likl);
  if (gaussian_likl)
    FREE (gaussian_likl);
  if (compl_gaussian_likl)
    FREE (compl_gaussian_likl);
  if (beta_likl)
    FREE (beta_likl);
  if (compl_beta_likl)
    FREE (compl_beta_likl);
}


int
main (int argc, char **argv)
{
  struct arguments arguments;
  init_args (&arguments);
  argp_parse (&argp, argc, argv, 0, 0, &arguments);

  char *motif_file = arguments.args[0];
  if (!motif_file)
    error (EXIT_FAILURE, errno, "No motif file specified");
  if (access (motif_file, F_OK) == -1)
    error (EXIT_FAILURE, errno, "Motif file %s does not exist",
           motif_file);
  hid_t file_id;
  file_id = H5Fopen (motif_file, H5F_ACC_RDONLY, H5P_DEFAULT);
  if (file_id < 0)
    error (EXIT_FAILURE, errno, "Failed to open motif file %s",
           motif_file);
  motifstats motif;
  read_motif_stats (file_id, &motif);
  H5Fclose (file_id);

  char *compl_motif_file = NULL;
  char *seq_file = NULL;
  gsl_vector *ecdf = NULL;
  motifstats compl_motif;
  compl_motif.pssm = NULL;
  if (motif.method == SCORE_BAYES)
    {
      compl_motif_file = arguments.args[1];
      if (!compl_motif_file)
        error (EXIT_FAILURE, errno, "No complementary motif file specified");
      if (access (compl_motif_file, F_OK) == -1)
        error (EXIT_FAILURE, errno, "Complementary motif file %s does not exist",
               compl_motif_file);
      hid_t compl_file_id;
      compl_file_id = H5Fopen (compl_motif_file, H5F_ACC_RDONLY, H5P_DEFAULT);
      if (compl_file_id < 0)
        error (EXIT_FAILURE, errno, "Failed to open complementary motif file %s",
               compl_motif_file);
      read_motif_stats (compl_file_id, &compl_motif);
      H5Fclose (compl_file_id);
      seq_file = arguments.args[2];
      if (arguments.pval > 0)
        {
          error (0, 0, "Scoring method is Naive Bayes, ignoring the request to "
                 "calculate p-values");
          arguments.pval = 0;
        }
      if (arguments.normalize)
        {
          error (0, 0, "Scoring method is Naive Bayes, ignoring the request to "
                 "normalize scores");
          arguments.normalize = false;
        }
    }
  else
    {
      if (arguments.scale == SCALE_GR)
        {
          free_motif_stats (motif, true);
          if (arguments.omit)
            FREE (arguments.omit);
          error (EXIT_FAILURE, errno, "Gain ratio weighting is only available "
                 "for Naive Bayes scoring");
        }
      seq_file = arguments.args[1];
      if (arguments.pval > 0)
        {
          gsl_vector *col_weight = NULL;
          if (arguments.scale == SCALE_IC)
            col_weight = motif.col_ic;
          else if (arguments.scale == SCALE_RELH)
            col_weight = motif.col_relh;
          ecdf = gsl_vector_calloc (pow (10, arguments.pval));
          calc_ecdf (ecdf, motif.pssm, motif.bg_freqs, col_weight, motif.type,
                     motif.bg, motif.method, arguments.normalize,
                     pow (10, arguments.pval), arguments.omit, arguments.n_omit);
        }
    }

  FILE *seq_stream;
  if (!seq_file)
    seq_stream = stdin;
  else
    {
      if (access (seq_file, F_OK) == -1)
        error (EXIT_FAILURE, errno, "Sequence file %s does not exist", seq_file);
      seq_stream = fopen (seq_file, "r");
      if (!seq_stream)
        error (EXIT_FAILURE, errno, "Failed to open sequence file %s",
               seq_file);
    }
  score_sequences (seq_stream, motif, compl_motif, ecdf, arguments);
  fclose (seq_stream);
  if (motif.method == SCORE_BAYES)
    free_motif_stats (compl_motif, true);
  free_motif_stats (motif, true);

  if (ecdf)
    gsl_vector_free (ecdf);

  if (arguments.omit)
    FREE (arguments.omit);

  return EXIT_SUCCESS;
}
