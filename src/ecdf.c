/* Copyright (C) 2019, 2020 Brandon Invergo <b.invergo@exeter.ac.uk> */

/* This program is free software: you can redistribute it and/or modify */
/* it under the terms of the GNU General Public License as published by */
/* the Free Software Foundation, either version 3 of the License, or */
/* (at your option) any later version. */

/* This program is distributed in the hope that it will be useful, */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the */
/* GNU General Public License for more details. */

/* You should have received a copy of the GNU General Public License */
/* along with this program.  If not, see <http://www.gnu.org/licenses/>. */


#include "ecdf.h"


void
random_seq_win (char *seq_win, size_t win_len, gsl_matrix *freqs, gsl_rng *rng,
                seq_type type)
{
  size_t max_c = type == SEQ_AA ? NUM_AA : NUM_NUC;
  for (size_t i=0; i<win_len; i++)
    {
      size_t rand_c;
      gsl_vector_view probs;
      probs = gsl_matrix_column (freqs, i);
      double rand_d = gsl_rng_uniform (rng);
      double cum_sum = 0.0;
      for (size_t j=0; j<max_c; j++)
        {
          cum_sum += gsl_vector_get (&probs.vector, j);
          if (rand_d < cum_sum)
            {
              rand_c = j;
              break;
            }
        }
      if (type == SEQ_AA)
        seq_win[i] = aa2char[rand_c];
      else
        seq_win[i] = nuc2char[rand_c];
    }
}


void
calc_ecdf (gsl_vector *ecdf, gsl_matrix *pm, gsl_matrix *bg_freqs,
           gsl_vector *col_weight, seq_type type, bg_type bg,
           score_method method, bool normalize, size_t n, size_t *omit,
           size_t n_omit)
{
  /* set up random number generator */
  gsl_rng_env_setup ();
  /* default is Mersenne Twister, but can be modified by the
     environment */
  gsl_rng *rng;
  rng = gsl_rng_alloc (gsl_rng_default);
  if (!rng)
    error (EXIT_FAILURE, errno, "Memory exhausted");

  size_t win_len = pm->size2;
  char *seq_win;
  if (ALLOC_N (seq_win, win_len + 1))
    error (EXIT_FAILURE, errno, "Memory exhausted");
  seq_win[win_len] = '\0';
  for (size_t i=0; i<n; i++)
    {
      random_seq_win (seq_win, pm->size2, bg_freqs, rng, type);
      double score = score_sequence (seq_win, pm, col_weight, type,
                                     method, normalize, omit, n_omit);
      gsl_vector_set (ecdf, i, score);
    }
  FREE (seq_win);
  gsl_sort_vector (ecdf);
  gsl_rng_free (rng);
}


double
ecdf_prob (double value, gsl_vector *ecdf)
{
  for (size_t i=0; i<ecdf->size; i++)
    {
      if (value < gsl_vector_get (ecdf, i))
        return 1.0 - ((double) i) / ((double) ecdf->size);
    }
  return 0.0;
}
