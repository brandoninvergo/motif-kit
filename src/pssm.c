/* Copyright (C) 2019, 2020, 2021 Brandon Invergo <b.invergo@exeter.ac.uk> */

/* This program is free software: you can redistribute it and/or modify */
/* it under the terms of the GNU General Public License as published by */
/* the Free Software Foundation, either version 3 of the License, or */
/* (at your option) any later version. */

/* This program is distributed in the hope that it will be useful, */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the */
/* GNU General Public License for more details. */

/* You should have received a copy of the GNU General Public License */
/* along with this program.  If not, see <http://www.gnu.org/licenses/>. */


#include "pssm.h"


/* Find the empirical number of residues at each position in the set
   of sequence windows, counting using sequence weights */
void
count_pos_res (const seq_set *seq_wins, gsl_vector *total_emp_counts,
               gsl_vector *total_pos_counts, gsl_matrix *emp_counts,
               const gsl_matrix *bg_freqs, bool weighted)
{
  gsl_matrix *res_pres = gsl_matrix_calloc (emp_counts->size1,
                                            emp_counts->size2);
  if (!res_pres)
    error (EXIT_FAILURE, errno, "Memory exhausted");
  gl_list_iterator_t i = gl_list_iterator (seq_wins->seqs);
  const void *eltp;
  while (gl_list_iterator_next (&i, &eltp, NULL))
    {
      seq_win *win = (seq_win *) eltp;
      for (size_t j=0; j<emp_counts->size2; j++)
        {
          double count;
          if (seq_wins->type == SEQ_AA)
            {
              aminoacid a = char2aa[(size_t) win->seq[j]];
              if (a == AA_ERR)
                error (EXIT_FAILURE, 0, "Invalid character in sequence %s: %c",
                       win->seq, win->seq[j]);
              else if (aa_is_ambiguous (a))
                continue;
              else if (aa_is_tail (a))
                {
                  /* For a sequence window tail ('_') effectively add
                     a weighted pseudocount divided across all
                     residues according to the background frequencies
                     (or evenly, depending on the user arguments) */
                  gsl_vector_view counts_col = gsl_matrix_column (emp_counts, j);
                  gsl_vector_const_view bg_col = gsl_matrix_const_column (bg_freqs, j);
                  gsl_vector *new_counts = gsl_vector_alloc (NUM_AA);
                  if (!new_counts)
                    error (EXIT_FAILURE, errno, "Memory exhausted");
                  gsl_vector_memcpy (new_counts, &bg_col.vector);
                  gsl_vector_scale (new_counts, win->weight);
                  gsl_vector_add (new_counts, &counts_col.vector);
                  gsl_matrix_set_col (emp_counts, j, new_counts);
                  gsl_vector_free (new_counts);
                }
              else
                {
                  count = gsl_matrix_get (emp_counts, a, j);
                  gsl_matrix_set (emp_counts, a, j, count + win->weight);
                  gsl_matrix_set (res_pres, a, j, 1.0);
                }
            }
          else
            {
              nucleotide n = char2nuc[(size_t) win->seq[j]];
              if (n == NUC_ERR)
                error (EXIT_FAILURE, 0, "Invalid character in sequence %s: %c",
                       win->seq, win->seq[j]);
              else if (nuc_is_ambiguous (n))
                continue;
              else if (nuc_is_tail (n))
                {
                  /* For a sequence window tail ('_') effectively add
                     a weighted pseudocount divided across all
                     residues according to the background frequencies
                     (or evenly, depending on the user arguments) */
                  gsl_vector_view counts_col = gsl_matrix_column (emp_counts, j);
                  gsl_vector_const_view bg_col = gsl_matrix_const_column (bg_freqs, j);
                  gsl_vector *new_counts = gsl_vector_alloc (NUM_NUC);
                  if (!new_counts)
                    error (EXIT_FAILURE, errno, "Memory exhausted");
                  gsl_vector_memcpy (new_counts, &bg_col.vector);
                  gsl_vector_scale (new_counts, win->weight);
                  gsl_vector_add (new_counts, &counts_col.vector);
                  gsl_matrix_set_col (emp_counts, j, new_counts);
                  gsl_vector_free (new_counts);
                }
              else
                {
                  count = gsl_matrix_get (emp_counts, n, j);
                  gsl_matrix_set (emp_counts, n, j, count + win->weight);
                  gsl_matrix_set (res_pres, n, j, 1.0);
                }
            }
        }
    }
  gl_list_iterator_free (&i);
  if (weighted)
    gsl_matrix_scale (emp_counts, (double) gl_list_size (seq_wins->seqs));
  gsl_vector *ones = gsl_vector_alloc (emp_counts->size1);
  if (!ones)
    {
      error (EXIT_FAILURE, errno, "Memory exhausted");
    }
  gsl_vector_set_all (ones, 1.0);
  gsl_blas_dgemv (CblasTrans, 1.0, res_pres, ones, 0.0, total_emp_counts);
  gsl_blas_dgemv (CblasTrans, 1.0, emp_counts, ones, 0.0, total_pos_counts);
  gsl_vector_free (ones);
  gsl_matrix_free (res_pres);
}


/* Calculate the number of position-specific pseudocounts using the
   method of Henikoff & Henikoff CABIOS 1996.  m is a tunable
   parameter.  N = m * R_c, where R_c is the total number of different
   residues observed in the column.  So, more diverse columns receive
   more pseudocounts (we are empirically less confident in their
   tendencies) */
void
calc_num_pos_pc (const gsl_vector *total_emp_counts, gsl_vector *num_pc,
                 double m)
{
  if (m <= 0)
    {
      error (EXIT_FAILURE, errno, "Invalid position-specific pseudocount "
             "parameter m.  Must be > 0");
    }
  gsl_vector_memcpy (num_pc, total_emp_counts);
  gsl_vector_scale (num_pc, m);
}


/* Calculate position-specific frequencies
 (nc + pc)/(num_nc + num_pc) */
void
calc_pos_freqs (gsl_matrix *pfm, const gsl_vector *total_pos_counts,
                const gsl_matrix *bg_freqs, const gsl_vector *num_pos_pc,
                double num_pc, bg_type bg, pc_n_method pc_n_meth,
                pc_dist_method pc_dist_meth)
{
  gsl_matrix *pcounts = gsl_matrix_calloc (pfm->size1,
                                           pfm->size2);
  if (!pcounts)
    error (EXIT_FAILURE, errno, "Memory exhausted");
  gsl_matrix *num_pcounts = gsl_matrix_calloc (pfm->size1,
                                               pfm->size2);
  if (!num_pcounts)
    error (EXIT_FAILURE, errno, "Memory exhausted");
  /* Number of pseudocounts */
  if (pc_n_meth == PC_N_POS)
    {
      for (size_t i=0; i<pcounts->size1; i++)
        gsl_matrix_set_row (num_pcounts, i, num_pos_pc);
    }
  else if (pc_n_meth == PC_N_CONST)
    {
      gsl_matrix_set_all (num_pcounts, num_pc);
    }
  else
    {
      gsl_matrix_set_zero (num_pcounts);
    }
  /* Distribute pseudocounts */
  if (pc_dist_meth == PC_DIST_FREQ)
    {
      gsl_matrix_memcpy (pcounts, bg_freqs);
      gsl_matrix_mul_elements (pcounts, num_pcounts);
    }
  else
    {
      /* PC_DIST_EQUAL */
      gsl_matrix_memcpy (pcounts, num_pcounts);
      gsl_matrix_scale (pcounts, 1.0 / pcounts->size1);
    }
  /* Number of empirical counts */
  gsl_matrix *num_counts = gsl_matrix_calloc (pfm->size1, pfm->size2);
  if (!num_counts)
    error (EXIT_FAILURE, errno, "Memory exhausted");
  for (size_t i=0; i<num_counts->size1; i++)
    gsl_matrix_set_row (num_counts, i, total_pos_counts);
  /* Total number of counts (empirical + pseudo */
  gsl_matrix_add (num_counts, num_pcounts);
  /* Position-specific frequencies = (emp. count + pseudocount)/(total counts) */
  gsl_matrix_add (pfm, pcounts);
  gsl_matrix_div_elements (pfm, num_counts);

  gsl_matrix_free (pcounts);
  gsl_matrix_free (num_pcounts);
  gsl_matrix_free (num_counts);
}


/* Get the column-wise information content of a PSSM.  Information
   content is defined in the sequence logo sense: max uncertainty
   minus column uncertainty. */
void
calc_col_ic (const gsl_matrix *pfm, gsl_vector *col_ic, double base)
{
  double max_h = log ((double) pfm->size1) / log (base);
  for (size_t j=0; j<pfm->size2; j++)
    {
      double h = 0.0;
      for (size_t i=0; i<pfm->size1; i++)
        {
          double freq = gsl_matrix_get (pfm, i, j);
          if (freq == 0.0)
            continue;
          h -= freq * log (freq) / log (base);
        }
      gsl_vector_set (col_ic, j, max_h - h);
    }
}


/* Get the column-wise relative entropy of a PSSM */
void
calc_col_relh (const gsl_matrix *pssm, const gsl_matrix *pfm,
               gsl_vector *col_relh)
{
  for (size_t j=0; j<pssm->size2; j++)
    {
      double relh = 0.0;
      for (size_t i=0; i<pssm->size1; i++)
        {
          double freq = gsl_matrix_get (pfm, i, j);
          double loglik = gsl_matrix_get (pssm, i, j);
          /* lim_{x ->0+} x log x = 0 */
          /* If freq = 0 implies loglik = 0, then relh = 0, otherwise
             it's undefined */
          if (isnan (loglik))
            {
              if (freq == 0)
                continue;
              else
                {
                  error (0, 0, "Undefined relative entropy: a character "
                         "was found that is not possible in the background");
                  relh = GSL_NAN;
                }
            }
          relh += freq * loglik;
        }
      gsl_vector_set (col_relh, j, relh);
    }
}


void
calc_col_gr (gsl_vector *gain_ratio, const gsl_matrix *pfm,
             const gsl_matrix *compl_pfm, double prior,
             double compl_prior, const gsl_vector *relh,
             const gsl_vector *compl_relh, const gsl_matrix *bg, double base)
{
  /* To calculate the gain ratio (mutual information over background
     entropy), we can take advantage of the already-calculated
     relative entropy. 

     MI = sum_{x,y} P(x, y)*log(P(x,y)/(P(x)P(y))
     relH = sum_x P(x, y)*log(P(x, y)/P(x)) 
     so, 
     MI = sum_y relH - sum_x P(x, y)*log(P(y))

     where P(y) is the prior probability as calculated for Naive Bayes
     scoring. */

  /* prior_offset is the bit we'll subtract from relH to be able to
     calculate MI */
  gsl_vector *prior_offset = gsl_vector_calloc (gain_ratio->size);
  if (!prior_offset)
    error (EXIT_FAILURE, errno, "Memory exhausted");
  gsl_vector *tmp = gsl_vector_alloc (gain_ratio->size);
  if (!tmp)
    error (EXIT_FAILURE, errno, "Memory exhausted");
  for (size_t i=0; i<pfm->size1; i++)
    {
      gsl_vector_const_view pfm_row = gsl_matrix_const_row (pfm, i);
      gsl_vector_memcpy (tmp, &pfm_row.vector);
      gsl_vector_scale (tmp, log (prior) / log (base));
      gsl_vector_add (prior_offset, tmp);
    }
  /* The first addend of the MI calculation: motif relH - prior_offset */
  gsl_vector *mi1 = gsl_vector_calloc (gain_ratio->size);
  if (!mi1)
    error (EXIT_FAILURE, errno, "Memory exhausted");
  gsl_vector_memcpy (mi1, relh);
  gsl_vector_sub (mi1, prior_offset);
  gsl_vector_set_zero (prior_offset);
  /* Now do the same for the complementary motif */
  for (size_t i=0; i<compl_pfm->size1; i++)
    {
      gsl_vector_const_view pfm_row = gsl_matrix_const_row (compl_pfm, i);
      gsl_vector_memcpy (tmp, &pfm_row.vector);
      gsl_vector_scale (tmp, log (compl_prior) / log (base));
      gsl_vector_add (prior_offset, tmp);
    }
  gsl_vector_free (tmp);
  gsl_vector *mi2 = gsl_vector_calloc (gain_ratio->size);
  if (!mi2)
    error (EXIT_FAILURE, errno, "Memory exhausted");
  gsl_vector_memcpy (mi2, compl_relh);
  gsl_vector_sub (mi2, prior_offset);
  gsl_vector_free (prior_offset);
  /* Add the two MI addends */
  gsl_vector_memcpy (gain_ratio, mi1);
  gsl_vector_free (mi1);
  gsl_vector_add (gain_ratio, mi2);
  gsl_vector_free (mi2);
  /* Divide by the column entropy of the background frequencies */
  gsl_vector *bg_ic = gsl_vector_calloc (gain_ratio->size);
  if (!bg_ic)
    error (EXIT_FAILURE, errno, "Memory exhausted");
  calc_col_ic (bg, bg_ic, base);
  gsl_vector_div (gain_ratio, bg_ic);
  gsl_vector_free (bg_ic);
}



/* Adjust a PSSM according as a fold-change against the background
   frequencies */
void
calc_fc_score (gsl_matrix *pssm, const gsl_matrix *pfm,
               const gsl_matrix *bg_freqs, bg_type bg,
               double base)
{
  gsl_matrix_memcpy (pssm, pfm);
  gsl_matrix_div_elements (pssm, bg_freqs);
  for (size_t i=0; i<pssm->size1; i++)
    {
      for (size_t j=0; j<pssm->size2; j++)
        {
          double fc = gsl_matrix_get (pssm, i, j);
          gsl_matrix_set (pssm, i, j, log (fc) / log (base));
        }
    }
}


/* Adjust PSSM according to the difference from the background
   frequences cf. Safei et al. 2011. Proteome Sci. 9 Suppl 1:S6. 
   doi: 10.1186/1477-5956-9-S1-S6. */
void
calc_diff_score (gsl_matrix *pssm, const gsl_matrix *pfm,
                 const gsl_matrix *bg_freqs, bg_type bg, double scale)
{
  gsl_matrix_memcpy (pssm, pfm);
  gsl_matrix_sub (pssm, bg_freqs);
  gsl_matrix *signs = gsl_matrix_calloc (pssm->size1, pssm->size2);
  if (!signs)
    error (EXIT_FAILURE, errno, "Memory exhausted");
  for (size_t i=0; i<pssm->size1; i++)
    {
      for (size_t j=0; j<pssm->size2; j++)
        {
          double cur = gsl_matrix_get (pssm, i, j);
          gsl_matrix_set (signs, i, j, (double) GSL_SIGN (cur));
          gsl_matrix_set (pssm, i, j, pow (fabs (cur), scale));
        }
    }
  gsl_matrix_mul_elements (pssm, signs);
  gsl_matrix_free (signs);
}


void
calc_freq_score (gsl_matrix *pssm, const gsl_matrix *pfm, double base)
{
  gsl_matrix_memcpy (pssm, pfm);
  /* Log-transform the frequencies */
  for (size_t i = 0; i < pssm->size1; i++)
    {
      for (size_t j = 0; j < pssm->size2; j++)
        {
          double freq = gsl_matrix_get (pssm, i, j);
          double log_freq = log10 (freq);
          gsl_matrix_set (pssm, i, j, log_freq);
        }
    }
}


/* Alternatives to gsl_vector_{min,max} that ignore NaN values */
static double
vector_min (const gsl_vector *v)
{
  const size_t N = v->size ;
  const size_t stride = v->stride ;
  double min = v->data[0 * stride];
  size_t i;
  for (i = 0; i < N; i++)
    {
      double x = v->data[i*stride];
      if (gsl_finite (x) && (!gsl_finite (min) || x < min))
        min = x;
    }
  return min;
}

static double
vector_max (const gsl_vector *v)
{
  const size_t N = v->size ;
  const size_t stride = v->stride ;
  double max = v->data[0 * stride];
  size_t i;
  for (i = 0; i < N; i++)
    {
      double x = v->data[i*stride];
      if (gsl_finite (x) && (!gsl_finite (max) || x > max))
        max = x;
    }
  return max;
}


double
score_sequence (const char *seq, const gsl_matrix *pssm,
                const gsl_vector *col_weight, seq_type type,
                score_method method, bool normalize, size_t *omit, size_t n_omit)
{
  if (!seq)
    error (EXIT_FAILURE, 0, "No sequence provided for scoring");
  if (!pssm)
    error (EXIT_FAILURE, 0, "No PSSM provided for scoring");
  if (strlen (seq) != pssm->size2)
    error (EXIT_FAILURE, 0, "Sequence length (%zd) does not match PSSM length (%zd): %s",
           strlen (seq), pssm->size2, seq);
  double score = 0.0;
  double min_score = 0.0;
  double max_score = 0.0;
  gsl_matrix *pssm_cpy = gsl_matrix_calloc (pssm->size1,
                                            pssm->size2);
  if (!pssm_cpy)
    error (EXIT_FAILURE, errno, "Memory exhausted");
  gsl_matrix_memcpy (pssm_cpy, pssm);
  gsl_matrix *scale_m = NULL;
  gsl_vector *col_weight_final = gsl_vector_alloc (pssm->size2);
  if (!col_weight_final)
    error (EXIT_FAILURE, errno, "Memory exhausted");
  if (col_weight)
    {
      gsl_vector_memcpy (col_weight_final, col_weight);
    }
  else
    {
      gsl_vector_set_all (col_weight_final, 1.0);
    }
  /* Zero-out the weights of any omitted columns */
  if (omit)
    {
      for (size_t i=0; i<n_omit; i++)
        {
          gsl_vector_set (col_weight_final, omit[i], 0.0);
        }
    }
  if (col_weight)
    {
      /* Re-scale so that the weights sum to n, the number of
         columns */
      double weight_sum = gsl_blas_dasum (col_weight_final);
      gsl_vector_scale (col_weight_final, (double) (col_weight_final->size - n_omit) / weight_sum);
    }
  /* Create a scaling matrix based on the weights */
  scale_m = gsl_matrix_calloc (pssm->size1,
                               pssm->size2);
  if (!scale_m)
    error (EXIT_FAILURE, errno, "Memory exhausted");
  for (size_t i=0; i<scale_m->size1; i++)
    gsl_matrix_set_row (scale_m, i, col_weight_final);
  gsl_matrix_mul_elements (pssm_cpy, scale_m);
  gsl_matrix_free (scale_m);
  gsl_vector_free (col_weight_final);

  gsl_vector *min_scores;
  gsl_vector *max_scores;
  if (normalize)
    {
      min_scores = gsl_vector_calloc (pssm_cpy->size2);
      if (!min_scores)
        error (EXIT_FAILURE, errno, "Memory exhausted");
      max_scores = gsl_vector_calloc (pssm_cpy->size2);
      if (!max_scores)
        error (EXIT_FAILURE, errno, "Memory exhausted");
      for (size_t i=0; i<pssm_cpy->size2; i++)
        {
          gsl_vector_const_view col = gsl_matrix_const_column (pssm_cpy, i);
          gsl_vector_set (min_scores, i, vector_min (&col.vector));
          gsl_vector_set (max_scores, i, vector_max (&col.vector));
        }
    }
  for (size_t i=0; i<pssm_cpy->size2; i++)
    {
      if (type == SEQ_AA)
        {
          aminoacid a = char2aa[seq[i]];
          if (a == AA_ERR)
            error (EXIT_FAILURE, 0, "Invalid character in sequence %s: %c",
                   seq, seq[i]);
          if (!aa_is_ambiguous (a) && !aa_is_tail (a))
            score += gsl_matrix_get (pssm_cpy, a, i);
        }
      else
        {
          nucleotide n = char2nuc[seq[i]];
          if (n == NUC_ERR)
            error (EXIT_FAILURE, 0, "Invalid character in sequence %s: %c",
                   seq, seq[i]);
          if (!nuc_is_ambiguous (n) && !nuc_is_tail (n))
            score += gsl_matrix_get (pssm_cpy, n, i);
        }
      if (normalize)
        {
          min_score += gsl_vector_get (min_scores, i);
          max_score += gsl_vector_get (max_scores, i);
        }
    }
  if (normalize)
    {
      score = (score - min_score) / (max_score - min_score);
      gsl_vector_free (min_scores);
      gsl_vector_free (max_scores);
    }
  gsl_matrix_free (pssm_cpy);
  return score;
}


/* Convenience function for printing a PSSM */
void
print_pssm (const gsl_matrix *pssm, unsigned int precision, char delim, int start_i)
{
  for (size_t j=0; j<pssm->size2; j++)
    {
      if (start_i >= 0)
        printf ("%c%zd", delim, start_i + j);
      else
        {
          if (start_i + j == 0)
            printf ("%c0", delim);
          else
            printf ("%c%+zd", delim, start_i + j);
        }
    }
  puts ("");
  for (size_t i=0; i<pssm->size1; i++)
    {
      if (pssm->size1 == NUM_AA)
        printf ("%c", aa2char[i]);
      else
        printf ("%c", nuc2char[i]);
      for (size_t j=0; j<pssm->size2; j++)
        {
          printf ("%c%.*f", delim, precision, gsl_matrix_get (pssm, i, j));
        }
      puts ("");
    }
}


/* Convenience function for printing a vector */
void
print_vector (const gsl_vector *v, unsigned int precision, char delim, int start_i)
{
  for (size_t i=0; i<v->size; i++)
    {
      if (start_i >= 0)
        printf ("%c%zd", delim, start_i + i);
      else
        {
          if (start_i + i == 0)
            printf ("%c0", delim);
          else
            printf ("%c%+zd", delim, start_i + i);
        }
      printf ("%c%.*f\n", delim, precision, gsl_vector_get (v, i));
    }
}


/* Calculate the distance between two PSSMs via the Frobenius norm ||A
 - B|| = sqrt(tr((A - B) * (A - B)^H)) where tr is the trace function
 (sum of the diagonal) and ^H is the conjugate transpose (which should
 just be a normal transpose in this case since there shouldn't be
 imaginary values). */
double
pssm_frob_distance (const gsl_matrix *pssm1, const gsl_matrix *pssm2)
{
  if (pssm1->size1 != pssm2->size1 || pssm1->size2 != pssm2->size2)
    {
      error (0, 0, "Cannot calculate distance between PSSMs of different size");
      return -1.0;
    }
  gsl_matrix *diff = gsl_matrix_calloc (pssm1->size1, pssm1->size2);
  if (!diff)
    error (EXIT_FAILURE, errno, "Memory exhausted");
  gsl_matrix_memcpy (diff, pssm1);
  /* C = A - B */
  gsl_matrix_sub (diff, pssm2);
  /* If both PSSMs have NaN values in the same position (due to a
     zero-probability residue in the background), then ignore those
     matrix cells by setting the difference to zero.  If only one has
     a NaN, that means a different background was use, which means it's
     probably meaningless to calculate the distance between the PSSMs */
  bool warned_bg = false;
  for (size_t i=0; i<diff->size1; i++)
    {
      for (size_t j=0; j<diff->size2; j++)
        {
          if (gsl_isnan (gsl_matrix_get (pssm1, i, j)) &&
              gsl_isnan (gsl_matrix_get (pssm2, i, j)))
            gsl_matrix_set (diff, i, j, 0.0);
          else if (gsl_isnan (gsl_matrix_get (pssm1, i, j)) ||
                   gsl_isnan (gsl_matrix_get (pssm2, i, j)))
            {
              if (warned_bg)
                continue;
              error (0, 0, "Warning: different backgrounds were probably used "
                     "so the distance is meaningless");
              warned_bg = true;
            }
        }
    }
  gsl_matrix *scratch = gsl_matrix_calloc (pssm1->size1, pssm1->size1);
  if (!scratch)
    error (EXIT_FAILURE, errno, "Memory exhausted");
  /* C * C^H */
  gsl_blas_dgemm (CblasNoTrans, CblasConjTrans, 1.0, diff, diff, 0.0, scratch);
  gsl_matrix_free (diff);
  /* tr(C * C^H) */
  gsl_vector_view diag = gsl_matrix_diagonal (scratch);
  gsl_vector *ones = gsl_vector_calloc (pssm1->size1);
  if (!ones)
    error (EXIT_FAILURE, errno, "Memory exhausted");
  gsl_vector_set_all (ones, 1.0);
  double norm;
  gsl_blas_ddot (&diag.vector, ones, &norm);
  gsl_vector_free (ones);
  gsl_matrix_free (scratch);
  /* sqrt(tr(C * C^H)) */
  return (sqrt (norm));
}


/* Calculate the distance between two PFMs by taking the total
   symmetric Kullback-Leibler Divergence (relative entropy) between
   them across all positions (columns).*/
double
pfm_kl_distance (const gsl_matrix *pfm1, const gsl_matrix *pfm2,
                 double base)
{
  if (pfm1->size1 != pfm2->size1 || pfm1->size2 != pfm2->size2)
    {
      error (0, 0, "Cannot calculate distance between PFMs of different size");
      return -1.0;
    }
  double dist = 0.0;
  if (base == 0)
    base = 2.0;
  for (size_t i=0; i<pfm1->size1; i++)
    {
      for (size_t j=0; j<pfm1->size2; j++)
        {
          double p1 = gsl_matrix_get (pfm1, i, j);
          double p2 = gsl_matrix_get (pfm2, i, j);
          if (p1 == 0.0 && p2 == 0.0)
            continue;
          double kl1 = p1 * (log (p1 / p2) / log (base));
          double kl2 = p2 * (log (p2 / p1) / log (base));
          dist += kl1 + kl2;
        }
    }
  return dist;
}
